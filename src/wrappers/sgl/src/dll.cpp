#include <ini.h>
#include <sgl.h>
#include <stdexcept>
#include <wrapper.h>
#include <null/null_wrapper.h>
#include <vulkan/vulkan_wrapper.h>

// This file implements the exported DLL functions which just redirect to an sglw::IWrapper instance.

#define API_IMPL(ret,name,parms) extern "C" __declspec(dllexport) ret CALL_CONV name parms

static CHAR g_iniWrapper[MAX_PATH];
static sglw::IWrapper* g_wrapper;

void loadSettings()
{
	sglw::Ini ini("sgl.ini");

	// Load INI settings.
	ini.get("sglw", "wrapper", g_iniWrapper, MAX_PATH, "null");

	// Ensure INI file has been created with default settings.
	ini.set("sglw", "wrapper", g_iniWrapper);
}

void createWrapper()
{
	try
	{
		if (!strcmp(g_iniWrapper, "null"))
			g_wrapper = new sglw::null::NullWrapper();
		else if (!strcmp(g_iniWrapper, "vulkan"))
			g_wrapper = new sglw::vulkan::VulkanWrapper();
		else
			throw std::runtime_error("Invalid wrapper specified in sglw.ini.");
	}
	catch (const std::exception& e)
	{
		char message[MAX_PATH]{};
		sprintf_s(message, "Could not create SGL wrapper.\n%s\nFalling back to null video output.", e.what());
		MessageBox(NULL, message, "sglw", MB_ICONERROR);
		g_wrapper = new sglw::null::NullWrapper();
	}
}

BOOL WINAPI DllMain(HMODULE hinstDLL, DWORD fdwReason, LPVOID lpReserved)
{
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			loadSettings();
			createWrapper();
			break;
		case DLL_PROCESS_DETACH:
			delete g_wrapper;
			break;
	}
	return TRUE;
}

// ---- Windows routines ----

API_IMPL(int, sgl_use_address_mode, (PROC_ADDRESS_CALLBACK ProcNextAddress, sgl_uint32** pStatus))
{
	return g_wrapper->useAddressMode(ProcNextAddress, pStatus);
}

// ---- Device and viewport routines ----

API_IMPL(int, sgl_create_screen_device, (int device_number, int x_dimension, int y_dimension,
	sgl_device_colour_types device_mode, sgl_bool double_buffer))
{
	return g_wrapper->createScreenDevice(device_number, x_dimension, y_dimension, device_mode, double_buffer);
}

API_IMPL(int, sgl_get_device, (int device_name, int* device_number, int* x_dimension, int* y_dimension,
	sgl_device_colour_types* device_mode, sgl_bool* double_buffer))
{
	return g_wrapper->getDevice(device_name, device_number, x_dimension, y_dimension, device_mode, double_buffer);
}

API_IMPL(void, sgl_delete_device, (int device_name))
{
	g_wrapper->deleteDevice(device_name);
}

API_IMPL(int, sgl_create_viewport, (int parent_device, int left, int top, int right, int bottom,
	float cam_rect_left, float cam_rect_top, float cam_rect_right, float cam_rect_bottom))
{
	return g_wrapper->createViewport(parent_device, left, top, right, bottom,
		cam_rect_left, cam_rect_top, cam_rect_right, cam_rect_bottom);
}

API_IMPL(int, sgl_get_viewport, (int viewport_name, int* left, int* top, int* right, int* bottom,
	float* cam_rect_left, float* cam_rect_top, float* cam_rect_right, float* cam_rect_bottom))
{
	return g_wrapper->getViewport(viewport_name, left, top, right, bottom,
		cam_rect_left, cam_rect_top, cam_rect_right, cam_rect_bottom);
}

API_IMPL(int, sgl_set_viewport, (int viewport_name, int left, int top, int right, int bottom,
	float cam_rect_left, float cam_rect_top, float cam_rect_right, float cam_rect_bottom))
{
	return g_wrapper->setViewport(viewport_name, left, top, right, bottom,
		cam_rect_left, cam_rect_top, cam_rect_right, cam_rect_bottom);
}

API_IMPL(void, sgl_subtract_viewport, (int viewport, int removed_viewport))
{
	g_wrapper->subtractViewport(viewport, removed_viewport);
}

API_IMPL(void, sgl_delete_viewport, (int viewport_name))
{
	g_wrapper->deleteViewport(viewport_name);
}

// ---- List and instance routines ----

API_IMPL(int, sgl_create_list, (sgl_bool generate_name, sgl_bool preserve_state, sgl_bool separate_list))
{
	return g_wrapper->createList(generate_name, preserve_state, separate_list);
}

API_IMPL(void, sgl_to_parent, ())
{
	g_wrapper->toParent();
}

API_IMPL(void, sgl_modify_list, (int list_name, sgl_bool clear_list))
{
	g_wrapper->modifyList(list_name, clear_list);
}

API_IMPL(void, sgl_delete_list, (int list_name))
{
	g_wrapper->deleteList(list_name);
}

API_IMPL(void, sgl_set_ignore_list, (int list_name, sgl_bool ignore))
{
	g_wrapper->setIgnoreList(list_name, ignore);
}

// ---- Transformation routines ----

API_IMPL(int, sgl_create_transform, (sgl_bool generate_name))
{
	return g_wrapper->createTransform(generate_name);
}

API_IMPL(void, sgl_modify_transform, (int name, sgl_bool clear_transform))
{
	g_wrapper->modifyTransform(name, clear_transform);
}

API_IMPL(void, sgl_translate, (float x, float y, float z))
{
	g_wrapper->translate(x, y, z);
}

API_IMPL(void, sgl_rotate, (sgl_vector axis, float angle))
{
	g_wrapper->rotate(axis, angle);
}

API_IMPL(void, sgl_scale, (float x, float y, float z))
{
	g_wrapper->scale(x, y, z);
}

// ---- Objects creation routines ----

API_IMPL(int, sgl_create_light_volume, (const sgl_bool bGenerateName, const sgl_bool bLightVol, const int LightName))
{
	return g_wrapper->createLightVolume(bGenerateName, bLightVol, LightName);
}

API_IMPL(void, sgl_add_simple_plane, (sgl_vector surface_point, sgl_vector normal, sgl_bool invisible))
{
	g_wrapper->addSimplePlane(surface_point, normal, invisible);
}

API_IMPL(void, sgl_add_plane, (sgl_vector surface_point, sgl_vector point2, sgl_vector point3, sgl_bool invisible,
	sgl_vector normal1, sgl_vector normal2, sgl_vector normal3, sgl_2d_vec uv1, sgl_2d_vec uv2, sgl_2d_vec uv3))
{
	g_wrapper->addPlane(surface_point, point2, point3, invisible, normal1, normal2, normal3, uv1, uv2, uv3);
}

// ---- Mesh routines ----

API_IMPL(int, sgl_create_mesh, (sgl_bool generate_name))
{
	return g_wrapper->createMesh(generate_name);
}

API_IMPL(void, sgl_add_vertices, (int num_to_add, sgl_vector* vertices, sgl_vector* vertex_normals,
	sgl_2d_vec* vertex_uvs))
{
	g_wrapper->addVertices(num_to_add, vertices, vertex_normals, vertex_uvs);
}

API_IMPL(void, sgl_add_face, (int num_face_points, int* vertex_ids))
{
	g_wrapper->addFace(num_face_points, vertex_ids);
}

// ---- Material routines ----

API_IMPL(void, sgl_set_texture_effect, (sgl_bool affect_ambient, sgl_bool affect_diffuse, sgl_bool affect_specular,
	sgl_bool affect_glow))
{
	g_wrapper->setTextureEffect(affect_ambient, affect_diffuse, affect_specular, affect_glow);
}

API_IMPL(int, sgl_create_texture, (sgl_map_types map_type, sgl_map_sizes map_size,
	sgl_mipmap_generation_options generate_mipmap, sgl_bool dither, const sgl_intermediate_map* pixel_data,
	const sgl_intermediate_map* filtered_maps[]))
{
	return g_wrapper->createTexture(map_type, map_size, generate_mipmap, dither, pixel_data, filtered_maps);
}

API_IMPL(void, sgl_set_ambient, (sgl_colour colour))
{
	g_wrapper->setAmbient(colour);
}

API_IMPL(void, sgl_set_diffuse, (sgl_colour colour))
{
	g_wrapper->setDiffuse(colour);
}

API_IMPL(void, sgl_set_glow, (sgl_colour colour))
{
	g_wrapper->setGlow(colour);
}

API_IMPL(void, sgl_set_specular, (sgl_colour colour, int shininess))
{
	g_wrapper->setSpecular(colour, shininess);
}

API_IMPL(void, sgl_set_opacity, (float opacity))
{
	g_wrapper->setOpacity(opacity);
}

API_IMPL(void, sgl_set_texture_map, (int texture_name, sgl_bool flip_u, sgl_bool flip_v))
{
	g_wrapper->setTextureMap(texture_name, flip_u, flip_v);
}

API_IMPL(int, sgl_preprocess_texture, (sgl_map_types map_type, sgl_map_sizes map_size,
	sgl_mipmap_generation_options generate_mipmap, sgl_bool dither, const sgl_intermediate_map* src_pixel_data,
	const sgl_intermediate_map* filtered_maps[], sgl_intermediate_map* processed_map))
{
	return g_wrapper->preprocessTexture(map_type, map_size,
		generate_mipmap, dither, src_pixel_data,
		filtered_maps, processed_map);
}

API_IMPL(void, sgl_delete_texture, (int texture_name))
{
	g_wrapper->deleteTexture(texture_name);
}

// ---- Light routines ----

API_IMPL(int, sgl_create_ambient_light, (sgl_bool generate_name, sgl_colour colour, sgl_bool relative))
{
	return g_wrapper->createAmbientLight(generate_name, colour, relative);
}

API_IMPL(int, sgl_create_point_light, (sgl_bool generate_name, sgl_colour colour, sgl_vector direction,
	sgl_vector position, int concentration, sgl_bool casts_shadows, sgl_bool smooth_highlights))
{
	return g_wrapper->createPointLight(generate_name, colour, direction,
		position, concentration, casts_shadows, smooth_highlights);
}

API_IMPL(void, sgl_switch_light, (int name, sgl_bool on, sgl_bool casts_shadows, sgl_bool smooth_highlights))
{
	g_wrapper->switchLight(name, on, casts_shadows, smooth_highlights);
}

API_IMPL(void, sgl_set_ambient_light, (int name, sgl_colour colour, sgl_bool relative))
{
	g_wrapper->setAmbientLight(name, colour, relative);
}

API_IMPL(void, sgl_set_point_light, (int name, sgl_colour colour, sgl_vector direction, sgl_vector position,
	int concentration, sgl_bool casts_shadows, sgl_bool smooth_highlights))
{
	g_wrapper->setPointLight(name, colour, direction, position, concentration, casts_shadows, smooth_highlights);
}

// ---- Camera routines ----

API_IMPL(int, sgl_create_camera, (float zoom_factor, float foreground, float inv_background))
{
	return g_wrapper->createCamera(zoom_factor, foreground, inv_background);
}

API_IMPL(void, sgl_render, (const int viewport_or_device, const int camera_or_list, const sgl_bool swap_buffers))
{
	g_wrapper->render(viewport_or_device, camera_or_list, swap_buffers);
}

// ---- Fog & background routines ----

API_IMPL(void, sgl_set_fog, (int camera_name, sgl_colour fog_colour, float fog_density))
{
	g_wrapper->setFog(camera_name, fog_colour, fog_density);
}

API_IMPL(void, sgl_set_background_colour, (int camera_name, sgl_colour colour))
{
	g_wrapper->setBackgroundColor(camera_name, colour);
}

// ---- Translucency routines ----

API_IMPL(void, sgl_new_translucent, ())
{
	g_wrapper->newTranslucent();
}

// ---- Quality settings ----

API_IMPL(void, sgl_qual_fog, (const sgl_bool enable))
{
	g_wrapper->qualFog(enable);
}

// ---- BMP to texture loading code ----

API_IMPL(void, FreeAllBMPTextures, ())
{
	g_wrapper->freeAllBmpTextures();
}
