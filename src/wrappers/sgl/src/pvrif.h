#pragma once

// This header is included by the original sgl.h, and changes the DLL function decorator macros to actually export them.

#define API_START(x)
#define API_END(x)
#define API_FN(ret,name,parms) extern "C" __declspec(dllexport) ret CALL_CONV name parms;
