#include "ini.h"
#include <stdio.h>

namespace sglw
{
	Ini::Ini(LPCSTR filename)
	{
		GetModuleFileName(NULL, _filepath, MAX_PATH);
		char* sepIdx = strrchr(_filepath, '\\') + 1;
		strcpy_s(sepIdx, MAX_PATH - (int)(sepIdx - _filepath), filename);
	}

	void Ini::get(LPCSTR category, LPCSTR key, BOOL& result, UINT fallback) const
	{
		result = GetPrivateProfileInt(category, key, fallback, _filepath);
	}

	void Ini::get(LPCSTR category, LPCSTR key, UINT& result, UINT fallback) const
	{
		result = GetPrivateProfileInt(category, key, fallback, _filepath);
	}

	void Ini::get(LPCSTR category, LPCSTR key, LPSTR result, INT resultLength, LPCSTR fallback) const
	{
		GetPrivateProfileString(category, key, fallback, result, resultLength, _filepath);
	}

	void Ini::set(LPCSTR category, LPCSTR key, UINT value) const
	{
		CHAR buffer[32];
		sprintf_s(buffer, "%d", value);
		WritePrivateProfileString(category, key, buffer, _filepath);
	}

	void Ini::set(LPCSTR category, LPCSTR key, LPCSTR value) const
	{
		WritePrivateProfileString(category, key, value, _filepath);
	}
}
