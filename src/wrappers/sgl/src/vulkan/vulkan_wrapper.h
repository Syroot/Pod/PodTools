#pragma once
#include <null/null_wrapper.h>
#include <sgl.h>
#include <vulkan/vulkan.h>
#include <vector>
#include <wrapper.h>

// This is an implementation of an SGL wrapper using the Vulkan API to wrap 3D drawing.

namespace sglw::vulkan
{
	class VulkanWrapper : public sglw::null::NullWrapper
	{
	public:
		// Device and viewport routines
		virtual int createScreenDevice(int deviceNumber, int xDimension, int yDimension,
			sgl_device_colour_types deviceMode, sgl_bool doubleBuffer) override;
		virtual void deleteDevice(int deviceName) override;

	private:
		bool _initialized{};
		VkInstance _instance{};
		VkDebugUtilsMessengerEXT _debugMessenger{};
		VkPhysicalDevice _physicalDevice{};

		static bool checkInstanceLayers(const std::vector<const char*>& layers);
		static bool checkInstanceExtensions(const std::vector<const char*>& extensions);
		static bool checkPhysicalDeviceExtensions(VkPhysicalDevice physicalDevice, std::vector<const char*>& extensions);
		static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
			VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
			void* pUserData);

		bool createInstance();
		bool createPhysicalDevice();
		void destroyInstance();
	};
}
