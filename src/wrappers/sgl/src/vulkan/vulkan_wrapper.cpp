#include "vulkan_wrapper.h"
#include <iostream>
#include <set>
#include <string>
#include <vulkan/vulkan_extensions.h>

using namespace std;

namespace sglw::vulkan
{
#ifdef NDEBUG
	static constexpr bool g_debug = false;
#else
	static constexpr bool g_debug = true;
#endif

	int VulkanWrapper::createScreenDevice(int deviceNumber, int xDimension, int yDimension,
		sgl_device_colour_types deviceMode, sgl_bool doubleBuffer)
	{
		if (!_initialized)
		{
			if (!createInstance() || !createPhysicalDevice())
				return sgl_err_bad_device;
			
			_initialized = true;
		}

		return _initialized || createInstance() ? 1 : sgl_err_bad_device;
	}

	void VulkanWrapper::deleteDevice(int deviceName)
	{
		if (_initialized)
			destroyInstance();
	}

	VKAPI_ATTR VkBool32 VKAPI_CALL VulkanWrapper::debugCallback(VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
		VkDebugUtilsMessageTypeFlagsEXT messageType, const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
		void* pUserData)
	{
		std::cerr << "debug message: " << pCallbackData->pMessage << std::endl;
		return VK_FALSE;
	}

	bool VulkanWrapper::checkInstanceLayers(const vector<const char*>& layers)
	{
		uint32_t count;
		vkEnumerateInstanceLayerProperties(&count, nullptr);
		std::vector<VkLayerProperties> availableLayers(count);
		vkEnumerateInstanceLayerProperties(&count, availableLayers.data());

		for (const char* layer : layers)
		{
			bool found = false;
			for (const auto& availableLayer : availableLayers)
			{
				if (!strcmp(layer, availableLayer.layerName))
				{
					found = true;
					break;
				}
			}
			if (!found)
				return false;
		}

		return true;
	}

	bool VulkanWrapper::checkInstanceExtensions(const vector<const char*>& extensions)
	{
		uint32_t count;
		vkEnumerateInstanceExtensionProperties(nullptr, &count, nullptr);
		std::vector<VkExtensionProperties> availableExtensions(count);
		vkEnumerateInstanceExtensionProperties(nullptr, &count, availableExtensions.data());

		for (const auto& extension : extensions)
		{
			bool found = false;
			for (const auto& availableExtension : availableExtensions)
			{
				if (!strcmp(extension, availableExtension.extensionName))
				{
					found = true;
					break;
				}
			}
			if (!found)
				return false;
		}

		return true;
	}

	bool VulkanWrapper::checkPhysicalDeviceExtensions(VkPhysicalDevice physicalDevice, vector<const char*>& extensions)
	{
		uint32_t extensionCount;
		vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, nullptr);
		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(physicalDevice, nullptr, &extensionCount, availableExtensions.data());

		for (const auto& extension : extensions)
		{
			bool found = false;
			for (const auto& availableExtension : availableExtensions)
			{
				if (!strcmp(extension, availableExtension.extensionName))
				{
					found = true;
					break;
				}
			}
			if (!found)
				return false;
		}

		return true;
	}

	bool VulkanWrapper::createInstance()
	{
		// Create debug messenger configuration.
		VkDebugUtilsMessengerCreateInfoEXT messengerCreateInfo{};
		messengerCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
		messengerCreateInfo.messageSeverity
			= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT
			| VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
		messengerCreateInfo.messageType
			= VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT
			| VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT
			| VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
		messengerCreateInfo.pfnUserCallback = debugCallback;
		messengerCreateInfo.pUserData = this;

		// Create instance.
		VkApplicationInfo appInfo{};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = "SGL Wrapper";
		appInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
		appInfo.pEngineName = "No Engine";
		appInfo.apiVersion = VK_API_VERSION_1_0;

		// Validate existence of required layers.
		vector<const char*> layers;
		if (g_debug)
			layers.push_back("VK_LAYER_KHRONOS_validation");
		if (!checkInstanceLayers(layers))
			return false;

		// Validate existence of required extensions.
		vector<const char*> extensions;
		if (g_debug)
			extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
		if (!checkInstanceExtensions(extensions))
			return false;

		VkInstanceCreateInfo instanceCreateInfo{};
		instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		if (g_debug)
			instanceCreateInfo.pNext = &messengerCreateInfo;
		instanceCreateInfo.pApplicationInfo = &appInfo;
		instanceCreateInfo.enabledLayerCount = static_cast<uint32_t>(layers.size());
		instanceCreateInfo.ppEnabledLayerNames = layers.data();
		instanceCreateInfo.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
		instanceCreateInfo.ppEnabledExtensionNames = extensions.data();
		if (vkCreateInstance(&instanceCreateInfo, nullptr, &_instance) != VK_SUCCESS)
			return false;

		// Create debug messenger.
		if (g_debug)
			if (CreateDebugUtilsMessengerEXT(_instance, &messengerCreateInfo, nullptr, &_debugMessenger) != VK_SUCCESS)
				return false;

		return true;
	}

	bool VulkanWrapper::createPhysicalDevice()
	{
		// Get all physical devices.
		uint32_t deviceCount;
		vkEnumeratePhysicalDevices(_instance, &deviceCount, nullptr);
		vector<VkPhysicalDevice> physicalDevices(deviceCount);
		vkEnumeratePhysicalDevices(_instance, &deviceCount, physicalDevices.data());

		// Take first device supporting required extensions and queues.
		std::vector<const char*> extensions;
		for (const auto& physicalDevice : physicalDevices)
		{
			if (checkPhysicalDeviceExtensions(physicalDevice, extensions))
			{
				_physicalDevice = physicalDevice;
				break;
			}
			// Check queue support.
		}

		return false;
	}

	void VulkanWrapper::destroyInstance()
	{
		if (g_debug && _debugMessenger)
			DestroyDebugUtilsMessengerEXT(_instance, _debugMessenger, nullptr);
		if (_instance)
			vkDestroyInstance(_instance, nullptr);
	}
}
