#pragma once
#include <list>
#include <sgl.h>

// This is the interface every SGL wrapper must implement.

namespace sglw
{
	class IWrapper
	{
	public:
		virtual ~IWrapper() { }

		// Windows routines
		virtual int useAddressMode(PROC_ADDRESS_CALLBACK procNextAddress, sgl_uint32** pStatus) = 0;

		// Device and viewport routines
		virtual int createScreenDevice(int deviceNumber, int xDimension, int yDimension,
			sgl_device_colour_types deviceMode, sgl_bool doubleBuffer) = 0;
		virtual int getDevice(int deviceName, int* deviceNumber, int* xDimension, int* yDimension,
			sgl_device_colour_types* deviceMode, sgl_bool* doubleBuffer) = 0;
		virtual void deleteDevice(int deviceName) = 0;
		virtual int createViewport(int parentDevice, int left, int top, int right, int bottom,
			float camRectLeft, float camRectTop, float camRectRight, float camRectBottom) = 0;
		virtual int getViewport(int viewportName, int* left, int* top, int* right, int* bottom,
			float* camRectLeft, float* camRectTop, float* camRectRight, float* camRectBottom) = 0;
		virtual int setViewport(int viewportName, int left, int top, int right, int bottom,
			float camRectLeft, float camRectTop, float camRectRight, float camRectBottom) = 0;
		virtual void subtractViewport(int viewport, int removedViewport) = 0;
		virtual void deleteViewport(int viewportName) = 0;

		// List and instance routines
		virtual int createList(sgl_bool generateName, sgl_bool preserveState, sgl_bool separateList) = 0;
		virtual void toParent() = 0;
		virtual void modifyList(int listName, sgl_bool clearList) = 0;
		virtual void deleteList(int listName) = 0;
		virtual void setIgnoreList(int listName, sgl_bool ignore) = 0;

		// Transformation routines
		virtual int createTransform(sgl_bool generateName) = 0;
		virtual void modifyTransform(int name, sgl_bool clearTransform) = 0;
		virtual void translate(float x, float y, float z) = 0;
		virtual void rotate(sgl_vector axis, float angle) = 0;
		virtual void scale(float x, float y, float z) = 0;

		// Objects creation routines
		virtual int createLightVolume(const bool generateName, const bool lightVol, const int lightName) = 0;
		virtual void addSimplePlane(sgl_vector surfacePoint, sgl_vector normal, sgl_bool invisible) = 0;
		virtual void addPlane(sgl_vector surfacePoint, sgl_vector point2, sgl_vector point3, sgl_bool invisible,
			sgl_vector normal1, sgl_vector normal2, sgl_vector normal3,
			sgl_2d_vec uv1, sgl_2d_vec uv2, sgl_2d_vec uv3) = 0;

		// Mesh routines
		virtual int createMesh(sgl_bool generateName) = 0;
		virtual void addVertices(int numToAdd, sgl_vector* vertices, sgl_vector* vertexNormals,
			sgl_2d_vec* vertexUvs) = 0;
		virtual void addFace(int numFacePoints, int* vertexIds) = 0;

		// Material routines
		virtual void setTextureEffect(sgl_bool affectAmbient, sgl_bool affectDiffuse, sgl_bool affectSpecular,
			sgl_bool affectGlow) = 0;
		virtual int createTexture(sgl_map_types mapType, sgl_map_sizes mapSize,
			sgl_mipmap_generation_options generateMipmap, sgl_bool dither, const sgl_intermediate_map* pixelData,
			const sgl_intermediate_map* filteredMaps[]) = 0;
		virtual void setAmbient(sgl_colour color) = 0;
		virtual void setDiffuse(sgl_colour color) = 0;
		virtual void setGlow(sgl_colour color) = 0;
		virtual void setSpecular(sgl_colour color, int shininess) = 0;
		virtual void setOpacity(float opacity) = 0;
		virtual void setTextureMap(int textureName, sgl_bool flipU, sgl_bool flipV) = 0;
		virtual int preprocessTexture(sgl_map_types mapType, sgl_map_sizes mapSize,
			sgl_mipmap_generation_options generateMipmap, sgl_bool dither, const sgl_intermediate_map* srcPixelData,
			const sgl_intermediate_map* filteredMaps[], sgl_intermediate_map* processedMap) = 0;
		virtual void deleteTexture(int textureName) = 0;

		// Light routines
		virtual int createAmbientLight(sgl_bool generateName, sgl_colour color, sgl_bool relative) = 0;
		virtual int createPointLight(sgl_bool generateName, sgl_colour color, sgl_vector direction,
			sgl_vector position, int concentration, sgl_bool castsShadows, sgl_bool smoothHighlights) = 0;
		virtual void switchLight(int name, sgl_bool on, sgl_bool castsShadows, sgl_bool smoothHighlights) = 0;
		virtual void setAmbientLight(int name, sgl_colour color, sgl_bool relative) = 0;
		virtual void setPointLight(int name, sgl_colour color, sgl_vector direction, sgl_vector position,
			int concentration, sgl_bool castsShadows, sgl_bool smoothHighlights) = 0;

		// Camera routines
		virtual int createCamera(float zoomFactor, float foreground, float invBackground) = 0;
		virtual void render(const int viewportOrDevice, const int cameraOrList, const bool swapBuffers) = 0;

		// Fog & background routines
		virtual void setFog(int cameraName, sgl_colour fogColor, float fogDensity) = 0;
		virtual void setBackgroundColor(int cameraName, sgl_colour color) = 0;

		// Translucency routines
		virtual void newTranslucent() = 0;

		// Quality settings
		virtual void qualFog(const bool enable) = 0;

		// BMP to texture loading code
		virtual void freeAllBmpTextures() = 0;
	};
}
