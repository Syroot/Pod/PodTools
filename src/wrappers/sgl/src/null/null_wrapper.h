#pragma once
#include <wrapper.h>

// This is a dummy base implementation of an SGL wrapper doing absolutely no 3D drawing, not even clearing the buffer.

namespace sglw::null
{
	struct SglDevice
	{
		int name;
		int deviceNumber;
		int xDimension;
		int yDimension;
		sgl_device_colour_types mode;
		sgl_bool doubleBuffer;
	};

	struct SglViewport
	{
		int name;
		int parentDevice;
		int left;
		int top;
		int right;
		int bottom;
		float camRectLeft;
		float camRectTop;
		float camRectRight;
		float camRectBottom;
	};

	class NullWrapper : public IWrapper
	{
	public:
		// Windows routines
		virtual int useAddressMode(PROC_ADDRESS_CALLBACK procNextAddress, sgl_uint32** pStatus) override;

		// Device and viewport routines
		virtual int createScreenDevice(int deviceNumber, int xDimension, int yDimension,
			sgl_device_colour_types deviceMode, sgl_bool doubleBuffer) override;
		virtual int getDevice(int deviceName, int* deviceNumber, int* xDimension, int* yDimension,
			sgl_device_colour_types* deviceMode, sgl_bool* doubleBuffer) override;
		virtual void deleteDevice(int deviceName) override;
		virtual int createViewport(int parentDevice, int left, int top, int right, int bottom,
			float camRectLeft, float camRectTop, float camRectRight, float camRectBottom) override;
		virtual int getViewport(int viewportName, int* left, int* top, int* right, int* bottom,
			float* camRectLeft, float* camRectTop, float* camRectRight, float* camRectBottom) override;
		virtual int setViewport(int viewportName, int left, int top, int right, int bottom,
			float camRectLeft, float camRectTop, float camRectRight, float camRectBottom) override;
		virtual void subtractViewport(int viewport, int removedViewport) override;
		virtual void deleteViewport(int viewportName) override;

		// List and instance routines
		virtual int createList(sgl_bool generateName, sgl_bool preserveState, sgl_bool separateList) override;
		virtual void toParent() override;
		virtual void modifyList(int listName, sgl_bool clearList) override;
		virtual void deleteList(int listName) override;
		virtual void setIgnoreList(int listName, sgl_bool ignore) override;

		// Transformation routines
		virtual int createTransform(sgl_bool generateName) override;
		virtual void modifyTransform(int name, sgl_bool clearTransform) override;
		virtual void translate(float x, float y, float z) override;
		virtual void rotate(sgl_vector axis, float angle) override;
		virtual void scale(float x, float y, float z) override;

		// Objects creation routines
		virtual int createLightVolume(const bool generateName, const bool lightVol, const int lightName) override;
		virtual void addSimplePlane(sgl_vector surfacePoint, sgl_vector normal, sgl_bool invisible) override;
		virtual void addPlane(sgl_vector surfacePoint, sgl_vector point2, sgl_vector point3, sgl_bool invisible,
			sgl_vector normal1, sgl_vector normal2, sgl_vector normal3,
			sgl_2d_vec uv1, sgl_2d_vec uv2, sgl_2d_vec uv3) override;

		// Mesh routines
		virtual int createMesh(sgl_bool generateName) override;
		virtual void addVertices(int numToAdd, sgl_vector* vertices, sgl_vector* vertexNormals,
			sgl_2d_vec* vertexUvs) override;
		virtual void addFace(int numFacePoints, int* vertexIds) override;

		// Material routines
		virtual void setTextureEffect(sgl_bool affectAmbient, sgl_bool affectDiffuse, sgl_bool affectSpecular,
			sgl_bool affectGlow) override;
		virtual int createTexture(sgl_map_types mapType, sgl_map_sizes mapSize,
			sgl_mipmap_generation_options generateMipmap, sgl_bool dither, const sgl_intermediate_map* pixelData,
			const sgl_intermediate_map* filteredMaps[]) override;
		virtual void setAmbient(sgl_colour color) override;
		virtual void setDiffuse(sgl_colour color) override;
		virtual void setGlow(sgl_colour color) override;
		virtual void setSpecular(sgl_colour color, int shininess) override;
		virtual void setOpacity(float opacity) override;
		virtual void setTextureMap(int textureName, sgl_bool flipU, sgl_bool flipV) override;
		virtual int preprocessTexture(sgl_map_types mapType, sgl_map_sizes mapSize,
			sgl_mipmap_generation_options generateMipmap, sgl_bool dither, const sgl_intermediate_map* srcPixelData,
			const sgl_intermediate_map* filteredMaps[], sgl_intermediate_map* processedMap) override;
		virtual void deleteTexture(int textureName) override;

		// Light routines
		virtual int createAmbientLight(sgl_bool generateName, sgl_colour color, sgl_bool relative) override;
		virtual int createPointLight(sgl_bool generateName, sgl_colour color, sgl_vector direction,
			sgl_vector position, int concentration, sgl_bool castsShadows, sgl_bool smoothHighlights) override;
		virtual void switchLight(int name, sgl_bool on, sgl_bool castsShadows, sgl_bool smoothHighlights) override;
		virtual void setAmbientLight(int name, sgl_colour color, sgl_bool relative) override;
		virtual void setPointLight(int name, sgl_colour color, sgl_vector direction, sgl_vector position,
			int concentration, sgl_bool castsShadows, sgl_bool smoothHighlights) override;

		// Camera routines
		virtual int createCamera(float zoomFactor, float foreground, float invBackground) override;
		virtual void render(const int viewportOrDevice, const int cameraOrList, const bool swapBuffers) override;

		// Fog & background routines
		virtual void setFog(int cameraName, sgl_colour fogColor, float fogDensity) override;
		virtual void setBackgroundColor(int cameraName, sgl_colour color) override;

		// Translucency routines
		virtual void newTranslucent() override;

		// Quality settings
		virtual void qualFog(const bool enable) override;

		// BMP to texture loading code
		virtual void freeAllBmpTextures() override;

	private:
		int _deviceNames;
		int _listNames;
		int _viewportNames;
		int _transformNames;
		int _meshNames;
		int _textureNames;
		int _lightNames;
		int _cameraNames;
		std::list<SglDevice> _devices;
		std::list<SglViewport> _viewports;
		PROC_ADDRESS_CALLBACK _callback;
		sgl_uint32 _status;
	};
}
