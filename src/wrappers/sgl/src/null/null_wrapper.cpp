#include "null_wrapper.h"

namespace sglw::null
{
	// ---- Windows routines ----

    int NullWrapper::useAddressMode(PROC_ADDRESS_CALLBACK procNextAddress, sgl_uint32** pStatus)
	{
		_callback = procNextAddress;
		*pStatus = &_status;
		return sgl_no_err;
    }

	// ---- Device and viewport routines ----

	int NullWrapper::createScreenDevice(int deviceNumber, int xDimension, int yDimension,
		sgl_device_colour_types deviceMode, sgl_bool doubleBuffer)
	{
		SglDevice device
		{
			++_deviceNames,
			std::max(1, deviceNumber),
			xDimension,
			yDimension,
			deviceMode,
			doubleBuffer
		};
		_devices.push_back(device);
		return device.name;
	}

	int NullWrapper::getDevice(int deviceName, int* deviceNumber, int* xDimension, int* yDimension,
		sgl_device_colour_types* deviceMode, sgl_bool* doubleBuffer)
	{
		for (const auto& device : _devices)
		{
			if (device.name == deviceName)
			{
				*deviceNumber = device.deviceNumber;
				*xDimension = device.xDimension;
				*yDimension = device.yDimension;
				*deviceMode = device.mode;
				*doubleBuffer = device.doubleBuffer;
				return sgl_no_err;
			}
		}
		return sgl_err_bad_name;
	}
	
	void NullWrapper::deleteDevice(int deviceName)
	{
		for (auto it = _devices.begin(); it != _devices.end(); it++)
		{
			if (it->name == deviceName)
			{
				_devices.erase(it);
				return;
			}
		}
	}
	
	int NullWrapper::createViewport(int parentDevice, int left, int top, int right, int bottom,
		float camRectLeft, float camRectTop, float camRectRight, float camRectBottom)
	{
		SglViewport viewport
		{
			++_viewportNames,
			parentDevice,
			left, top,
			right, bottom,
			camRectLeft,
			camRectTop,
			camRectRight,
			camRectBottom
		};
		_viewports.push_back(viewport);
		return viewport.name;
	}
	
	int NullWrapper::getViewport(int viewportName, int* left, int* top, int* right, int* bottom,
		float* camRectLeft, float* camRectTop, float* camRectRight, float* camRectBottom)
	{
		for (const auto& viewport : _viewports)
		{
			if (viewport.name == viewportName)
			{
				*left = viewport.left;
				*top = viewport.top;
				*right = viewport.right;
				*bottom = viewport.bottom;
				*camRectLeft = viewport.camRectLeft;
				*camRectTop = viewport.camRectTop;
				*camRectRight = viewport.camRectRight;
				*camRectBottom = viewport.camRectBottom;
				return sgl_no_err;
			}
		}
		return sgl_err_bad_name;
	}
	
	int NullWrapper::setViewport(int viewportName, int left, int top, int right, int bottom,
		float camRectLeft, float camRectTop, float camRectRight, float camRectBottom)
	{
		for (auto& viewport : _viewports)
		{
			if (viewport.name == viewportName)
			{
				viewport.left = left;
				viewport.top = top;
				viewport.right = right;
				viewport.bottom = bottom;
				viewport.camRectLeft = camRectLeft;
				viewport.camRectTop = camRectTop;
				viewport.camRectRight = camRectRight;
				viewport.camRectBottom = camRectBottom;
				return sgl_no_err;
			}
		}
		return sgl_err_bad_name;
	}
	
	void NullWrapper::subtractViewport(int viewport, int removedViewport)
	{
	}
	
	void NullWrapper::deleteViewport(int viewportName)
	{
		for (auto it = _viewports.begin(); it != _viewports.end(); it++)
		{
			if (it->name == viewportName)
			{
				_viewports.erase(it);
				return;
			}
		}
	}

	// ---- List and instance routines ----

	int NullWrapper::createList(sgl_bool generateName, sgl_bool preserveState, sgl_bool separateList)
	{
		if (generateName)
			return ++_listNames;
		return sgl_no_err;
	}
	
	void NullWrapper::toParent()
	{
	}
	
	void NullWrapper::modifyList(int listName, sgl_bool clearList)
	{
	}
	
	void NullWrapper::deleteList(int listName)
	{
	}
	
	void NullWrapper::setIgnoreList(int listName, sgl_bool ignore)
	{
	}

	// ---- Transformation routines ----

	int NullWrapper::createTransform(sgl_bool generateName)
	{
		if (generateName)
			return ++_textureNames;
		return sgl_no_err;
	}
	
	void NullWrapper::modifyTransform(int name, sgl_bool clearTransform)
	{
	}
	
	void NullWrapper::translate(float x, float y, float z)
	{
	}
	
	void NullWrapper::rotate(sgl_vector axis, float angle)
	{
	}
	
	void NullWrapper::scale(float x, float y, float z)
	{
	}

	// ---- Objects creation routines ----

	int NullWrapper::createLightVolume(const bool generateName, const bool lightVol, const int lightName)
	{
		if (generateName)
			return ++_lightNames;
		return sgl_no_err;
	}
	
	void NullWrapper::addSimplePlane(sgl_vector surfacePoint, sgl_vector normal, sgl_bool invisible)
	{
	}
	
	void NullWrapper::addPlane(sgl_vector surfacePoint, sgl_vector point2, sgl_vector point3, sgl_bool invisible,
		sgl_vector normal1, sgl_vector normal2, sgl_vector normal3,
		sgl_2d_vec uv1, sgl_2d_vec uv2, sgl_2d_vec uv3)
	{
	}

	// ---- Mesh routines ----

	int NullWrapper::createMesh(sgl_bool generateName)
	{
		if (generateName)
			return ++_meshNames;
		return sgl_no_err;
	}
	
	void NullWrapper::addVertices(int numToAdd, sgl_vector* vertices, sgl_vector* vertexNormals,
		sgl_2d_vec* vertexUvs)
	{
	}
	
	void NullWrapper::addFace(int numFacePoints, int* vertexIds)
	{
	}

	// ---- Material routines ----

	void NullWrapper::setTextureEffect(sgl_bool affectAmbient, sgl_bool affectDiffuse, sgl_bool affectSpecular,
		sgl_bool affectGlow){}
	
	int NullWrapper::createTexture(sgl_map_types mapType, sgl_map_sizes mapSize,
		sgl_mipmap_generation_options generateMipmap, sgl_bool dither, const sgl_intermediate_map* pixelData,
		const sgl_intermediate_map* filteredMaps[])
	{
		return ++_textureNames;
	}
	
	void NullWrapper::setAmbient(sgl_colour color)
	{
	}
	
	void NullWrapper::setDiffuse(sgl_colour color)
	{
	}
	
	void NullWrapper::setGlow(sgl_colour color)
	{
	}
	
	void NullWrapper::setSpecular(sgl_colour color, int shininess)
	{
	}
	
	void NullWrapper::setOpacity(float opacity)
	{
	}
	
	void NullWrapper::setTextureMap(int textureName, sgl_bool flipU, sgl_bool flipV)
	{
	}
	
	int NullWrapper::preprocessTexture(sgl_map_types mapType, sgl_map_sizes mapSize,
		sgl_mipmap_generation_options generateMipmap, sgl_bool dither, const sgl_intermediate_map* srcPixelData,
		const sgl_intermediate_map* filteredMaps[], sgl_intermediate_map* processedMap)
	{
		// Make up some kind of meaningful value.
		int size = 0;
		switch (mapType)
		{
			case sgl_map_16bit:
			case sgl_map_16bit_mm:
			case sgl_map_trans16:
			case sgl_map_trans16_mm:
				size = 2;
				break;
			case sgl_map_8bit:
				size = 1;
				break;
		}
		switch (mapSize)
		{
			case sgl_map_32x32:
				size *= 32 * 32;
				break;
			case sgl_map_64x64:
				size *= 64 * 64;
				break;
			case sgl_map_128x128:
				size *= 128 * 128;
				break;
			case sgl_map_256x256:
				size *= 256 * 256;
				break;
		}
		switch (generateMipmap)
		{
			case sgl_mipmap_generate_2x2:
				size += size / 2;
				break;
			case sgl_mipmap_generate_4x4:
				size += size / 2;
				size += size / 4;
				break;
		}
		return size;
	}

	void NullWrapper::deleteTexture(int textureName)
	{
	}

	// ---- Light routines ----

	int NullWrapper::createAmbientLight(sgl_bool generateName, sgl_colour color, sgl_bool relative)
	{
		if (generateName)
			return ++_lightNames;
		return sgl_no_err;
	}
	
	int NullWrapper::createPointLight(sgl_bool generateName, sgl_colour color, sgl_vector direction,
		sgl_vector position, int concentration, sgl_bool castsShadows, sgl_bool smoothHighlights)
	{
		if (generateName)
			return ++_lightNames;
		return sgl_no_err;
	}
	
	void NullWrapper::switchLight(int name, sgl_bool on, sgl_bool castsShadows, sgl_bool smoothHighlights)
	{
	}
	
	void NullWrapper::setAmbientLight(int name, sgl_colour color, sgl_bool relative)
	{
	}
	
	void NullWrapper::setPointLight(int name, sgl_colour color, sgl_vector direction, sgl_vector position,
		int concentration, sgl_bool castsShadows, sgl_bool smoothHighlights)
	{
	}

	// ---- Camera routines ----

	int NullWrapper::createCamera(float zoomFactor, float foreground, float invBackground)
	{
		return ++_cameraNames;
	}

	void NullWrapper::render(const int viewportOrDevice, const int cameraOrList, const bool swapBuffers)
	{
		// Could wait here for "vertical sync" or at least clear the buffer.
		CALLBACK_ADDRESS_PARAMS paramBlk{};
		_callback(&paramBlk);
	}

	// ---- Fog & background routines ----

	void NullWrapper::setFog(int cameraName, sgl_colour fogColor, float fogDensity)
	{
	}

	void NullWrapper::setBackgroundColor(int cameraName, sgl_colour color)
	{
	}

	// ---- Translucency routines ----

	void NullWrapper::newTranslucent()
	{
	}

	// ---- Quality settings ----

	void NullWrapper::qualFog(const bool enable)
	{
	}

	// ---- BMP to texture loading code ----

	void NullWrapper::freeAllBmpTextures()
	{
	}
}
