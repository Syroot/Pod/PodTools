#include "client.h"

using namespace std;

namespace gs
{
	bool Client::init(uint32_t version)
	{
		return true;
	}

	bool Client::connectTo(const string& ip, const uint16_t port)
	{
		_initiateConnectionCb();
		return true;
	}

	bool Client::connectionRequest(const string& playerName, const string& password, const string&,
		const string& connectInfo, const string& connectVersion, const string&, const string&)
	{
		return true;
	}

	bool Client::createNewUserRequest(const string& name, const string& password, const string& email,
		const string& country, const string visitingCard)
	{
		return true;
	}

	void Client::setInitiateConnectionCb(InitiateConnectionCb cb)
	{
		_initiateConnectionCb = cb;
	}
}
