#pragma once
#include <string>
#include <stdint.h>

namespace gs
{
	class Client
	{
	public:
		typedef void(*InitiateConnectionCb)();

		bool init(uint32_t version);

		bool connectTo(const std::string& ip, const uint16_t port);
		bool connectionRequest(const std::string& playerName, const std::string& password, const std::string&,
			const std::string& connectInfo, const std::string& connectVersion, const std::string&, const std::string&);
		bool createNewUserRequest(const std::string& name, const std::string& password, const std::string& email,
			const std::string& country, const std::string visitingCard);
		void setInitiateConnectionCb(InitiateConnectionCb cb);

	private:
		InitiateConnectionCb _initiateConnectionCb{};
	};
}
