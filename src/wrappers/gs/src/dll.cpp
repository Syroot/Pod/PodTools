#define NOMINMAX
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <client.h>

// This file implements the exported DLL functions which just redirect to the gs::Client instance.

#define GSAPI(ret) extern "C" __declspec(dllexport) ret __cdecl

static gs::Client g_client;

GSAPI(BOOL) GS_bBeginGame()
{
	// Arena 49 / 4 / 3
	return TRUE;
}

GSAPI(BOOL) GS_bChatToAll(LPCSTR message)
{
	// Arena 27 / 4 / 2
	// message
	return TRUE;
}

GSAPI(BOOL) GS_bChatToOnePlayer(LPCSTR playerName, LPCSTR message)
{
	// not used by POD
	// Arena 30 / 4 / 2
	// playerName
	// message
	return TRUE;
}

GSAPI(BOOL) GS_bChatToPlayers(LPCSTR* playerNames, DWORD playerCount, LPCSTR message)
{
	// Arena 28 / 4 / 2
	// playerNames[playerCount]
	// message
	return TRUE;
}

GSAPI(BOOL) GS_bChatToSession(LPCSTR sessionName, LPCSTR message)
{
	// not used by POD
	// Arena 29 / 4 / 2
	// sessionName
	// message
	return TRUE;
}

GSAPI(BOOL) GS_bCheckPassword(LPCSTR playerName, LPCSTR password)
{
	// Arena 74 / 4 / 1
	// playerName
	// password
	return TRUE;
}

GSAPI(BOOL) GS_bConnectTo(LPCSTR ip, WORD port)
{
	return g_client.connectTo(ip, port);
}

GSAPI(BOOL) GS_bConnectionRequest(LPCSTR playerName, LPCSTR password, LPCSTR a3, LPCSTR connectInfo,
	LPCSTR connectVersion, LPCSTR a6, LPCSTR a7)
{
	// Arena 4 / 4 / 1
	// playerName
	// password
	// a3
	// a6
	// a7
	return g_client.connectionRequest(playerName, password, a3, connectInfo, connectVersion, a6, a7);
}

GSAPI(BOOL) GS_bCreateNewUserRequest(LPCSTR name, LPCSTR password, LPCSTR email, LPCSTR country, LPCSTR visitingCard)
{
	// Arena 4 / 1 / 1
	// name
	// password
	// email | ""
	// country | ""
	// visitingCard | ""
	return g_client.createNewUserRequest(name, password, email, country, visitingCard);
}

GSAPI(BOOL) GS_bCreateSession(LPCSTR a1, LPCSTR a2, LPCSTR a3, WORD a4, WORD a5)
{
	// Arena 33 / 4 / 2
	// a1
	// a4 %d
	// a5 %d
	// a2
	// a3
	return TRUE;
}

GSAPI(BOOL) GS_bDeleteUserRequest(LPCSTR a1, LPCSTR a2)
{
	// not used by POD
	// Arena 75 / 4 / 1
	// a1
	// a2
	return TRUE;
}

GSAPI(BOOL) GS_bDisconnectionRequest()
{
	return TRUE;
}

GSAPI(BOOL) GS_bExcludePlayer(LPCSTR a1)
{
	// Arena 56 / 4 / 2
	// a1
	return TRUE;
}

GSAPI(BOOL) GS_bFreeDll()
{
	return TRUE;
}

GSAPI(BOOL) GS_bGameFinish()
{
	// Arena 51 / 4 / 2
	return TRUE;
}

GSAPI(BOOL) GS_bGetLastNews()
{
	// Arena 12 / 4 / 1|2
	return TRUE;
}

GSAPI(BOOL) GS_bGetNewMessage(LPDWORD replyType, LPDWORD queryType)
{
	return FALSE;
}

GSAPI(BOOL) GS_bGetNextArgument(DWORD logID, LPSTR buffer, WORD bufferSize)
{
	return FALSE;
}

GSAPI(BOOL) GS_bGetPlayerInfo(DWORD a1, LPCSTR a2, LPCSTR a3)
{
	// Arena 25 / 4 / 1
	// a1 %d
	// a2
	// a3
	return TRUE;
}

GSAPI(BOOL) GS_bGetPlayers(DWORD a1)
{
	// Arena 21 / 4 / 2
	// a1 %d
	return TRUE;
}

GSAPI(BOOL) GS_bGetRouteurs()
{
	// Arena 76 / 4 / 1
	return TRUE;
}

GSAPI(BOOL) GS_bGetServers(LPCSTR a1, LPCSTR a2)
{
	// Arena 17 / 4 / 1
	// a1
	// a2
	return TRUE;
}

GSAPI(BOOL) GS_bGetSessions(DWORD a1)
{
	// Arena 19 / 4 / 2
	// a1 %d
	return TRUE;
}

GSAPI(BOOL) GS_bInfoPlayer(DWORD a1, LPCSTR a2)
{
	// Arena 25 / 4 / 2
	// a1 %d
	// a2
	return TRUE;
}

GSAPI(BOOL) GS_bInfoSession(DWORD a1, LPCSTR a2)
{
	// Arena 23 / 4 / 2
	// a1 %d
	// a2
	return TRUE;
}

GSAPI(BOOL) GS_bInit(DWORD version)
{
	// version 12724 == full version (not shareware)
	return g_client.init(version);
}

GSAPI(BOOL) GS_bInvitePlayers(DWORD a1, LPCWSTR* playerNames, DWORD playerCount, LPCSTR a4)
{
	// not used by POD
	// Arena 44 / 4 /2
	// a1 %d
	// a4
	// playerNames[playerCount]
	return TRUE;
}

GSAPI(BOOL) GS_bJoinSession(LPCSTR a1)
{
	// Arena 37 / 4 / 2
	// a1
	return TRUE;
}

GSAPI(BOOL) GS_bLeaveSession(LPCSTR a1)
{
	// Arena 41 / 4 / 2
	// a1
	return TRUE;
}

GSAPI(BOOL) GS_bModifySession(DWORD mask, LPCSTR a2, LPCSTR a3, LPCSTR a4, WORD a5, WORD a6)
{
	// Arena 57 / 4 / 2
	// mask %d
	// a2
	// if mask & 0x01: a5 %d
	// if mask & 0x08: a3
	// if mask & 0x40: a4
	// if mask & 0x80: a6 %d
	return TRUE;
}

GSAPI(BOOL) GS_bModifyUserRequest(LPCSTR a1, LPCSTR a2, LPCSTR a3, LPCSTR a4, LPCSTR a5, LPCSTR a6)
{
	// Arena 53 / 4 / 1
	// a1
	// a2
	// a3 | a2
	// a5 | ""
	// a4 | ""
	// a6 | ""
	return TRUE;
}

GSAPI(BOOL) GS_bPingSetProperties(int timeout, int requestCount, int threadPoolSize, int requestSize)
{
	return TRUE;
}

GSAPI(BOOL) GS_bReConnectTo(LPCSTR ip, WORD port)
{
	// not used by POD
	return TRUE;
}

GSAPI(BOOL) GS_bRescueConnection()
{
	// not used by POD
	return TRUE;
}

GSAPI(BOOL) GS_bSearchClient(LPCSTR a1)
{
	// not used by POD
	// Arena 14 / 4 / 1
	// a1
	return TRUE;
}

GSAPI(BOOL) GS_bServiceMessage(LPCSTR* a1, DWORD a2, LPCSTR a3)
{
	// Arena 77 / 4 / 3
	// a1[a2]
	// a3
	return TRUE;
}

GSAPI(BOOL) GS_bSessionResults(LPCSTR a1, DWORD a2, DWORD a3, int count, LPCSTR* a5, LPCSTR* a6, LPCSTR* a7)
{
	// Arena 52 / 4 / 3
	// a1
	// (a5, a7, a6)[count]
	return TRUE;
}

GSAPI(BOOL) GS_bSrvConnectTo(LPCSTR a1, WORD a2, LPCSTR Source, LPCSTR a4, LPCSTR a5, LPCSTR a6, LPCSTR a7)
{
	// not used by POD
	return TRUE;
}

GSAPI(BOOL) GS_bStillAlive()
{
	// Arena 59(shareware)|67(full) / 4 / 3
	return TRUE;
}

GSAPI(int) GS_iConnectReady()
{
	// not used by POD
	return 1;
}

GSAPI(int) GS_iGetLoadLevel()
{
	// not used by POD
	return 1;
}

GSAPI(int) GS_iPingAddress(LPCSTR ip, LPCSTR name, BOOL async)
{
	return 1;
}

GSAPI(DWORD) GS_lEngine(LPCSTR unused)
{
	return 0;
}

GSAPI(DWORD) GS_lGetPlayerId()
{
	// not used by POD
	return 0;
}

GSAPI(void) GS_vAddPlayerToGroup(WORD a1, WORD a2)
{
	// not used by POD
	// Game 11 / 0 / 0
	// info a1
	// info a2
}

GSAPI(void) GS_vCreateGroup(LPCSTR groupName, WORD playerCount, WORD* playerIDs)
{
	// not used by POD
	// Game 21 / 0 / 0
	// info playerIDs[playerCount]
	// data groupName
}

GSAPI(void) GS_vDebug(int debugLevel)
{
	// not used by POD
}

GSAPI(void) GS_vDestroyGroup(WORD groupID)
{
	// not used by POD
	// Game 10 / 0 / 0
	// info groupID
}

GSAPI(void) GS_vEndGameProcess()
{
	// not used by POD
}

GSAPI(void) GS_vFreeMessage()
{
}

GSAPI(void) GS_vGetPlayersId()
{
	// not used by POD
	// Game 15 / 0 / 0
	// info 1
}

GSAPI(void) GS_vGetSessionName(LPCSTR buffer, WORD bufferSize)
{
	// not used by POD
}

GSAPI(void) GS_vNetLibConnectionResult(BOOL useType71)
{
	// Arena 70|71 / 4 / 3
}

GSAPI(void) GS_vPreGameProcess()
{
	// not used by POD
}

GSAPI(void) GS_vQuitSession()
{
	// Game 16 / 0 / ?
	// info g_playerID
}

GSAPI(void) GS_vRmPlayerFromGroup(WORD a1, WORD a2)
{
	// not used by POD
	// Game 12 / 0 / 0
	// info a1
	// info a2
}

GSAPI(void) GS_vSendToAllPlayers(LPCSTR data, WORD dataSize, BOOL type, WORD flags)
{
	// Game type ? 17 : 2 / flags & 0x80 / flags & 0x7F
	// info g_playerID
	// data data
}

GSAPI(void) GS_vSendToGroup(WORD groupID, LPCSTR data, WORD dataSize, BOOL type, WORD flags)
{
	// not used by POD
	// Game type ? 19 : 20 / flags & 0x80 / flags & 0x7F
	// info groupID
	// data data
}

GSAPI(void) GS_vSendToOnePlayer(WORD playerID, LPCSTR data, WORD dataSize, WORD flags)
{
	// Game 3 / flags & 0x80 & flags & 0x7F
	// info playerID
	// data data
}

GSAPI(void) GS_vSendToPlayers(WORD* playerIDs, WORD playerCount, LPCSTR data, WORD dataSize, WORD flags)
{
	// not used by POD
	// Game 4 / flags & 0x80 / flags & 0x7F
	// info playerIds[playerCount]
	// data data
}

typedef void(__cdecl* GS_ADD_PLAYER_TO_GROUP_CB)(WORD, WORD);
GSAPI(void) GS_vSetAddPlayerToGroupCallBack(GS_ADD_PLAYER_TO_GROUP_CB cb)
{
	// not used by POD
}

typedef void(__cdecl* GS_INITIATE_CONNECTION_CB)();
GSAPI(void) GS_vSetInitiateConnectionCallBack(GS_INITIATE_CONNECTION_CB cb)
{
	g_client.setInitiateConnectionCb(cb);
}

typedef void(__cdecl* GS_MSG_RECEIVED_CB)(WORD, LPCSTR, WORD);
GSAPI(void) GS_vSetMsgReceivedCallBack(GS_MSG_RECEIVED_CB cb)
{
}

typedef void(__cdecl* GS_NEW_GROUP_CB)(WORD);
GSAPI(void) GS_vSetNewGroupCallBack(GS_NEW_GROUP_CB cb)
{
}

typedef void(__cdecl* GS_NEW_PLAYER_CB)(WORD, LPCSTR name, LPVOID, WORD, DWORD);
GSAPI(void) GS_vSetNewPlayerCallBack(GS_NEW_PLAYER_CB cb)
{
}

typedef void(__cdecl* GS_RATE_CTRL_CB)(DWORD);
GSAPI(void) GS_vSetRateCtrlCallBack(GS_RATE_CTRL_CB cb)
{
}

typedef void(__cdecl* GS_RM_GROUP_CB)(WORD);
GSAPI(void) GS_vSetRmGroupCallBack(GS_RM_GROUP_CB cb)
{
}

typedef void(__cdecl* GS_RM_PLAYER_CB)(WORD, DWORD);
GSAPI(void) GS_vSetRmPlayerCallBack(GS_RM_PLAYER_CB cb)
{
}

typedef void(__cdecl* GS_RM_PLAYER_FROM_GROUP_CB)(WORD, WORD);
GSAPI(void) GS_vSetRmPlayerFromGroupCallBack(GS_RM_PLAYER_FROM_GROUP_CB cb)
{
	// not used by POD
}

typedef void(__cdecl* GS_SESSION_FINISH_CB)();
GSAPI(void) GS_vSetSessionFinishCallBack(GS_SESSION_FINISH_CB cb)
{
	// not used by POD
}

typedef void(__cdecl* GS_SET_CHANNEL_CB)(WORD port, LPCSTR address);
GSAPI(void) GS_vSetSetChannelCallBack(GS_SET_CHANNEL_CB cb)
{
}

typedef void(__cdecl* GS_SET_PLAYER_CB)(WORD, WORD);
GSAPI(void) GS_vSetSetPlayerCallBack(GS_SET_PLAYER_CB cb)
{
}

GSAPI(WORD) GS_wGetMasterId()
{
	return 1;
}
