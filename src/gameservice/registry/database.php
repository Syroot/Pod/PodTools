<?php

require "credentials.php";
mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

function connect()
{
    // Connect to database.
    global $db_host, $db_user, $db_pass, $db_name;
    $db = new mysqli($db_host, $db_user, $db_pass, $db_name);
    if ($db->connect_error)
        die("Could not connect to database: ". $db->connect_error);

    // Ensure router table exists.
    $db->query("CREATE TABLE IF NOT EXISTS routers(
        id VARCHAR(32) NOT NULL,
        name VARCHAR(30),
        address INT UNSIGNED,
        port SMALLINT UNSIGNED,
        updated DATETIME,
        PRIMARY KEY(id))");

    return $db;
}

function list_all($db)
{
    return $db->query("SELECT name, address, port, updated FROM routers
        WHERE updated IS NOT NULL
            AND updated >= (UTC_TIMESTAMP() - INTERVAL 1 HOUR)
        ORDER BY updated DESC");
}

function register($db, $id, $name, $address, $port)
{
    // Update row.
    $query = $db->prepare("UPDATE routers
        SET name = ?, address = ?, port = ?, updated = UTC_TIMESTAMP()
        WHERE id = ?");
    $query->bind_param("siis", $name, $address, $port, $id);
    $query->execute();
    return $db->affected_rows == 1;
}

function unregister($db, $id)
{
    // Update row.
    $query = $db->prepare("UPDATE routers
        SET updated = NULL
        WHERE id = ?");
    $query->bind_param("s", $id);
    $query->execute();
    return $db->affected_rows == 1;
}

?>
