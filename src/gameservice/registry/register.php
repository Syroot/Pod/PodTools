<?php

error_reporting(-1);

function is_valid_string($string)
{
    return preg_match("/^[0-9A-Za-z_ ]+$/", $string) === 1;
}

// Call as register.php?id=32characterlongkey&name=Abcdef&port=12345

// Validate ID.
$id = $_GET["id"];
echo "ID: " . $id . "<br>";

if (!is_valid_string($id) || strlen($id) != 32)
    throw new Exception("Invalid ID.");

// Validate router name.
$name = $_GET["name"];
echo "Name: " . $name . "<br>";

if (strlen($name) < 3 || strlen($name) > 30)
    throw new Exception("Router name must be between 3 and 30 characters.");
if (!is_valid_string($name))
    throw new Exception("Router name must consist of alphanumeric characters, underscores, or spaces only.");

// Get router address.
$addr = $_SERVER["HTTP_CLIENT_IP"]
    ?? $_SERVER["HTTP_X_FORWARDED_FOR"]
    ?? $_SERVER["HTTP_X_FORWARDED"]
    ?? $_SERVER["HTTP_FORWARDED_FOR"]
    ?? $_SERVER["HTTP_FORWARDED"]
    ?? $_SERVER["REMOTE_ADDR"]
    ?? throw new Exception("Router address could not be determined from your end point.");
echo "Address: " . $addr . "<br>";

$addr = ip2long($addr);

// Get router port.
$port = (int)$_GET["port"];
echo "Port: " . $port . "<br>";

if ($port < 1024 || $port > 65535)
    throw new Exception("Router port must be between 1024 and 65535.");

// Connect to database.
require("database.php");
$db = connect();

// Register router.
if (!register($db, $id, $name, $addr, $port))
    throw new Exception("Registration failed.");

echo "Registered.<br>";

?>
