<?php

error_reporting(-1);

function is_valid_string($string)
{
    return preg_match("/^[0-9A-Za-z_ ]+$/", $string) === 1;
}

// Call as unregister.php?id=32characterlongkey

// Validate ID.
$id = $_GET["id"];
echo "ID: " . $id . "<br>";

if (!is_valid_string($id) || strlen($id) != 32)
    throw new Exception("Invalid ID.");

// Connect to database.
require("database.php");
$db = connect();

// Register router.
if (!unregister($db, $id))
    throw new Exception("Unregistration failed.");

echo "Unregistered.<br>";

?>
