﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;

namespace Syroot.Pod.GameService.Configuration
{
    /// <summary>
    /// Represents the GameService "gs.bin" configuration file.
    /// </summary>
    public class GsConfig
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly byte[] _key = { 0x31, 0x07, 0x19, 0x73 };

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the last used login name.
        /// </summary>
        public string PlayerName { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the last used login password.
        /// </summary>
        public string Password { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the <see cref="GsConfigRouter.Name"/> of the last used login server.
        /// </summary>
        public string LastUsedServer { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the <see cref="GsConfigRouter"/> listed in the login screen.
        /// </summary>
        public IList<GsConfigRouter> Routers { get; set; } = new List<GsConfigRouter>();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the configuration stored in the given <see cref="stream"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to load the configuration from.</param>
        public void Load(Stream stream)
        {
            IList<string> lines = Decrypt(stream);

            PlayerName = lines[0];
            Password = lines[1];
            int routerCount = Int32.Parse(lines[2]);
            LastUsedServer = lines[3];

            Routers.Clear();
            for (int i = 4; i < 4 + routerCount * 3; i += 3)
            {
                Routers.Add(new()
                {
                    Name = lines[i],
                    EndPoint = new IPEndPoint(
                        IPAddress.Parse(lines[i + 1]),
                        UInt16.Parse(lines[i + 2], CultureInfo.InvariantCulture))
                });
            }
        }

        /// <summary>
        /// Saves the configuration into the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">A <see cref="Stream"/> to save the configuration to.</param>
        public void Save(Stream stream)
        {
            List<string> lines = new();

            lines.Add(PlayerName);
            lines.Add(Password);
            lines.Add(Routers.Count.ToString(CultureInfo.InvariantCulture));
            lines.Add(LastUsedServer);

            foreach (GsConfigRouter router in Routers)
            {
                lines.Add(router.Name);
                lines.Add(router.EndPoint.Address.ToString());
                lines.Add(router.EndPoint.Port.ToString(CultureInfo.InvariantCulture));
            }

            Encrypt(stream, lines);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static IList<string> Decrypt(Stream stream)
        {
            List<string> lines = new();

            byte checksum = 0;
            StringBuilder sb = new();
            while (stream.Position < stream.Length)
            {
                byte b = (byte)(_key[stream.Position % 4] ^ stream.ReadByte());
                if (stream.Position % 5 == 0)
                {
                    // Validate checksum.
                    if (b != checksum)
                        throw new InvalidDataException("Checksum mismatch.");
                    checksum = 0;
                }
                else
                {
                    // Handle a byte.
                    if (b == 0xFE)
                    {
                        lines.Add(sb.ToString());
                        sb.Clear();
                    }
                    else
                    {
                        sb.Append((char)b);
                    }
                    checksum += b;
                }
            }

            return lines;
        }

        private static void Encrypt(Stream stream, IList<string> lines)
        {
            byte checksum = 0;

            void writeByte(byte b)
            {
                checksum += b;
                stream.WriteByte((byte)(b ^ _key[stream.Position % 4]));

                // Write checksum.
                if (stream.Position % 5 == 4)
                {
                    stream.WriteByte((byte)(checksum ^ _key[stream.Position % 4]));
                    checksum = 0;
                }
            }

            foreach (string line in lines)
            {
                foreach (char c in line)
                    writeByte((byte)c);
                writeByte(0xFE);
            }
        }
    }
}
