﻿using System;
using System.Net;

namespace Syroot.Pod.GameService.Configuration
{
    /// <summary>
    /// Represents a router stored in a <see cref="GsConfig"/>.
    /// </summary>
    public class GsConfigRouter
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the displayed name.
        /// </summary>
        public string Name { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the remote end point to connect to.
        /// </summary>
        public IPEndPoint EndPoint { get; set; } = new IPEndPoint(IPAddress.Any, 0);
    }
}
