﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;

namespace Syroot.Pod.GameService.RouterRegistry
{
    public class WebRegistry : IRouterRegistry
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly string _url = "https://pod.syroot.com/gs/";

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly List<RegisteredRouter> _routerCache = new();
        private DateTime _routerCacheTime;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public async ValueTask<string> GetFaqs()
        {
            using WebClient webClient = new();
            return await webClient.DownloadStringTaskAsync(_url + "faqs.txt");
        }

        /// <inheritdoc/>
        public async ValueTask<IList<RegisteredRouter>> GetRouters()
        {
            // Update list if older than 1 hour.
            DateTime updateTime = DateTime.Now;
            if ((updateTime - _routerCacheTime).TotalHours > 1)
            {
                _routerCacheTime = updateTime;

                // Download the list from the web service.
                string[] lines;
                using (WebClient webClient = new())
                    lines = (await webClient.DownloadStringTaskAsync(_url)).Split("<br>");

                // Parse router instances out of it.
                _routerCache.Clear();
                foreach (string line in lines)
                {
                    if (String.IsNullOrWhiteSpace(line))
                        continue;
                    string[] parts = line.Split(",");
                    _routerCache.Add(new()
                    {
                        Name = parts[0],
                        EndPoint = IPEndPoint.Parse(parts[1]),
                        Updated = DateTime.ParseExact(parts[2], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture)
                    });
                }
            }

            return _routerCache;
        }

        /// <inheritdoc/>
        public async ValueTask Register(string id, string name, int port)
        {
            string result;
            using (WebClient webClient = new())
                result = await webClient.DownloadStringTaskAsync(_url + $"register.php?id={id}&name={name}&port={port}");

            if (result.Contains("error", StringComparison.OrdinalIgnoreCase))
                throw new WebException($"Registration failed. {result}.");
        }

        /// <inheritdoc/>
        public async ValueTask Unregister(string id)
        {
            string result;
            using (WebClient webClient = new())
                result = await webClient.DownloadStringTaskAsync(_url + $"unregister.php?id={id}");

            if (result.Contains("error", StringComparison.OrdinalIgnoreCase))
                throw new WebException($"Unregistration failed. {result}.");
        }
    }
}
