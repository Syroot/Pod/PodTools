﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Syroot.Pod.GameService.RouterRegistry
{
    /// <summary>
    /// Represents an external collection of GameService routers which are stored in the users GS configuration file to
    /// initially connect to.
    /// </summary>
    public interface IRouterRegistry
    {
        /// <summary>
        /// Gets the GameService notes.
        /// </summary>
        /// <returns>The text representing the GameService notes.</returns>
        ValueTask<string> GetFaqs();

        /// <summary>
        /// Gets the complete list of known routers stored in the external registry.
        /// </summary>
        /// <returns>A task completing when the list of known routers is returned.</returns>
        ValueTask<IList<RegisteredRouter>> GetRouters();

        /// <summary>
        /// Registers this router with the given settings and credentials.
        /// </summary>
        /// <param name="id">A key to access the registry with.</param>
        /// <param name="name">A name displayed to the user in the login screen.</param>
        /// <param name="port">A port at which the router will listen.</param>
        /// <returns>A task completing when registration finished.</returns>
        ValueTask Register(string id, string name, int port);

        /// <summary>
        /// Removes this router with the given credentials.
        /// </summary>
        /// <param name="id">A key to access the registry with.</param>
        /// <returns>A task completing when unregistration finished.</returns>
        ValueTask Unregister(string id);
    }
}
