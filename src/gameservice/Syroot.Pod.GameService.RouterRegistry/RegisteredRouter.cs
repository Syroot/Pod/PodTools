﻿using System;
using System.Net;

namespace Syroot.Pod.GameService.RouterRegistry
{
    public record RegisteredRouter
    {
        public string Name { get; set; } = String.Empty;

        public IPEndPoint EndPoint { get; set; } = new IPEndPoint(IPAddress.Any, 0);

        public DateTime Updated { get; set; }
    }
}
