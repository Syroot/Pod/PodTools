﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Syroot.Pod.GameService.Messages
{
    /// <summary>
    /// Represents an <see cref="ArenaMessage"/> data formatter.
    /// </summary>
    public static class ArenaNode
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly byte[] _listStart = GsEncoding.Instance.GetBytes(" [ ");
        private static readonly byte[] _listSeparator = GsEncoding.Instance.GetBytes(" ");
        private static readonly byte[] _listEnd = GsEncoding.Instance.GetBytes(" ] ");

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Serializes the given enumerable list of objects or <see langword="null"/> to raw bytes.
        /// </summary>
        /// <param name="node">An enumerable list of objects to serialize or <see langword="null"/>.</param>
        /// <returns>The serialized bytes.</returns>
        public static ReadOnlySpan<byte> Serialize(IEnumerable? node)
        {
            MemoryStream stream = new();
            if (node != null)
                SerializeList(node, stream);
            stream.WriteByte(0);
            return stream.GetBuffer().AsSpan(..(int)stream.Length);
        }

        /// <summary>
        /// Deserializes a list of objects or <see langword="null"/>.
        /// </summary>
        /// <param name="data">A <see cref="ReadOnlySpan{Byte}"/> to deserialize from.</param>
        /// <returns>The deserialized list of objects or <see langword="null"/>.</returns>
        public static IList<object>? Deserialize(ReadOnlySpan<byte> data)
        {
            return DeserializeNode(ref data) as IList<object>;
        }

        /// <summary>
        /// Serializes the given enumerable list of objects or <see langword="null"/> to a readable string.
        /// </summary>
        /// <param name="node">An enumerable list of objects to visualize or <see langword="null"/></param>
        /// <returns>The readable string.</returns>
        public static string Visualize(object? node) => node switch
        {
            Enum enumValue => VisualizeString(enumValue.ToString("D")),
            string stringValue => VisualizeString(stringValue),
            IEnumerable enumerableValue => VisualizeList(enumerableValue),
            null => String.Empty,
            _ => VisualizeString(node?.ToString())
        };

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void SerializeNode(object? value, MemoryStream stream)
        {
            switch (value)
            {
                case string stringValue: SerializeString(stringValue, stream); break;
                case IEnumerable enumerableValue: SerializeList(enumerableValue, stream); break;
                case Enum enumValue: SerializeString(enumValue.ToString("D"), stream); break;
                default: SerializeString(value?.ToString(), stream); break;
            }
        }

        private static void SerializeList(IEnumerable value, MemoryStream stream)
        {
            stream.Write(_listStart);
            foreach (object element in value)
            {
                SerializeNode(element, stream);
                stream.Write(_listSeparator);
            }
            stream.Write(_listEnd);
        }

        private static void SerializeString(string? value, MemoryStream stream)
        {
            stream.WriteByte((byte)'"');

            if (value != null)
            {
                Span<byte> bytes = stackalloc byte[GsEncoding.Instance.GetByteCount(value)];
                GsEncoding.Instance.GetBytes(value, bytes);
                for (int i = 0; i < bytes.Length; i++)
                    if (bytes[i] == '"')
                        bytes[i] = 0xFE;
                stream.Write(bytes);
            }

            stream.WriteByte((byte)'"');
        }

        private static object? DeserializeNode(ref ReadOnlySpan<byte> span) => GetNextToken(ref span) switch
        {
            Token.ListBegin => DeserializeList(ref span),
            Token.StringQuote => DeserializeString(ref span),
            _ => null
        };

        private static IList<object> DeserializeList(ref ReadOnlySpan<byte> span)
        {
            List<object> node = new();

            while (!span.IsEmpty)
            {
                Token token = GetNextToken(ref span);
                switch (token)
                {
                    case Token.ListBegin: node.Add(DeserializeList(ref span)); break;
                    case Token.StringQuote: node.Add(DeserializeString(ref span)); break;
                    case Token.ListEnd: return node;
                    default: throw new InvalidDataException("Invalid list token.");
                }
            }

            throw new InvalidDataException("Incomplete list.");
        }

        private static string DeserializeString(ref ReadOnlySpan<byte> span)
        {
            MemoryStream buffer = new();

            while (!span.IsEmpty)
            {
                byte b = span[0];
                span = span[1..];
                switch (b)
                {
                    case (byte)'\0':
                        throw new InvalidDataException("Invalid zero-terminated string.");
                    case (byte)'"':
                        return GsEncoding.Instance.GetString(buffer.GetBuffer().AsSpan(..(int)buffer.Length));
                    default:
                        buffer.WriteByte(b);
                        break;
                }
            }

            throw new InvalidDataException("Incomplete string.");
        }

        private static Token GetNextToken(ref ReadOnlySpan<byte> span)
        {
            while (!span.IsEmpty)
            {
                byte b = span[0];
                span = span[1..];
                switch (b)
                {
                    case (byte)'\0':
                        return Token.End;
                    case (byte)' ':
                    case (byte)'\t':
                    case (byte)'\b':
                    case (byte)'\r':
                    case (byte)'\n':
                        continue;
                    case (byte)'[':
                        return Token.ListBegin;
                    case (byte)']':
                        return Token.ListEnd;
                    case (byte)'"':
                        return Token.StringQuote;
                }
            }
            return Token.End;
        }

        private static string VisualizeList(IEnumerable value)
        {
            StringBuilder sb = new("[ ");
            foreach (object element in value)
            {
                sb.Append(Visualize(element));
                sb.Append(' ');
            }
            sb.Append(']');
            return sb.ToString();
        }

        private static string VisualizeString(string? value)
        {
            return $"\"{value?.Replace(@"\", @"\\").Replace("\"", "\\\"")}\"";
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private enum Token
        {
            End,
            ListBegin,
            ListEnd,
            StringQuote
        }
    }
}
