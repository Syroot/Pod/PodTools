﻿using System;
using System.Collections.Generic;

namespace Syroot.Pod.GameService.Messages
{
    /// <summary>
    /// Represents the contents of an arena packet.
    /// </summary>
    public class ArenaMessage : IGsMessage
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public ArenaMessage(ArenaMessageType type, ArenaMachine source, ArenaMachine target, IList<object>? args = null)
        {
            Type = type;
            Source = source;
            Target = target;
            Args = args;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the classification of the message data.
        /// </summary>
        public ArenaMessageType Type { get; set; }

        /// <summary>
        /// Gets or sets the classification of the participant from which this message originates.
        /// </summary>
        public ArenaMachine Source { get; set; }

        /// <summary>
        /// Gets or sets the classification of the participant(s) by which this message should be received.
        /// </summary>
        public ArenaMachine Target { get; set; }

        /// <summary>
        /// Gets or sets optional hierarchical data.
        /// </summary>
        public IList<object>? Args { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override string ToString() => String.Join(' ', Type, ArenaNode.Visualize(Args));
    }

    /// <summary>
    /// Represents the possible sources and targets of an <see cref="ArenaMessage"/>.
    /// </summary>
    [Flags]
    public enum ArenaMachine : ushort
    {
        Router = 1 << 0,
        Server = 1 << 1,
        Client = 1 << 2
    }

    /// <summary>
    /// Represents which information is carried in an <see cref="ArenaMessage"/>.
    /// </summary>
    public enum ArenaMessageType : ushort
    {
        Connect = 0,
        NewUserRequest = 1,
        ConnectionRequest = 4,
        PlayerNew = 6,
        Disconnection = 10,
        PlayerRemoved = 11,
        GetLastNews = 12,
        News = 13,
        SearchPlayer = 14,
        RemoveAccount = 15,
        GetServers = 17,
        ServerList = 18,
        GetSessions = 19,
        SessionList = 20,
        GetPlayers = 21,
        PlayerList = 22,
        GetSessionInfo = 23,
        SessionInfo = 24,
        GetPlayerInfo = 25,
        PlayerInfo = 26,
        ChatAll = 27,
        ChatList = 28,
        ChatSession = 29,
        ChatPlayer = 30,
        Chat = 31,
        GeneralChat = 32,
        CreateSession = 33,
        SessionNew = 36,
        JoinSession = 37,
        LoadLevel = 38,
        JoinNew = 40,
        LeaveSession = 41,
        JoinLeave = 42,
        SessionRemoved = 43,
        InvitePlayers = 44,
        Success = 47,
        Fail = 48,
        BeginGame = 49,
        GameBegun = 50,
        GameFinish = 51,
        SessionResults = 52,
        ModifyUserRequest = 53,
        UpdatePlayerInfo = 54,
        ExcludePlayer = 56,
        ModifySession = 57,
        Connect_Shareware = 58,
        StillAlive_Shareware = 59,
        UpdateAllSession = 60,
        UrgentMessage = 61,
        ChangeModule = 62,
        NewWaitModule = 63,
        KillModule = 64,
        PlayerId = 65,
        StillAlive = 67,
        ConnectPlayer = 68,
        AddPlayer = 69,
        NetLibConnectionResult1 = 70,
        NetLibConnectionResult2 = 71,
        GetPort = 72,
        CheckPassword = 74,
        DeleteUserRequest = 75,
        GetRouters = 76,
        ServiceMessage = 77,
        RouterList = 78,
        SessionModify = 79,
        PingValue = 100
    }
}
