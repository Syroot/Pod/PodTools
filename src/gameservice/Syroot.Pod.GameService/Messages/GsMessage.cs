﻿namespace Syroot.Pod.GameService.Messages
{
    /// <summary>
    /// Represents the base interface for any unit of data sent through the GameService.
    /// </summary>
    public interface IGsMessage
    {
    }
}
