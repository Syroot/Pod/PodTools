﻿namespace Syroot.Pod.GameService.Messages
{
    public class GameMessage : IGsMessage
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override string ToString() => "Game ?";
    }
}
