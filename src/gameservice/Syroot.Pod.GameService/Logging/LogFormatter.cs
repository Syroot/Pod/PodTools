﻿using System;
using System.IO;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Logging.Console;

namespace Syroot.Pod.GameService.Logging
{
    /// <summary>
    /// Represents a custom console formatter for colorizing router logging.
    /// </summary>
    public sealed class LogFormatter : ConsoleFormatter
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static readonly ConsoleColor[] _levelColors = new[]
        {
            ConsoleColor.DarkGray,
            ConsoleColor.Gray,
            ConsoleColor.White,
            ConsoleColor.Yellow,
            ConsoleColor.DarkRed,
            ConsoleColor.Red
        };

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public LogFormatter() : base("Router")
        {
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public override void Write<TState>(in LogEntry<TState> logEntry, IExternalScopeProvider scopeProvider,
            TextWriter textWriter)
        {
            // Write colorized level tag.
            if (logEntry.LogLevel != LogLevel.None)
                textWriter.SetColor(_levelColors[(int)logEntry.LogLevel]);

            // Write time stamp.
            textWriter.Write($"{DateTime.Now:HH:mm:ss} ");

            // Write category.
            textWriter.Write(logEntry.Category);
            textWriter.Write(" | ");

            // Write colorized send or receive message.
            string message = logEntry.Formatter(logEntry.State, logEntry.Exception);
            if (message.StartsWith("C>"))
                textWriter.SetColor(ConsoleColor.Blue);
            else if (message.StartsWith("R>") || message.StartsWith("S>"))
                textWriter.SetColor(ConsoleColor.Magenta);
            textWriter.Write(message);
            textWriter.ResetColor();
            textWriter.WriteLine();
        }
    }

    internal static class TextWriterExtensions
    {
        private static readonly string[] _fgEscapeCodes = new[]
        {
            "\x1B[30m", // Black
            "\x1B[34m", // DarkBlue
            "\x1B[32m", // DarkGreen
            "\x1B[36m", // DarkCyan
            "\x1B[31m", // DarkRed
            "\x1B[35m", // DarkMagenta
            "\x1B[33m", // DarkYellow
            "\x1B[37m", // Gray
            "\x1B[37m", // DarkGray (uses Gray)
            "\x1B[1m\x1B[34m", // Blue
            "\x1B[1m\x1B[32m", // Green
            "\x1B[1m\x1B[36m", // Cyan
            "\x1B[1m\x1B[31m", // Red
            "\x1B[1m\x1B[35m", // Magenta
            "\x1B[1m\x1B[33m", // Yellow
            "\x1B[1m\x1B[37m", // White
        };

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static void ResetColor(this TextWriter writer)
        {
            writer.Write("\x1B[39m\x1B[22m");
        }

        internal static void SetColor(this TextWriter writer, ConsoleColor color)
        {
            writer.Write(_fgEscapeCodes[(int)color]);
        }
    }
}
