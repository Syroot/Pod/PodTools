﻿namespace Syroot.Pod.GameService.Services
{
    /// <summary>
    /// Represents methods to salt and hash passwords.
    /// </summary>
    public interface IPasswordHasher
    {
        /// <summary>
        /// Hashes and salts the given <paramref name="password"/>, using the provided <paramref name="salt"/>,
        /// resulting in the <paramref name="hash"/>.
        /// </summary>
        /// <param name="password">The text to hash.</param>
        /// <param name="salt">The resulting salt.</param>
        /// <param name="hash">The resulting hash.</param>
        void Hash(string password, out string hash, out string salt);

        /// <summary>
        /// Returns a value indicating whether the given <paramref name="password"/> yields the
        /// <paramref name="expectedHash"/> by using the given <paramref name="salt"/>.
        /// </summary>
        /// <param name="password">The text to validate.</param>
        /// <param name="expectedHash">The expected hash to return success.</param>
        /// <param name="salt">The salt used to hash the text with.</param>
        /// <returns><see langword="true"/> if the hash equals the expected one.</returns>
        bool Validate(string password, string expectedHash, string salt);
    }
}
