﻿using System;
using System.Net;

namespace Syroot.Pod.GameService.Services
{
    internal static class IPResolver
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static IPAddress _publicAddress = IPAddress.Any;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static IPAddress ResolveAddress(string text)
        {
            // Handle keywords "public" (external), "private" (loopback), and "any2.
            if (text.Equals("any", StringComparison.OrdinalIgnoreCase))
            {
                return IPAddress.Any;
            }
            else if (text.Equals("private", StringComparison.OrdinalIgnoreCase))
            {
                return IPAddress.Loopback;
            }
            else if (text.Equals("public", StringComparison.OrdinalIgnoreCase))
            {
                if (_publicAddress == IPAddress.Any)
                {
                    using WebClient webClient = new();
                    _publicAddress = IPAddress.Parse(webClient.DownloadString("https://ip.syroot.com"));
                }
                return _publicAddress;
            }
            else
            {
                return IPAddress.Parse(text);
            }
        }

        internal static IPEndPoint ResolveEndPoint(string text)
        {
            string[] parts = text.Split(':');
            return new IPEndPoint(ResolveAddress(parts[0]), UInt16.Parse(parts[1]));
        }
    }
}
