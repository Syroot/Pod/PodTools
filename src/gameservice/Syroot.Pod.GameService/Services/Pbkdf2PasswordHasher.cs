﻿using System;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace Syroot.Pod.GameService.Services
{
    /// <summary>
    /// Represents methods to salt and hash passwords.
    /// </summary>
    public class Pbkdf2PasswordHasher : IPasswordHasher
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Hashes and salts the given <paramref name="password"/>, using the provided <paramref name="salt"/>,
        /// resulting in the <paramref name="hash"/>.
        /// </summary>
        /// <param name="password">The text to hash.</param>
        /// <param name="salt">The resulting salt.</param>
        /// <param name="hash">The resulting hash.</param>
        public void Hash(string password, out string hash, out string salt)
        {
            // Create a new salt and calculate the hash.
            byte[] saltBytes = GetRandomBytes(16);
            byte[] hashBytes = GetHash(password, saltBytes);

            // Encode in strings.
            salt = Convert.ToBase64String(saltBytes);
            hash = Convert.ToBase64String(hashBytes);
        }

        /// <summary>
        /// Returns a value indicating whether the given <paramref name="password"/> yields the
        /// <paramref name="expectedHash"/> by using the given <paramref name="salt"/>.
        /// </summary>
        /// <param name="password">The text to validate.</param>
        /// <param name="expectedHash">The expected hash to return success.</param>
        /// <param name="salt">The salt used to hash the text with.</param>
        /// <returns><see langword="true"/> if the hash equals the expected one.</returns>
        public bool Validate(string password, string expectedHash, string salt)
        {
            // Calculate the hash for the given password.
            byte[] saltBytes = Convert.FromBase64String(salt);
            byte[] hashBytes = GetHash(password, saltBytes);
            return expectedHash == Convert.ToBase64String(hashBytes);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static byte[] GetHash(string password, byte[] saltBytes)
            => KeyDerivation.Pbkdf2(password, saltBytes, KeyDerivationPrf.HMACSHA512, 10000, 32);

        private static byte[] GetRandomBytes(int count)
        {
            byte[] bytes = new byte[count];
            using RandomNumberGenerator rng = RandomNumberGenerator.Create();
            rng.GetBytes(bytes);
            return bytes;
        }
    }
}
