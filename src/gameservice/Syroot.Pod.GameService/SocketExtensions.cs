﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.Pod.GameService
{
    /// <summary>
    /// Represents extension methods for <see cref="Socket"/> instances.
    /// </summary>
    internal static class SocketExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Accepts a pending connection request as an asynchronous operation.
        /// </summary>
        /// <param name="socket">The <see cref="Socket"/> instance.</param>
        /// <returns>A task object representing the asynchronous operation. The <see cref="Task{TcpClient}.Result"/>
        /// property on the task object returns a <see cref="TcpClient"/> used to send and receive data.</returns>
        internal static async Task<Socket> AcceptAsync(this Socket socket, CancellationToken cancellationToken)
        {
            using (cancellationToken.Register(() => socket.Dispose()))
            {
                try
                {
                    return await socket.AcceptAsync();
                }
                catch (InvalidOperationException)
                {
                    cancellationToken.ThrowIfCancellationRequested();
                    throw;
                }
            }
        }
    }
}
