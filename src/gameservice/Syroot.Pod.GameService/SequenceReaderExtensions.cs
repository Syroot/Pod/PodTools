﻿using System.Buffers;

namespace Syroot.Pod.GameService
{
    /// <summary>
    /// Represents extension methods for <see cref="SequenceReader{Byte}"/> instances.
    /// </summary>
    public static class SequenceReaderExtensions
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public static bool TryRead(ref this SequenceReader<byte> reader, out sbyte value)
        {
            value = default;
            if (!reader.TryRead(out byte unsignedValue))
                return false;
            value = (sbyte)unsignedValue;
            return true;
        }

        public static bool TryReadBigEndian(ref this SequenceReader<byte> reader, out ushort value)
        {
            value = default;
            if (!reader.TryReadBigEndian(out short signedValue))
                return false;
            value = (ushort)signedValue;
            return true;
        }
    }
}
