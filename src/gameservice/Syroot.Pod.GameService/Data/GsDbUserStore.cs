﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Syroot.Pod.GameService.Services;

namespace Syroot.Pod.GameService.Data
{
    public class GsDbUserStore : IUserStore
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IDbContextFactory<GsDbContext> _dbFactory;
        private readonly IPasswordHasher _passwordHasher;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public GsDbUserStore(IDbContextFactory<GsDbContext> dbFactory, IPasswordHasher passwordHasher)
        {
            _dbFactory = dbFactory;
            _passwordHasher = passwordHasher;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public User? Authenticate(string username, string password)
        {
            // Validate parameters.
            ValidateUsername(username);
            ValidatePassword(password);

            // User must exist.
            User? user = _dbFactory.CreateDbContext().Users.FirstOrDefault(x => x.Name == username);
            if (user == null)
                return null;

            // Password must match.
            if (!_passwordHasher.Validate(password, user.PasswordHash, user.PasswordSalt))
                return null;

            return user;
        }

        public async ValueTask Create(string username, string password,
            string? email = null, string? country = null, string? visitingCard = null)
        {
            // Validate parameters.
            ValidateUsername(username);
            ValidatePassword(password);

            // User name must be unique.
            using GsDbContext db = _dbFactory.CreateDbContext();
            if (db.Users.Any(x => x.Name.ToLower() == username.ToLower()))
                throw new ArgumentException("User already exists.");

            // Create profile.
            _passwordHasher.Hash(password, out string hash, out string salt);
            User user = new(username, hash, salt);
            user.Email = email;
            user.Country = country;
            user.VisitingCard = visitingCard;
            db.Users.Add(user);
            await db.SaveChangesAsync();
        }

        public User? Find(string username)
        {
            using GsDbContext db = _dbFactory.CreateDbContext();
            return db.Users.FirstOrDefault(x => x.Name == username);
        }

        public async ValueTask Modify(string username, string password, string? newPassword = null,
            string? email = null, string? country = null, string? visitingCard = null)
        {
            // Authenticate user.
            User? user = Authenticate(username, password);
            if (user == null)
                throw new ArgumentException("Wrong username or password.");

            // Modify profile.
            using GsDbContext db = _dbFactory.CreateDbContext();
            user = db.Users.First(x => x.Name == user.Name);
            _passwordHasher.Hash(password, out string hash, out string salt);
            user.PasswordHash = hash;
            user.PasswordSalt = salt;
            user.Email = email;
            user.Country = country;
            user.VisitingCard = visitingCard;
            await db.SaveChangesAsync();
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static void ValidatePassword(string password)
        {
            if (password.Length is < 8 or > 8)
                throw new ArgumentException("Invalid password (must be 8 characters).");
        }

        private static void ValidateUsername(string username)
        {
            if (username.Length < 3)
                throw new ArgumentException("Invalid username (must be at least 3 characters).");
        }
    }
}
