﻿using System.Threading.Tasks;

namespace Syroot.Pod.GameService.Data
{
    public interface IUserStore
    {
        User? Authenticate(string username, string password);

        ValueTask Create(string username, string password,
            string? email = null, string? country = null, string? visitingCard = null);

        User? Find(string username);

        ValueTask Modify(string username, string password, string? newPassword = null,
            string? email = null, string? country = null, string? visitingCard = null);
    }
}
