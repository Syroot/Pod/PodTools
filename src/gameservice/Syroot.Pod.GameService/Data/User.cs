﻿using System.ComponentModel.DataAnnotations;

namespace Syroot.Pod.GameService.Data
{
    /// <summary>
    /// Represents a registered player account.
    /// </summary>
    public record User
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="User"/> record with the given values.
        /// </summary>
        /// <param name="name">A value for the <see cref="Name"/> property.</param>
        /// <param name="passwordHash">A value for the <see cref="PasswordHash"/> property.</param>
        /// <param name="passwordSalt">A value for the <see cref="PasswordSalt"/> property.</param>
        public User(string name, string passwordHash, string passwordSalt)
        {
            Name = name;
            PasswordHash = passwordHash;
            PasswordSalt = passwordSalt;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the login and display name of the player.
        /// </summary>
        [Key]
        [MaxLength(8)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the hash of the login password of the player.
        /// </summary>
        public string PasswordHash { get; set; }

        /// <summary>
        /// Gets or sets the salt of the login password of the player.
        /// </summary>
        public string PasswordSalt { get; set; }

        /// <summary>
        /// Gets or sets the optional email address of the player.
        /// </summary>
        public string? Email { get; set; }

        /// <summary>
        /// Gets or sets the optional country or town of the player.
        /// </summary>
        public string? Country { get; set; }

        /// <summary>
        /// Gets or sets the optional memo displayed in the user profile.
        /// </summary>
        public string? VisitingCard { get; set; }

        /// <summary>
        /// Gets or sets the ladder score.
        /// </summary>
        public int Score { get; set; }
    }
}
