﻿using Microsoft.EntityFrameworkCore;

namespace Syroot.Pod.GameService.Data
{
    /// <summary>
    /// Represents the Entity Framework database access.
    /// </summary>
    /// <remarks>
    /// To create or update an initial database, run Update-Database.
    /// To create code for updating an existing database, run Add-Migration.
    /// S. https://docs.microsoft.com/en-us/ef/core/get-started/?tabs=visual-studio#create-the-database for more info.
    /// </remarks>
    public sealed class GsDbContext : DbContext
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public GsDbContext(DbContextOptions<GsDbContext> options)
            : base(options) { }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the registered player accounts.
        /// </summary>
        public DbSet<User> Users => Set<User>();

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) => base.OnConfiguring(optionsBuilder);
    }
}
