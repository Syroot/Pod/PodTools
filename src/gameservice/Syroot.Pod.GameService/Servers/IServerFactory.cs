﻿using Microsoft.Extensions.Options;

namespace Syroot.Pod.GameService.Servers
{
    public interface IServerFactory
    {
        // ---- METHODS ------------------------------------------------------------------------------------------------

        Server Create(IOptions<ServerOptions> options);
    }
}
