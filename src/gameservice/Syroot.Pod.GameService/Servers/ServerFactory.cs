﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Syroot.Pod.GameService.Servers
{
    public class ServerFactory : IServerFactory
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IServiceProvider _serviceProvider;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public ServerFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public Server Create(IOptions<ServerOptions> options)
        {
            return ActivatorUtilities.CreateInstance<Server>(_serviceProvider, options);
        }
    }
}
