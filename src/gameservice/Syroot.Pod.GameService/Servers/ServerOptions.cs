﻿using System.Collections.Generic;
using Syroot.Pod.GameService.Net;
using Syroot.Pod.GameService.Messages;

namespace Syroot.Pod.GameService.Servers
{
    /// <summary>
    /// Represents the settings for one server.
    /// </summary>
    public class ServerOptions : HosterOptions
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int PingLimit { get; set; }

        public List<string> News { get; set; } = new();

        internal override ArenaMachine Machine => ArenaMachine.Server;
    }
}
