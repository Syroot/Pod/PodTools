﻿using System;
using System.Collections.Generic;
using Syroot.Pod.GameService.Net;

namespace Syroot.Pod.GameService.Servers
{
    /// <summary>
    /// Represents the state of a game session.
    /// </summary>
    public class Session
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Session(string name, Client master)
        {
            Name = name;
            Master = master;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name { get; set; }

        public int MaxPlayerCount { get; set; }

        public Client Master { get; set; }

        public IList<Client> Players { get; set; } = new List<Client>();

        public string Infos { get; set; } = String.Empty;

        public bool Open { get; set; }

        public bool InRace { get; set; }
    }
}
