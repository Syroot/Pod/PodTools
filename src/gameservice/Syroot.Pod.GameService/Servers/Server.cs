﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Syroot.Pod.GameService.Data;
using Syroot.Pod.GameService.Messages;
using Syroot.Pod.GameService.Net;

namespace Syroot.Pod.GameService.Servers
{
    /// <summary>
    /// Represents a server in which clients meet and create sessions.
    /// </summary>
    public class Server : Hoster
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IUserStore _userStore;
        private readonly List<Session> _sessions = new();

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Server(
            IMessageDispatcher dispatcher,
            IListener listener,
            ILoggerFactory loggerFactory,
            IOptions<ServerOptions> options,
            IProtocol<IGsMessage> protocol,
            IUserStore userStore)
            : base(dispatcher, listener, loggerFactory, options, protocol)
        {
            Options = options.Value;
            _userStore = userStore;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal ServerOptions Options { get; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        [ArenaHandler(ArenaMessageType.BeginGame)]
        public async ValueTask HandleBeginGame(Client sender, ArenaMessage message)
        {
            // Validate query.
            Session? session = sender.Session;
            if (session == null)
                throw new ArgumentException("Sender is not in a session.");
            if (session.Master.PlayerName != sender.PlayerName)
                throw new ArgumentException("Sender is not master of the session.");

            session.InRace = true;

            IList<Client> sessionClients = Clients.Where(x => x.Session == session).ToList();

            // TODO: Tell each client their own ID.
            foreach (Client client in sessionClients)
                await Send(client, ArenaMessageType.PlayerId, new object[] { client.SessionID });

            // TODO: Connect players to each other.
            foreach (Client toClient in sessionClients)
            {
                foreach (Client otherClient in sessionClients.Where(x => x != toClient))
                {
                    IPEndPoint otherEndPoint = (IPEndPoint)toClient.Connection.Socket.RemoteEndPoint!;
                    await Send(toClient, ArenaMessageType.ConnectPlayer, new object[]
                        {
                            otherClient.SessionID,
                            new object[] {
                                new object[] { otherEndPoint.Port, otherEndPoint.Address }
                            }
                        });
                }
            }

            // TODO: Add players.
            List<object> players = new();
            foreach (Client client in sessionClients)
                players.Add(new object[] { client.SessionID, client.PlayerName, "?", session.Master == client ? 1 : 0 });
            foreach (Client client in sessionClients)
                await Send(client, ArenaMessageType.AddPlayer, players);

            // Update all clients.
            foreach (Client client in Clients)
            {
                if (session.Players.Contains(client))
                    await Send(client, ArenaMessageType.GameBegun);
                else
                    await Send(client, ArenaMessageType.GameBegun, new object[] { session.Name });
            }
        }

        [ArenaHandler(ArenaMessageType.ChatAll)]
        public async ValueTask HandleChatAll(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            string text = ArgS(args[0]);

            // Update all server clients.
            foreach (Client client in Clients)
                await Send(client, ArenaMessageType.GeneralChat, new object[] { sender.PlayerName, text });
        }

        [ArenaHandler(ArenaMessageType.ChatList)]
        public async ValueTask HandleChatList(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            IList<string> receivers = ArgSList(args[0]);
            string text = ArgS(args[1]);

            // Update all participating server clients.
            object[] reply = new object[] { sender.PlayerName, text };
            foreach (Client client in Clients.Where(x => receivers.Contains(x.PlayerName)))
                await Send(client, ArenaMessageType.Chat, reply);
        }

        [ArenaHandler(ArenaMessageType.ConnectionRequest), Unauthenticated]
        public async ValueTask HandleConnectionRequest(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            string username = ArgS(args[0]);
            string password = ArgS(args[1]);
            string serverName = ArgS(args[2]);
            string gameName = ArgS(args[3]);
            string gameVersion = ArgS(args[4]);

            // Authenticate.
            User? user = _userStore.Authenticate(username, password);
            if (user == null)
            {
                await SendFail(sender, ArenaMessageType.ConnectionRequest, "Wrong username or password.");
                return;
            }

            // Server name must match.
            if (serverName != Options.Name)
            {
                await SendFail(sender, ArenaMessageType.ConnectionRequest, "Server not found.");
                return;
            }

            // Move in client.
            sender.PlayerName = user.Name;
            sender.GameConfig = ((string)args[5]).ToUpperInvariant();
            Logger.LogInformation("{EndPoint} authenticated as {Username}",
                ((IPEndPoint?)sender.Connection.Socket.RemoteEndPoint)?.ToString(), sender.PlayerName);

            // Reply sender.
            await SendSuccess(sender, ArenaMessageType.ConnectionRequest);

            // Update other server clients.
            IList<object> reply = GetPlayerInfo(sender, user, PlayerMask.Score | PlayerMask.Shareware);
            foreach (Client client in Clients.Where(x => x != sender))
                await Send(client, ArenaMessageType.PlayerNew, reply);
        }

        [ArenaHandler(ArenaMessageType.CreateSession)]
        public async ValueTask HandleCreateSession(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            string name = ArgS(args[0]);
            int maxPlayers = Math.Clamp(ArgI(args[1]), 2, 8);
            int unknown2 = ArgI(args[2]); // 17
            string unknown3 = ArgS(args[3]); // ?
            string config = ArgS(args[4]);

            if (unknown2 != 17)
                throw new ArgumentException("Unexpected unknown value.");

            // Session name must be unique.
            if (_sessions.Any(x => x.Name == name))
            {
                await SendFail(sender, ArenaMessageType.CreateSession, "Session already exists.");
                return;
            }

            // Create session and move in sender.
            Session session = new(name, sender)
            {
                MaxPlayerCount = maxPlayers,
                Players = { sender },
                Infos = config,
                Open = true
            };
            _sessions.Add(session);
            sender.Session = session;

            // Reply sender.
            await SendSuccess(sender, ArenaMessageType.CreateSession, session.Name, /*stay*/String.Empty, 0);

            // Update other server clients.
            IList<object> sessionInfo = GetSessionInfo(session, SessionMask.MaxPlayerCount | SessionMask.PlayerCount
                | SessionMask.MasterName | SessionMask.PlayerList | SessionMask.OpenPrivate);
            foreach (Client client in Clients.Where(x => x != sender))
                await Send(client, ArenaMessageType.SessionNew, sessionInfo);
        }

        [ArenaHandler(ArenaMessageType.GetLastNews)]
        public async ValueTask HandleGetLastNews(Client sender, ArenaMessage message)
        {
            // Reply sender.
            await Send(sender, ArenaMessageType.News, Options.News.Cast<object>().ToArray());
        }

        [ArenaHandler(ArenaMessageType.GetPlayerInfo)]
        public async ValueTask HandleGetPlayerInfo(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            PlayerMask mask = ArgE<PlayerMask>(args[0]);
            string name = ArgS(args[1]);

            if (mask != (PlayerMask.Country | PlayerMask.EMail | PlayerMask.VisitingCard | PlayerMask.GameConfig))
                throw new ArgumentException("Unexpected player mask.", nameof(message));

            // User must exist.
            User? user = _userStore.Find(name);
            if (user == null)
            {
                await SendFail(sender, ArenaMessageType.GetPlayerInfo, "Player not found.");
                return;
            }

            // Reply sender.
            Client? client = Clients.FirstOrDefault(x => x.PlayerName == name);
            await Send(sender, ArenaMessageType.PlayerInfo, GetPlayerInfo(client, user, mask));
        }

        [ArenaHandler(ArenaMessageType.GetPlayers)]
        public async ValueTask HandleGetPlayers(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            PlayerMask mask = ArgE<PlayerMask>(args[0]);

            if (mask != (PlayerMask.Score | PlayerMask.Shareware))
                throw new ArgumentException("Unexpected player mask.");

            // Get all server players.
            List<object> reply = new();
            foreach (Client client in Clients)
            {
                User? user = _userStore.Find(client.PlayerName);
                if (user == null)
                    continue;
                reply.Add(GetPlayerInfo(client, user, mask));
            }

            // Reply sender.
            await Send(sender, ArenaMessageType.PlayerList, reply);
        }

        [ArenaHandler(ArenaMessageType.GetSessionInfo)]
        public async ValueTask HandleGetSessionInfo(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            SessionMask mask = ArgE<SessionMask>(args[0]);
            string name = ArgS(args[1]);

            if (mask != SessionMask.Infos)
                throw new ArgumentException("Unexpected session mask.", nameof(message));

            // Session must exist.
            Session? session = _sessions.FirstOrDefault(x => x.Name == name);
            if (session == null)
            {
                await SendFail(sender, ArenaMessageType.GetSessionInfo, "Session not found.");
                return;
            }

            // Reply sender.
            await Send(sender, ArenaMessageType.SessionInfo, GetSessionInfo(session, mask));
        }

        [ArenaHandler(ArenaMessageType.GetSessions)]
        public async ValueTask HandleGetSessions(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            SessionMask mask = ArgE<SessionMask>(args[0]);

            if (mask != (SessionMask.MaxPlayerCount | SessionMask.PlayerCount | SessionMask.MasterName
                | SessionMask.PlayerList | SessionMask.OpenPrivate))
                throw new ArgumentException("Unexpected session mask.");

            // Get all server sessions.
            List<object> reply = new();
            foreach (Session session in _sessions)
                reply.Add(GetSessionInfo(session, mask));

            // Reply sender.
            await Send(sender, ArenaMessageType.SessionList, reply);
        }

        [ArenaHandler(ArenaMessageType.JoinSession)]
        public async ValueTask HandleJoinSession(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            string sessionName = ArgS(args[0]);

            // Session must exist.
            Session? session = _sessions.FirstOrDefault(x => x.Name == sessionName);
            if (session == null)
            {
                await SendFail(sender, ArenaMessageType.JoinSession, "Session not found.");
                return;
            }

            // Session must have open slot.
            if (session.Players.Count >= session.MaxPlayerCount)
            {
                await SendFail(sender, ArenaMessageType.JoinSession, "Session is full.");
                return;
            }

            // Add player to session.
            session.Players.Add(sender);
            sender.Session = session;

            // Reply sender.
            await SendSuccess(sender, ArenaMessageType.JoinSession, session.Name, /*stay*/String.Empty, 0);

            // Update other server clients.
            List<object> reply = new() { sender.PlayerName, session.Name };
            foreach (Client serverClients in Clients.Where(x => x != sender))
                await Send(serverClients, ArenaMessageType.JoinNew, reply);
        }

        [ArenaHandler(ArenaMessageType.LeaveSession)]
        public async ValueTask HandleLeaveSession(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            string sessionName = ArgS(args[0]);

            if (sessionName != sender.Session?.Name)
                return;

            // Update all server clients.
            await LeaveSession(sender);
        }

        [ArenaHandler(ArenaMessageType.ServiceMessage)]
        public async ValueTask HandleServiceMessage(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            IList<string> playerNames = ArgSList(args[0]);

            // TODO: Validate sender state.

            string msg = ArgS(args[1]);
            int sessionNameStart = msg.IndexOf('\xFE');
            int playerIDsStart = msg.IndexOf('\xFD');
            if (sessionNameStart == -1 || playerIDsStart == -1)
                throw new ArgumentException("Invalid service message.");
            string sessionName = msg[(sessionNameStart + 1)..playerIDsStart];
            int[] playerIDs = msg[(playerIDsStart + 1)..]
                .Split(' ', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => Int32.Parse(x)).ToArray();

            // Assign IDs to players.
            IList<Client> sessionClients = Clients.Where(x => x.Session == sender.Session).ToList();
            for (int i = 0; i < playerIDs.Length; i++)
                sessionClients[i].SessionID = playerIDs[i];

            // TODO: PlayerId/ConnectPlayer/AddPlayer here or in GameBegin?

            // Update session clients.
            // TODO: Validate contents of reply.
            foreach (Client client in Clients.Where(x => playerNames.Contains(x.PlayerName)))
            {
                await Send(client, ArenaMessageType.ServiceMessage, new object[] {
                    client.Session!.Master.PlayerName, $"GOTOMJ \xFE{sessionName}\xFD"
                });
            }
        }

        [ArenaHandler(ArenaMessageType.StillAlive), Unauthenticated]
        public ValueTask HandleStillAlive(Client sender, ArenaMessage message)
        {
            return default;
        }

        [ArenaHandler(ArenaMessageType.StillAlive_Shareware), Unauthenticated]
        public ValueTask HandleStillAliveShareware(Client sender, ArenaMessage message)
        {
            sender.Shareware = true;
            return default;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override async ValueTask OnClientDisconnect(Client client, Exception ex)
        {
            await base.OnClientDisconnect(client, ex);

            // If client is in a session, notify others about leave.
            if (client.Session != null)
                await LeaveSession(client);

            // Notify others about leave.
            List<object> args = new() { client.PlayerName! };
            foreach (Client other in Clients)
                await Send(other, ArenaMessageType.PlayerRemoved, args);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private async ValueTask LeaveSession(Client client)
        {
            Session? session = client.Session;

            // Player must be in the session.
            if (session == null)
                return;

            if (session.Master == client)
            {
                // Master leaves, remove session.
                await RemoveSession(session);
            }
            else
            {
                // Player leaves, remove from session.
                client.Session = null;
                session.Players.Remove(client);
                // Update all clients.
                List<object> reply = new() { client.PlayerName, session.Name };
                foreach (Client serverClient in Clients)
                    await Send(serverClient, ArenaMessageType.JoinLeave, reply);
            }
        }

        private async ValueTask RemoveSession(Session session)
        {
            // Remove all players.
            foreach (Client player in session.Players)
            {
                player.Session = null;
                List<object> leftPlayer = new() { player.PlayerName, session.Name };
                foreach (Client serverClient in Clients)
                    await Send(serverClient, ArenaMessageType.JoinLeave, leftPlayer);
            }
            session.Players.Clear();

            // Remove session.
            _sessions.Remove(session);

            List<object> removedSession = new() { session.Name };
            foreach (Client client in Clients)
                await Send(client, ArenaMessageType.SessionRemoved, removedSession);
        }
    }
}
