﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using Syroot.Pod.GameService.Data;
using Syroot.Pod.GameService.Logging;
using Syroot.Pod.GameService.Net;
using Syroot.Pod.GameService.Routers;
using Syroot.Pod.GameService.Servers;
using Syroot.Pod.GameService.Services;
using Syroot.Pod.GameService.Messages;
using Syroot.Pod.GameService.RouterRegistry;

namespace Syroot.Pod.GameService
{
    /// <summary>
    /// Represents the main class of the application containing the program entry point.
    /// </summary>
    internal class Program
    {
        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static async Task Main(string[] args) => await CreateHostBuilder(args).Build().RunAsync();

        private static IHostBuilder CreateHostBuilder(string[]? args) => Host.CreateDefaultBuilder(args)
            .ConfigureServices((context, services) =>
            {
                // Add logging.
                services.AddLogging(builder => builder
                    .AddConsole(options => options.FormatterName = "Router")
                    .AddConsoleFormatter<LogFormatter, SimpleConsoleFormatterOptions>());

                // Add database.
                services.AddDbContextFactory<GsDbContext>(builder => builder
                    .UseSqlite(context.Configuration.GetConnectionString(nameof(GsDbContext))));
                services.AddSingleton<IUserStore, GsDbUserStore>();

                // Add services.
                services.AddSingleton<IPasswordHasher, Pbkdf2PasswordHasher>();

                // Add registry.
                services.AddHostedService<RegistryService>();
                services.AddSingleton<IRouterRegistry, WebRegistry>();
                services.Configure<RegistryOptions>(context.Configuration.GetSection("Registry"));

                // Add router and servers.
                services.AddHostedService<Router>();
                services.AddSingleton<IProtocol<IGsMessage>, GsProtocol>();
                services.AddSingleton<IServerFactory, ServerFactory>();
                services.AddTransient<IListener, Listener>();
                services.AddTransient<IMessageDispatcher, MessageDispatcher>();
                services.AddTransient<Server>();
                services.Configure<RouterOptions>(context.Configuration.GetSection("Router"));
            });
    }
}
