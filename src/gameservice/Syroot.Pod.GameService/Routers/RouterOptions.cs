﻿using System;
using System.Collections.Generic;
using Syroot.Pod.GameService.Net;
using Syroot.Pod.GameService.Servers;
using Syroot.Pod.GameService.Messages;

namespace Syroot.Pod.GameService.Routers
{
    public class RouterOptions : HosterOptions
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the key to register to the web router registry with.
        /// </summary>
        public string RegistryID { get; set; } = String.Empty;

        public List<string> News { get; set; } = new();

        public List<ServerOptions> Servers { get; set; } = new();

        internal override ArenaMachine Machine => ArenaMachine.Router;
    }
}
