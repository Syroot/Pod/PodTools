﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Syroot.Pod.GameService.Data;
using Syroot.Pod.GameService.Net;
using Syroot.Pod.GameService.Servers;
using Syroot.Pod.GameService.Messages;
using Syroot.Pod.GameService.RouterRegistry;

namespace Syroot.Pod.GameService.Routers
{
    /// <summary>
    /// Represents a GameService router which dispatches clients to several <see cref="Server"/> instances.
    /// </summary>
    public partial class Router : Hoster
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IDbContextFactory<GsDbContext> _dbFactory;
        private readonly RouterOptions _options;
        private readonly IRouterRegistry _routerRegistry;
        private readonly IServerFactory _serverFactory;
        private readonly IUserStore _userStore;
        private readonly List<Server> _servers = new();

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Router(
            IMessageDispatcher dispatcher,
            IListener listener,
            ILoggerFactory loggerFactory,
            IOptions<RouterOptions> options,
            IProtocol<IGsMessage> protocol,

            IDbContextFactory<GsDbContext> dbFactory,
            IRouterRegistry routerRegistry,
            IServerFactory serverFactory,
            IUserStore userStore)
            : base(dispatcher, listener, loggerFactory, options, protocol)
        {
            _dbFactory = dbFactory;
            _options = options.Value;
            _routerRegistry = routerRegistry;
            _serverFactory = serverFactory;
            _userStore = userStore;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        [ArenaHandler(ArenaMessageType.CheckPassword), Unauthenticated]
        public async ValueTask HandleCheckPassword(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            string username = ArgS(args[0]);
            string password = ArgS(args[1]);

            // Authenticate.
            User? user = _userStore.Authenticate(username, password);
            if (user == null)
            {
                await SendFail(sender, ArenaMessageType.CheckPassword, "Wrong username or password.");
                return;
            }

            // Login this connection.
            sender.PlayerName = user.Name;
            Logger.LogInformation("{EndPoint} authenticated as {Username}",
                ((IPEndPoint?)sender.Connection.Socket.RemoteEndPoint)?.ToString(), sender.PlayerName);

            // Reply sender.
            await SendSuccess(sender, ArenaMessageType.CheckPassword);
        }

        [ArenaHandler(ArenaMessageType.Connect), Unauthenticated]
        public async ValueTask HandleConnect(Client sender, ArenaMessage message)
        {
            // Reply sender.
            await Send(sender, ArenaMessageType.Connect);
        }

        [ArenaHandler(ArenaMessageType.Connect_Shareware), Unauthenticated]
        public async ValueTask HandleConnectShareware(Client sender, ArenaMessage message)
        {
            // Mark sender as shareware.
            sender.Shareware = true;

            // Reply sender.
            await Send(sender, ArenaMessageType.Connect_Shareware);
        }

        [ArenaHandler(ArenaMessageType.ConnectionRequest)]
        public async ValueTask HandleConnectionRequest(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            string username = ArgS(args[0]);
            string password = ArgS(args[1]);
            string serverName = ArgS(args[2]);
            string gameName = ArgS(args[3]);
            string gameVersion = ArgS(args[4]);

            // Authenticate.
            User? user = _userStore.Authenticate(username, password);
            if (user == null)
            {
                await SendFail(sender, ArenaMessageType.ConnectionRequest, "Wrong username or password.");
                return;
            }

            // Server must exist.
            Server? server = _servers.FirstOrDefault(x => x.Options.Name == serverName);
            if (server == null)
            {
                await SendFail(sender, ArenaMessageType.ConnectionRequest, "Server not found.");
                return;
            }

            // Request for server details, reply sender.
            await SendSuccess(sender, ArenaMessageType.ConnectionRequest, new object[] {
                server.Options.IPEndPoint.Address, // virtualize server on router endpoint
                server.Options.IPEndPoint.Port,
                server.Options.IPEndPoint.Port, // rescue port?
                new object[] { username, serverName, password, gameName, gameVersion } });
        }

        [ArenaHandler(ArenaMessageType.GetLastNews)]
        public async ValueTask HandleGetLastNews(Client sender, ArenaMessage message)
        {
            // Reply sender.
            await Send(sender, ArenaMessageType.News, _options.News.Cast<object>().ToArray());
        }

        [ArenaHandler(ArenaMessageType.GetPlayerInfo)]
        public async ValueTask HandleGetPlayerInfo(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            PlayerMask mask = ArgE<PlayerMask>(args[0]);
            string username = ArgS(args[1]);
            string password = ArgS(args[2]);

            if (mask != (PlayerMask.Country | PlayerMask.EMail | PlayerMask.VisitingCard))
                throw new ArgumentException("Unexpected player mask.", nameof(message));

            if (username != sender.PlayerName)
                throw new ArgumentException("Cannot request detailed player info from other user.");

            // Authenticate user.
            User? user = _userStore.Authenticate(username, password);
            if (user == null)
            {
                await SendFail(sender, ArenaMessageType.GetPlayerInfo, "Wrong username or password.");
                return;
            }

            // Reply sender.
            await Send(sender, ArenaMessageType.PlayerInfo, GetPlayerInfo(sender, user, mask));
        }

        [ArenaHandler(ArenaMessageType.GetRouters)]
        public async ValueTask HandleGetRouters(Client sender, ArenaMessage message)
        {
            // Validate query.
            List<object> reply = new();
            foreach (RegisteredRouter router in await _routerRegistry.GetRouters())
                reply.Add(new object[] { router.Name, router.EndPoint.Address, router.EndPoint.Port });

            // Reply sender.
            await Send(sender, ArenaMessageType.RouterList, reply);
        }

        [ArenaHandler(ArenaMessageType.GetServers)]
        public async ValueTask HandleGetServers(Client sender, ArenaMessage message)
        {
            // Get all servers.
            List<object> reply = new();
            foreach (Server server in _servers)
            {
                reply.Add(new object[]
                {
                    server.Options.Name,
                    server.Options.InternalName,
                    server.Clients.Count,
                    server.Options.PingLimit
                });
            }

            // Reply sender.
            await Send(sender, ArenaMessageType.ServerList, reply);
        }

        [ArenaHandler(ArenaMessageType.ModifyUserRequest)]
        public async ValueTask HandleModifyUserRequest(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            string username = ArgS(args[0]);
            string password = ArgS(args[1]);
            string newPassword = ArgS(args[2]); // new password, not supported by POD, cannot be edited
            string country = ArgS(args[3]);
            string email = ArgS(args[4]);
            string visitingCard = ArgS(args[5]);

            try
            {
                await _userStore.Modify(username, password, newPassword, email, country, visitingCard);
                await SendSuccess(sender, ArenaMessageType.ModifyUserRequest);
            }
            catch (Exception ex)
            {
                await SendFail(sender, ArenaMessageType.ModifyUserRequest, ex.Message);
            }
        }

        [ArenaHandler(ArenaMessageType.NewUserRequest), Unauthenticated]
        public async ValueTask HandleNewUserRequest(Client sender, ArenaMessage message)
        {
            // Validate query.
            IList<object> args = message.Args ?? throw new ArgumentException("Missing arguments.");
            string username = ArgS(args[0]);
            string password = ArgS(args[1]);
            string email = ArgS(args[2]);
            string country = ArgS(args[3]);
            string visitingCard = ArgS(args[4]);

            try
            {
                await _userStore.Create(username, password, email, country, visitingCard);
                await SendSuccess(sender, ArenaMessageType.NewUserRequest);
            }
            catch (Exception ex)
            {
                await SendFail(sender, ArenaMessageType.NewUserRequest, ex.Message);
            }
        }

        [ArenaHandler(ArenaMessageType.StillAlive), Unauthenticated]
        public ValueTask HandleStillAlive(Client sender, ArenaMessage message)
        {
            return default;
        }

        [ArenaHandler(ArenaMessageType.StillAlive_Shareware), Unauthenticated]
        public ValueTask HandleStillAliveShareware(Client sender, ArenaMessage message)
        {
            sender.Shareware = true;
            return default;
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // Update database.
            Logger.LogInformation("Updating database...");
            await _dbFactory.CreateDbContext().Database.MigrateAsync(stoppingToken);

            // Start server instances.
            foreach (ServerOptions serverOptions in _options.Servers)
            {
                Server server = _serverFactory.Create(Options.Create(serverOptions));
                _ = server.StartAsync(stoppingToken);
                _servers.Add(server);
            }

            await base.ExecuteAsync(stoppingToken);
        }
    }
}
