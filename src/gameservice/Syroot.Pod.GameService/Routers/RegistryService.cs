﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Syroot.Pod.GameService.RouterRegistry;

namespace Syroot.Pod.GameService.Routers
{
    /// <summary>
    /// Represents a background service continually updating the router end point on the web service.
    /// </summary>
    public class RegistryService : BackgroundService
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly ILogger _logger;
        private readonly RegistryOptions _options;
        private readonly RouterOptions _routerOptions;
        private readonly IRouterRegistry _registry;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public RegistryService(
            ILoggerFactory loggerFactory,
            IOptions<RegistryOptions> options,
            IOptions<RouterOptions> routerOptions,
            IRouterRegistry registry)
        {
            _logger = loggerFactory.CreateLogger("Registry");
            _options = options.Value;
            _routerOptions = routerOptions.Value;
            _registry = registry;
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                _logger.LogInformation("Updating router online state...");
                await _registry.Unregister(_options.ID);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Could not unregister router.");
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    _logger.LogInformation("Updating router online state...");
                    await _registry.Register(_options.ID, _routerOptions.Name, _routerOptions.IPEndPoint.Port);
                }
                catch (Exception ex)
                {
                    stoppingToken.ThrowIfCancellationRequested();
                    _logger.LogError(ex, "Could not register router: {Exception}", ex);
                }
                await Task.Delay(_options.Interval <= 0 ? -1 : _options.Interval * 60 * 1000, stoppingToken);
            }
        }
    }
}
