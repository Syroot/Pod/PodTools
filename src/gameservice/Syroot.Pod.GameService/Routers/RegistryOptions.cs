﻿using System;

namespace Syroot.Pod.GameService.Routers
{
    /// <summary>
    /// Represents configuration of <see cref="RegistryService"/>.
    /// </summary>
    public class RegistryOptions
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the key to register to the web service with.
        /// </summary>
        public string ID { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the number of minutes after which the router endpoint will be updated on the web service, or 0
        /// to disable updates.
        /// </summary>
        public int Interval { get; set; } = 1;
    }
}
