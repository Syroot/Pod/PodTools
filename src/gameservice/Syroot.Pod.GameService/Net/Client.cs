﻿using System;
using System.Net;
using System.Net.Sockets;
using Pipelines.Sockets.Unofficial;
using Syroot.Pod.GameService.Servers;

namespace Syroot.Pod.GameService.Net
{
    /// <summary>
    /// Represents the state of a connected user.
    /// </summary>
    public class Client
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Client(Socket socket) => Connection = SocketConnection.Create(socket);

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the <see cref="SocketConnection"/> to send and receive data with.
        /// </summary>
        public SocketConnection Connection { get; }

        /// <summary>
        /// Gets or sets the session the client is currently in.
        /// </summary>
        public Session? Session { get; set; }

        /// <summary>
        /// Gets or sets the ID a player receives during a session game.
        /// </summary>
        public int SessionID { get; set; }

        /// <summary>
        /// Gets or sets the user name as displayed in the game.
        /// </summary>
        public string PlayerName { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the game configuration string.
        /// </summary>
        public string GameConfig { get; set; } = String.Empty;

        /// <summary>
        /// Whether the client connected with a shareware version, to limit some of the available features.
        /// </summary>
        public bool Shareware { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override string ToString() => String.IsNullOrEmpty(PlayerName)
            ? ((IPEndPoint?)Connection.Socket.RemoteEndPoint)?.ToString() ?? "disconnected"
            : PlayerName;
    }
}
