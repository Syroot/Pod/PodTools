﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Syroot.Pod.GameService.Messages;

namespace Syroot.Pod.GameService.Net
{
    public class MessageDispatcher : IMessageDispatcher
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private Hoster? _instance;
        private readonly List<HandlerInfo> _handlers = new();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Bind(Hoster instance)
        {
            _instance = instance;

            foreach (MethodInfo method in _instance.GetType().GetMethods(BindingFlags.Instance | BindingFlags.Public))
            {
                // Validate method, must accept client as first parameter and return ValueTask.
                ParameterInfo[] parameters = method.GetParameters();
                if (parameters.Length != 2)
                    continue;
                if (parameters[0].ParameterType != typeof(Client))
                    continue;
                if (method.ReturnType != typeof(ValueTask))
                    continue;

                HandlerInfo handlerInfo = new(method);
                handlerInfo.Unauthenticated = method.GetCustomAttribute<UnauthenticatedAttribute>() != null;

                // Validate arena handler, must accept ArenaMessage as second parameter.
                handlerInfo.ArenaType = method.GetCustomAttribute<ArenaHandlerAttribute>()?.Type;
                if (handlerInfo.ArenaType.HasValue)
                {
                    if (parameters[1].ParameterType != typeof(ArenaMessage))
                        throw new InvalidOperationException($"Arena message handler {method.Name} has bad parameters.");
                    _handlers.Add(handlerInfo);
                    continue;
                }

                //// TODO: Validate game handler, must accept GameMessage as second parameter.
                //handlerInfo.GameType = method.GetCustomAttribute<GameHandlerAttribute>()?.Type;
                //if (handlerInfo.GameType.HasValue)
                //{
                //    if (parameters[1].ParameterType != typeof(GameMessage))
                //        throw new InvalidOperationException($"Game message handler {method.Name} has bad parameters.");
                //    _handlers.Add(handlerInfo);
                //    continue;
                //}
            }
        }

        public async ValueTask<bool> Dispatch(Client sender, IGsMessage message)
        {
            // Find message handler.
            HandlerInfo? handler = null;
            switch (message)
            {
                case ArenaMessage arenaMessage:
                    handler = _handlers.SingleOrDefault(x
                        => (x.Unauthenticated || !String.IsNullOrEmpty(sender.PlayerName))
                        && x.ArenaType == arenaMessage.Type);
                    break;
                    // TODO: case GameMessage:
                    //    handler = _handlers.SingleOrDefault(x
                    //        => (x.Unauthenticated || !String.IsNullOrEmpty(sender.PlayerName))
                    //        && x.ArenaType == gameMessage.Type);
                    //    break;
            }

            // Invoke the handler, if any.
            if (handler == null)
                return false;

            await (ValueTask)handler.Method.Invoke(_instance, new object[] { sender, message })!;
            return true;
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private record HandlerInfo
        {
            internal HandlerInfo(MethodInfo method) => Method = method;

            internal bool Unauthenticated;
            internal ArenaMessageType? ArenaType;
            // TODO: internal GameMessageType? GameType;
            internal MethodInfo Method;
        }
    }

    /// <summary>
    /// Represents a decoration for a message handling method which does not require the client to be authorized to be
    /// executed.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class UnauthenticatedAttribute : Attribute { }

    /// <summary>
    /// Represents a decoration for a message handling method accepting an <see cref="ArenaMessage"/>.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class ArenaHandlerAttribute : Attribute
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public ArenaHandlerAttribute(ArenaMessageType type) => Type = type;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public ArenaMessageType Type { get; }
    }

    // TODO
    ///// <summary>
    ///// Represents a decoration for a message handling method accepting a <see cref="GameMessage"/>.
    ///// </summary>
    //[AttributeUsage(AttributeTargets.Method)]
    //public class GameHandlerAttribute : Attribute
    //{
    //}
}
