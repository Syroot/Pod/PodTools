﻿using System;
using System.Buffers;
using System.Diagnostics.CodeAnalysis;

namespace Syroot.Pod.GameService.Net
{
    public interface IProtocol<TMessage>
    {
        bool TryParseMessage(in ReadOnlySequence<byte> input, ref SequencePosition consumed,
            ref SequencePosition examined, [NotNullWhen(true)] out TMessage? message);

        void WriteMessage(TMessage message, IBufferWriter<byte> output);
    }
}
