﻿using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Syroot.Pod.GameService.Net
{
    /// <summary>
    /// Represents a listener thread for incoming TCP connections, dispatching the accepted <see cref="Socket"/>
    /// instances for dequeing.
    /// </summary>
    public interface IListener
    {
        // ---- METHODS ------------------------------------------------------------------------------------------------

        /// <summary>
        /// Starts listen asynchronously for new client connections and stores them for dequeuing..
        /// </summary>
        /// <param name="endPoint">A <see cref="IPEndPoint"/> to listen under.</param>
        /// <param name="ct">A <see cref="CancellationToken"/> to cancel the operation with.</param>
        /// <returns>A task only completing on exceptions or cancellation.</returns>
        Task ListenAsync(IPEndPoint endPoint, CancellationToken ct = default);

        /// <summary>
        /// Tries to dequeue an accepted <paramref name="socket"/> connection if any is available.
        /// </summary>
        /// <param name="socket">A accepted <see cref="Socket"/> or <see langword="null"/> if none is available.</param>
        /// <returns><see langword="true"/>when a socket was accepted.</returns>
        bool TryAccept([NotNullWhen(true)] out Socket? socket);
    }
}
