﻿using System;
using System.Net;
using Syroot.Pod.GameService.Services;
using Syroot.Pod.GameService.Messages;

namespace Syroot.Pod.GameService.Net
{
    /// <summary>
    /// Represents basic connectivity settings for a router or server.
    /// </summary>
    public abstract class HosterOptions
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the display name of the router or server as seen in the game.
        /// </summary>
        public string Name { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the internal name of the router or server used in logs and some packets.
        /// </summary>
        public string InternalName { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the IPv4 end point on which the router or server listens. Supports 'public', 'private',
        /// and 'any' keywords for the address.
        /// </summary>
        public string EndPoint { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the number of milliseconds to pause in the client accept / read loop. A higher number simulates
        /// slow connections while preserving CPU resources.
        /// </summary>
        public int PollDelay { get; set; } = 10;

        internal IPEndPoint IPEndPoint => IPResolver.ResolveEndPoint(EndPoint);

        internal abstract ArenaMachine Machine { get; }
    }
}
