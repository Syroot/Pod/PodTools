﻿using System;
using System.Buffers;
using System.Buffers.Binary;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.CompilerServices;
using Syroot.Pod.GameService.Messages;

namespace Syroot.Pod.GameService.Net
{
    /// <summary>
    /// Represents the GameService message protocol.
    /// </summary>
    public class GsProtocol : IProtocol<IGsMessage>
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public bool TryParseMessage(in ReadOnlySequence<byte> input, ref SequencePosition consumed,
            ref SequencePosition examined, [NotNullWhen(true)] out IGsMessage? message)
        {
            SequenceReader<byte> reader = new(input);
            message = default!;

            // Read header.
            Header header = new();
            if (!reader.TryReadBigEndian(out header.Size) || input.Length < header.Size)
                return false;
            if (!reader.TryRead(out header.Flags))
                return false;
            if (!reader.TryRead(out header.Mode))
                return false;

            // Read data.
            if (header.Mode < 0)
            {
                if (!TryParseGameMessage(ref reader, header, out GameMessage? gameMessage))
                    return false;
                message = gameMessage;
            }
            else
            {
                if (!TryParseArenaMessage(ref reader, header, out ArenaMessage? arenaMessage))
                    return false;
                message = arenaMessage;
            }

            consumed = reader.Position;
            examined = consumed;
            return true;
        }

        /// <inheritdoc/>
        public void WriteMessage(IGsMessage message, IBufferWriter<byte> output)
        {
            // Serialize message.
            Header header;
            ReadOnlySpan<byte> data = message switch
            {
                GameMessage gameMessage => WriteGameMessage(out header, gameMessage),
                ArenaMessage arenaMessage => WriteArenaMessage(out header, arenaMessage),
                _ => throw new ArgumentException("Unknown message type.", nameof(message))
            };

            Span<byte> span = output.GetSpan(header.Size);

            // Write header.
            BinaryPrimitives.WriteUInt16BigEndian(span, header.Size);
            span[2] = header.Flags;
            span[3] = (byte)header.Mode;

            // Write data.
            data.CopyTo(span[Unsafe.SizeOf<Header>()..]);

            output.Advance(header.Size);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static bool TryParseArenaMessage(ref SequenceReader<byte> reader, Header header,
            [NotNullWhen(true)] out ArenaMessage? message)
        {
            message = default;
            if (header.Mode < 0)
                throw new InvalidDataException("Bad mode.");

            // Read data.
            Span<byte> data = stackalloc byte[header.Size - Unsafe.SizeOf<Header>()];
            if (!reader.TryCopyTo(data))
                return false;
            reader.Advance(data.Length); // advance manually as TryCopyTo does not do it

            // Deserialize.
            message = new(
                (ArenaMessageType)header.Mode,
                (ArenaMachine)(header.Flags >> 0 & 0b111),
                (ArenaMachine)(header.Flags >> 3 & 0b111),
                ArenaNode.Deserialize(data)!);

            return true;
        }

        private static bool TryParseGameMessage(ref SequenceReader<byte> reader, Header header,
            [NotNullWhen(true)] out GameMessage? message)
        {
            if (header.Mode > 0)
                throw new InvalidDataException("Bad mode.");

            throw new NotImplementedException();
        }

        private static ReadOnlySpan<byte> WriteArenaMessage(out Header header, ArenaMessage message)
        {
            ReadOnlySpan<byte> data = ArenaNode.Serialize(message.Args);
            header.Size = (ushort)(Unsafe.SizeOf<Header>() + data.Length);
            header.Mode = (sbyte)message.Type;
            header.Flags = (byte)(((ushort)message.Source & 0b111) | ((ushort)message.Target & 0b111) << 3);
            return data;
        }

        private static ReadOnlySpan<byte> WriteGameMessage(out Header header, GameMessage gameMessage)
        {
            throw new NotImplementedException();
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private struct Header
        {
            public ushort Size; // total length of message including header and data
            public sbyte Mode; // < 0 ? GameMessage : ArenaMessage
            public byte Flags;
        }
    }
}
