﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Pipelines;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Syroot.Pod.GameService.Data;
using Syroot.Pod.GameService.Messages;
using Syroot.Pod.GameService.Servers;

namespace Syroot.Pod.GameService.Net
{
    /// <summary>
    /// Represents the base class of a GameService connection handler which is either a <see cref="Router"/> or a
    /// <see cref="Server"/>.
    /// </summary>
    public abstract class Hoster : BackgroundService
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IMessageDispatcher _dispatcher;
        private readonly IListener _listener;
        private readonly HosterOptions _options;
        private readonly IProtocol<IGsMessage> _protocol;
        private readonly List<Client> _clients = new();

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public Hoster(
            IMessageDispatcher dispatcher,
            IListener listener,
            ILoggerFactory loggerFactory,
            IOptions<HosterOptions> options,
            IProtocol<IGsMessage> protocol)
        {
            _dispatcher = dispatcher;
            _listener = listener;
            Logger = loggerFactory.CreateLogger(options.Value.InternalName);
            _options = options.Value;
            _protocol = protocol;

            _dispatcher.Bind(this);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IReadOnlyList<Client> Clients => _clients.AsReadOnly();

        protected ILogger Logger { get; }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected static string ArgS(object arg) => (string)arg;
        protected static T ArgE<T>(object arg) where T : Enum => (T)(object)Int32.Parse((string)arg);
        protected static int ArgI(object arg) => Int32.Parse((string)arg);
        protected static IList<string> ArgSList(object args) => ((IEnumerable<object>)args).Select(x => ArgS(x)).ToList();

        protected static IList<object> GetPlayerInfo(Client? client, User user, PlayerMask mask)
        {
            List<object> info = new() { mask, user.Name };

            if (mask.HasFlag(PlayerMask.Unknown0))
                info.Add("Unknown0");

            if (mask.HasFlag(PlayerMask.Country))
                info.Add(user.Country ?? " ");

            if (mask.HasFlag(PlayerMask.Score))
                info.Add(client?.Shareware == true ? "-" : user.Score);

            if (mask.HasFlag(PlayerMask.EMail))
                info.Add(user.Email ?? " ");

            if (mask.HasFlag(PlayerMask.VisitingCard))
                info.Add(user.VisitingCard ?? " ");

            if (mask.HasFlag(PlayerMask.Unknown5))
                info.Add("Unknown5");

            if (mask.HasFlag(PlayerMask.GameConfig))
                info.Add(client?.GameConfig ?? " ");

            if (mask.HasFlag(PlayerMask.Unknown7))
                info.Add("Unknown7");

            if (mask.HasFlag(PlayerMask.Shareware))
                info.Add(client?.Shareware == true ? "S" : " ");

            return info;
        }

        protected static IList<object> GetSessionInfo(Session session, SessionMask mask)
        {
            List<object> info = new() { mask, session.Name };

            if (mask.HasFlag(SessionMask.MaxPlayerCount))
                info.Add(session.MaxPlayerCount);

            if (mask.HasFlag(SessionMask.PlayerCount))
                info.Add(session.Players.Count);

            if (mask.HasFlag(SessionMask.MasterName))
                info.Add(session.Master.PlayerName!);

            if (mask.HasFlag(SessionMask.PlayerList))
            {
                List<object> players = new();
                foreach (Client player in session.Players)
                    players.Add(new object[] { player.PlayerName! });
                info.Add(players);
            }

            if (mask.HasFlag(SessionMask.Infos))
                info.Add(session.Infos);

            if (mask.HasFlag(SessionMask.OpenPrivate))
                info.Add((session.Open ? 1 << 0 : 0) | (session.InRace ? 1 << 1 : 0));

            return info;
        }

        /// <inheritdoc/>
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            // https://blog.stephencleary.com/2020/05/backgroundservice-gotcha-startup.html
            await Task.Yield();

            // Start listening.
            _ = _listener.ListenAsync(new IPEndPoint(IPAddress.Any, _options.IPEndPoint.Port), stoppingToken);
            Logger.LogInformation($"Listening on {_options.IPEndPoint}...");

            // Run accept / read loop.
            while (!stoppingToken.IsCancellationRequested)
            {
                await Task.Delay(_options.PollDelay, stoppingToken);
                await AcceptClients();
                await ReadClients();
            }
        }

        protected void LogMessage(Client client, IGsMessage message)
        {
            static string acronym(ArenaMachine machine) => machine switch
            {
                ArenaMachine.Client => "C",
                ArenaMachine.Router => "R",
                ArenaMachine.Server => "S",
                ArenaMachine.Router | ArenaMachine.Server => "RS",
                _ => machine.ToString("D")
            };
            string direction = message is ArenaMessage arenaMessage
                ? $"{acronym(arenaMessage.Source)}>{acronym(arenaMessage.Target),-2}"
                : "Game";

            string player = client.ToString().PadRight(8);

            Logger.LogInformation("{Direction} {Player} {Message}", direction, player, message);
        }

        protected virtual ValueTask OnClientConnect(Client client)
        {
            Logger.LogWarning("{Client} connected", client);
            _clients.Add(client);
            return default;
        }

        protected virtual ValueTask OnClientDisconnect(Client client, Exception ex)
        {
            Logger.LogWarning("{Client} disconnected ({Reason})", client, ex.Message);
            _clients.Remove(client);
            return default;
        }

        protected async ValueTask Send(Client receiver, ArenaMessageType type, IList<object>? args = null)
        {
            await Send(receiver, new ArenaMessage(type, _options.Machine, ArenaMachine.Client, args));
        }

        protected async ValueTask SendFail(Client receiver, ArenaMessageType queryType, string reason)
        {
            await Send(receiver, ArenaMessageType.Fail, new object[] { queryType, reason });
        }

        protected async ValueTask SendSuccess(Client receiver, ArenaMessageType queryType, params object[] args)
        {
            if (args.Length == 0)
                await Send(receiver, ArenaMessageType.Success, new object[] { queryType });
            else
                await Send(receiver, ArenaMessageType.Success, new object[] { queryType, args });
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private async ValueTask AcceptClients()
        {
            while (_listener.TryAccept(out Socket? socket))
                await OnClientConnect(new Client(socket));
        }

        private async ValueTask ReadClients()
        {
            for (int i = _clients.Count - 1; i >= 0; i--)
            {
                Client client = _clients[i];
                try
                {
                    // Receive one packet.
                    if (!client.Connection.Input.TryRead(out ReadResult read))
                        continue;
                    if (read.IsCanceled)
                        throw new OperationCanceledException("read was canceled");
                    if (read.IsCompleted)
                        throw new EndOfStreamException("client sent no more data");

                    SequencePosition consumed = default;
                    SequencePosition examined = default;
                    if (_protocol.TryParseMessage(read.Buffer, ref consumed, ref examined, out IGsMessage? message))
                    {
                        client.Connection.Input.AdvanceTo(consumed, examined);
                        await Receive(client, message);
                    }
                    else
                    {
                        client.Connection.Input.CancelPendingRead();
                    }
                }
                catch (Exception ex)
                {
                    await OnClientDisconnect(client, ex);
                }
            }
        }

        private async ValueTask Receive(Client sender, IGsMessage message)
        {
            LogMessage(sender, message);
            await _dispatcher.Dispatch(sender, message);
        }

        private async ValueTask Send(Client receiver, IGsMessage message)
        {
            LogMessage(receiver, message);
            _protocol.WriteMessage(message, receiver.Connection.Output);
            await receiver.Connection.Output.FlushAsync();
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        [Flags]
        protected enum PlayerMask
        {
            Unknown0 = 1 << 0,
            Country = 1 << 1,
            Score = 1 << 2,
            EMail = 1 << 3,
            VisitingCard = 1 << 4,
            Unknown5 = 1 << 5,
            GameConfig = 1 << 6,
            Unknown7 = 1 << 7,
            Shareware = 1 << 8
        }

        [Flags]
        protected enum SessionMask
        {
            MaxPlayerCount = 1 << 0,
            PlayerCount = 1 << 1,
            MasterName = 1 << 2,
            String3 = 1 << 3,
            PlayerList = 1 << 4,
            List5 = 1 << 5,
            Infos = 1 << 6,
            OpenPrivate = 1 << 7
        }
    }
}
