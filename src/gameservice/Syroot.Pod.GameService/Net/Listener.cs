﻿using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace Syroot.Pod.GameService.Net
{
    public class Listener : IListener
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly Channel<Socket> _queue = Channel.CreateBounded<Socket>(new BoundedChannelOptions(100)
        {
            FullMode = BoundedChannelFullMode.Wait
        });

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public async Task ListenAsync(IPEndPoint endPoint, CancellationToken ct = default)
        {
            Socket listenSocket = new(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listenSocket.Bind(endPoint);
            listenSocket.Listen();

            while (true)
            {
                Socket clientSocket = await listenSocket.AcceptAsync(ct);
                await _queue.Writer.WriteAsync(clientSocket, ct);
            }
        }

        /// <inheritdoc/>
        public bool TryAccept([NotNullWhen(true)] out Socket? socket)
        {
            return _queue.Reader.TryRead(out socket);
        }
    }
}
