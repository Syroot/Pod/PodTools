﻿using System.Threading.Tasks;
using Syroot.Pod.GameService.Messages;

namespace Syroot.Pod.GameService.Net
{
    /// <summary>
    /// Represents a reflection of a class to dispatch <see cref="IGsMessage"/> instances to handling methods.
    /// </summary>
    public interface IMessageDispatcher
    {
        // ---- METHODS ------------------------------------------------------------------------------------------------

        void Bind(Hoster instance);
        ValueTask<bool> Dispatch(Client sender, IGsMessage message);
    }

    // TODO
    ///// <summary>
    ///// Represents a decoration for a message handling method accepting a <see cref="GameMessage"/>.
    ///// </summary>
    //[AttributeUsage(AttributeTargets.Method)]
    //public class GameHandlerAttribute : Attribute
    //{
    //}
}
