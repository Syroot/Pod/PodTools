﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Syroot.Pod.GameService.Configuration;
using Syroot.Pod.GameService.RouterRegistry;

namespace Syroot.Pod.Launcher
{
    /// <summary>
    /// Represents the main class of the application containing the program entry point.
    /// </summary>
    internal static class Program
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly static IRouterRegistry _registry = new WebRegistry();
        private readonly static string[] _podExes = new[]
        {
            "podatix",
            "podd3d5x",
            "podd3dx",
            "podfxm64",
            "podmax64",
            "podmmx",
            "podpvrx",
            "pods3dx",
            "podx3dfx",
            "winpod",
            "wpod3dfx",
            "wpodati",
            "wpodd3d",
            "wpodd3d5",
            "wpodpvr",
            "wpods3d"
        };

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private static async Task Main(string[] args)
        {
            try
            {
                // Find POD executable.
                string? exeName = FindExecutable(args.Length == 1 ? args[0] : null);
                if (exeName == null)
                    throw new FileNotFoundException("Could not find POD executable.");

                // Download current router list.
                Console.WriteLine($"Downloading router list...");
                IList<RegisteredRouter> routers = await _registry.GetRouters();

                // Download FAQs.
                Console.WriteLine($"Downloading latest news...");
                string faqs = await _registry.GetFaqs();
                using (StreamWriter stream = File.CreateText("faqs.txt"))
                {
                    await stream.WriteLineAsync(faqs);
                    await stream.WriteLineAsync($"Last update ran on {DateTime.Now:yyyy-MM-dd HH: mm}.");
                }

                UpdateConfig(routers);

                // Start POD.
                Console.WriteLine($"Starting {Path.GetFileName(exeName)}...");
                Process.Start(exeName);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                Console.ReadKey();
            }
        }

        private static string? FindExecutable(string? forcedName = null)
        {
            IList<string> exes = forcedName == null ? _podExes : new[] { forcedName };
            foreach (string exe in exes)
            {
                string fullPath = Path.ChangeExtension(Path.GetFullPath(exe), "exe");
                if (File.Exists(fullPath))
                    return fullPath;
            }

            return null;
        }

        private static void UpdateConfig(IList<RegisteredRouter> routers)
        {
            GsConfig config = new();

            // Load existing configuration.
            if (File.Exists("gs.bin"))
            {
                Console.WriteLine($"Loading GS.bin...");
                using FileStream stream = File.OpenRead("gs.bin");
                config.Load(stream);
            }

            // Create new configuration contents.
            if (String.IsNullOrEmpty(config.PlayerName))
                config.PlayerName = "USERNAME";
            if (String.IsNullOrEmpty(config.Password))
                config.Password = "PASSWORD";

            config.Routers.Clear();
            foreach (RegisteredRouter router in routers)
            {
                config.Routers.Add(new()
                {
                    Name = router.Name,
                    EndPoint = router.EndPoint
                });
            }

            if (String.IsNullOrEmpty(config.LastUsedServer))
                config.LastUsedServer = "-";

            // Save gs.bin.
            Console.WriteLine($"Saving GS.bin...");
            using (FileStream stream = File.Create("gs.bin"))
                config.Save(stream);
        }
    }
}
