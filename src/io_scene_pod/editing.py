import bmesh as BM
import bpy
import bpy.props
import bpy.types

from .common import FACE_LAYER_NAME, FACE_LAYER_PROPS

current_bm = None
pod_face_updating = False


def register() -> None:
    bpy.types.WindowManager.pod_face = bpy.props.PointerProperty(name="POD Face",
        description="Additional POD related mesh face properties",
        type=POD_FaceProperties)
    bpy.app.handlers.depsgraph_update_post.append(depsgraph_update_post_handler)


def unregister() -> None:
    del bpy.types.WindowManager.pod_face
    bpy.app.handlers.depsgraph_update_post.remove(depsgraph_update_post_handler)


class POD_FaceProperties(bpy.types.PropertyGroup):
    def name_update(self, context: bpy.types.Context) -> None:
        if not pod_face_updating:
            selected_faces = (face for face in current_bm.faces if face.select)
            layer = current_bm.faces.layers.string[FACE_LAYER_NAME]
            for face in selected_faces:
                face[layer] = self.name.encode()

    def props_update(self, context: bpy.types.Context) -> None:
        if not pod_face_updating:
            selected_faces = (face for face in current_bm.faces if face.select)
            layer = current_bm.faces.layers.int[FACE_LAYER_PROPS]
            for face in selected_faces:
                face[layer] = self.props

    def visible_get(self) -> bool:
        return bool(self.props & 0b1)

    def visible_set(self, value: bool) -> None:
        if value:
            self.props |= 0b1
        else:
            self.props &= ~0b1

    def road_get(self) -> bool:
        return bool(self.props & 0b1000)

    def road_set(self, value: bool) -> None:
        if value:
            self.props |= 0b1000
        else:
            self.props &= ~0b1000

    def wall_get(self) -> bool:
        return bool(self.props & 0b100000)

    def wall_set(self, value: bool) -> None:
        if value:
            self.props |= 0b100000
        else:
            self.props &= ~0b100000

    def tex_level_get(self) -> int:
        return self.props >> 6 & 0b11

    def tex_level_set(self, value: int) -> None:
        new = self.props
        new &= ~(0b11 << 6)
        new |= (value & 0b11) << 6
        self.props = new

    def black_get(self) -> bool:
        return bool(self.props & 0b100000000)

    def black_set(self, value: bool) -> None:
        if value:
            self.props |= 0b100000000
        else:
            self.props &= ~0b100000000

    def two_side_get(self) -> bool:
        return bool(self.props & 0b10000000000)

    def two_side_set(self, value: bool) -> None:
        if value:
            self.props |= 0b10000000000
        else:
            self.props &= ~0b10000000000

    def dural_get(self) -> bool:
        return bool(self.props & 0b10000000000000)

    def dural_set(self, value: bool) -> None:
        if value:
            self.props |= 0b10000000000000
        else:
            self.props &= ~0b10000000000000

    def slip_get(self) -> int:
        return self.props >> 16 & 0xFF

    def slip_set(self, value: int) -> None:
        new = self.props
        new &= ~(0xFF << 16)
        new |= (value & 0xFF) << 16
        self.props = new

    name: bpy.props.StringProperty(name="Name",
        description="The internal name of this face. Has no effect in-game",
        update=name_update)
    props: bpy.props.IntProperty(name="Raw Value",
        description="The resulting flags integer",
        default=0, min=0,
        update=props_update)
    visible: bpy.props.BoolProperty(name="Visible",
        description="If unchecked, this face will not be rendered",
        get=visible_get, set=visible_set)
    road: bpy.props.BoolProperty(name="Road",
        description="Applies road physics, making it possible to accelerate on this surface",
        get=road_get, set=road_set)
    wall: bpy.props.BoolProperty(name="Wall",
        description="Applies wall physics, making it impossible to accelerate on this surface",
        get=wall_get, set=wall_set)
    tex_level: bpy.props.IntProperty(name="Graphic Level",
        description="The required graphic level at which a texture will be drawn instead of a replacement color. Not "
                    "handled by all graphics backends (notably 3dfx). 0 always draws the texture",
        default=0, min=0, max=3,
        get=tex_level_get, set=tex_level_set)
    black: bpy.props.BoolProperty(name="Transparent",
        description="When enabled, black color will not be drawn to enable transparent parts",
        get=black_get, set=black_set)
    two_side: bpy.props.BoolProperty(name="2-sided",
        description="Renders the polygon from both sides",
        get=two_side_get, set=two_side_set)
    dural: bpy.props.BoolProperty(name="Dural",
        description="Renders a metallic texture like the one applied to the player car when the DURAL cheat was entered",
        get=dural_get, set=dural_set)
    slip: bpy.props.IntProperty(name="Slipperyness",
        description="The amount of friction on this surface. 0 for default friction (full steering control)",
        default=0, min=0, max=255,
        get=slip_get, set=slip_set)


class POD_OT_AddFaceLayers(bpy.types.Operator):
    """Adds face layers to the current mesh for storing POD face names and properties"""
    bl_idname = "mesh.pod_add_face_layers"
    bl_label = "Add POD Face Layers"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return current_bm is not None

    def execute(self, context: bpy.types.Context):
        # Add string name layer.
        layer = current_bm.faces.layers.string.get(FACE_LAYER_NAME)
        if not layer:
            current_bm.faces.layers.string.new(FACE_LAYER_NAME)
        # Add integral properties layer.
        layer = current_bm.faces.layers.int.get(FACE_LAYER_PROPS)
        if not layer:
            current_bm.faces.layers.int.new(FACE_LAYER_PROPS)
        # Update WindowManager properties to not show stale values from previously deleted layers.
        depsgraph_update_post_handler(bpy.context.scene)
        return {'FINISHED'}


class POD_OT_RemoveFaceLayers(bpy.types.Operator):
    """Removes the face layers of the current mesh storing POD face names and properties"""
    bl_idname = "mesh.pod_remove_face_layers"
    bl_label = "Remove POD Face Layers"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return current_bm is not None

    def execute(self, context: bpy.types.Context):
        # Remove string name layer.
        layer = current_bm.faces.layers.string.get(FACE_LAYER_NAME)
        if layer:
            current_bm.faces.layers.string.remove(layer)
        # Add integral properties layer.
        layer = current_bm.faces.layers.int.get(FACE_LAYER_PROPS)
        if layer:
            current_bm.faces.layers.int.remove(layer)
        return {'FINISHED'}


class POD_PT_FacePanel(bpy.types.Panel):
    bl_label = "Face Properties"
    bl_category = "POD"
    bl_region_type = 'UI'
    bl_space_type = 'VIEW_3D'

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return current_bm is not None

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        pod_face = context.window_manager.pod_face
        sel_faces = [f for f in current_bm.faces if f.select]
        if not any(sel_faces):
            self.layout.label(text="No face selected")
            return
        # Draw name layer.
        layer = current_bm.faces.layers.string.get(FACE_LAYER_NAME)
        if layer:
            self.layout.prop(pod_face, "name")
        # Draw props layer.
        layer = current_bm.faces.layers.int.get(FACE_LAYER_PROPS)
        if layer:
            row = layout.row()
            row.prop(pod_face, "road")
            row.prop(pod_face, "wall")
            layout.prop(pod_face, "slip")
            row = layout.row()
            row.prop(pod_face, "visible")
            row.prop(pod_face, "two_side")
            row = self.layout.row()
            row.prop(pod_face, "black")
            row.prop(pod_face, "dural")
            layout.prop(pod_face, "tex_level")
            layout.label(text="Flag Debug")
            layout.prop(pod_face, "props")
            row = layout.row()
            row.label(text=format(pod_face.props >> 24 & 0xFF, "#010b"))
            row.label(text=format(pod_face.props >> 16 & 0xFF, "#010b"))
            row = layout.row()
            row.label(text=format(pod_face.props >> 8 & 0xFF, "#010b"))
            row.label(text=format(pod_face.props & 0xFF, "#010b"))
            layout.operator(POD_OT_RemoveFaceLayers.bl_idname, icon='X', text="Remove POD properties")
        else:
            layout.operator(POD_OT_AddFaceLayers.bl_idname, icon='ADD', text="Add POD properties")


@bpy.app.handlers.persistent
def depsgraph_update_post_handler(scene: bpy.types.Scene) -> None:
    global current_bm, pod_face_updating
    if bpy.context.mode == 'EDIT_MESH':
        # Get the active face from the currently edited mesh.
        current_bm = current_bm or BM.from_edit_mesh(bpy.context.object.data)
        try:
            active_face = current_bm.faces.active
        except ReferenceError:  # Mesh instance changed
            current_bm = BM.from_edit_mesh(bpy.context.object.data)
            active_face = current_bm.faces.active
        # Display properties of active face.
        if active_face:
            pod_face_updating = True
            layer = current_bm.faces.layers.string.get(FACE_LAYER_NAME)
            if layer:
                bpy.context.window_manager.pod_face.name = active_face[layer].decode()
            layer = current_bm.faces.layers.int.get(FACE_LAYER_PROPS)
            if layer:
                bpy.context.window_manager.pod_face.props = active_face[layer]
            pod_face_updating = False
    else:
        current_bm = None


classes = (
    POD_FaceProperties,
    POD_OT_AddFaceLayers,
    POD_OT_RemoveFaceLayers,
    POD_PT_FacePanel,
)
