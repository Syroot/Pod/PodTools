from typing import Any, Dict


bl_info = {
    "name": "POD (Planet of Death) formats",
    "description": "Import-Export POD cars and tracks",
    "author": "Syroot",
    "version": (0, 3, 3),
    "blender": (3, 4, 1),
    "location": "File > Import-Export",
    "warning": "This add-on is under development.",
    "wiki_url": "https://gitlab.com/Syroot/Pod",
    "tracker_url": "https://gitlab.com/Syroot/Pod/issues",
    "support": 'COMMUNITY',
    "category": "Import-Export"
}


def reload_package(module_dict_main: Dict[str, Any]) -> None:
    import importlib
    from pathlib import Path

    def reload_package_recursive(current_dir, module_dict):
        for path in current_dir.iterdir():
            if "__init__" in str(path) or path.stem not in module_dict:
                continue

            if path.is_file() and path.suffix == ".py":
                importlib.reload(module_dict[path.stem])
            elif path.is_dir():
                reload_package_recursive(path, module_dict[path.stem].__dict__)

    reload_package_recursive(Path(__file__).parent, module_dict_main)


if "bpy" in locals():
    reload_package(locals())

import bpy
from . import editing
from . import editing_car
from . import exporting_car
from . import importing_car
from . import importing_track


modules = (
    editing,
    editing_car,
    exporting_car,
    importing_car,
    importing_track,
)


def register() -> None:
    for module in modules:
        [bpy.utils.register_class(cls) for cls in module.classes]
        module.register()


def unregister() -> None:
    for module in modules:
        [bpy.utils.unregister_class(cls) for cls in module.classes]
        module.unregister()
