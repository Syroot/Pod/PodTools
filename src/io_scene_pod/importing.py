from __future__ import annotations

from typing import Dict, List, Tuple

import bmesh as BM
import bpy
import bpy.types
from bpy_extras.io_utils import ImportHelper

from . import btools
from .pod.common import Mesh, Texture, TextureFormat
from .common import FACE_LAYER_NAME, FACE_LAYER_PROPS


class PodImportHelper(ImportHelper):
    mats: List[bpy.types.Material]  # stores textures first, then color materials
    color_mat_indices: Dict[int, int]  # maps color to materials index

    def __init__(self) -> None:
        self.mats = []
        self.color_mat_indices = {}

    def create_collection(self, name: str) -> bpy.types.Collection:
        bcol = bpy.data.collections.new(name)
        bpy.context.scene.collection.children.link(bcol)
        return bcol

    def create_mesh_object(self, mesh: Mesh, name: str, col: bpy.types.Collection,
                           loc: Tuple[float, float, float] = (0.0, 0.0, 0.0)) -> bpy.types.Object:
        bmesh = self.create_mesh(mesh, name)
        bobj = bpy.data.objects.new(name, bmesh)
        col.objects.link(bobj)
        bobj.location = loc
        return bobj

    def create_mesh(self, mesh: Mesh, name: str) -> bpy.types.Mesh:
        # Create new mesh.
        bmesh = bpy.data.meshes.new(name)
        [bmesh.materials.append(m) for m in self.mats]
        bm = BM.new()
        # Create vertices.
        for position in mesh.positions:
            bm.verts.new(position)
        bm.verts.ensure_lookup_table()
        # Create faces.
        layer_name = bm.faces.layers.string.new(FACE_LAYER_NAME)
        layer_props = bm.faces.layers.int.new(FACE_LAYER_PROPS)
        b_uv = bm.loops.layers.uv.new()
        for face in mesh.faces:
            try:
                bface = bm.faces.new((bm.verts[face.indices[i]] for i in range(face.vertex_count)))
            except ValueError as e:
                print(e)  # Ignore duplicate faces for now.
                continue
            bface[layer_name] = face.name.encode()
            bface[layer_props] = face.properties
            # TODO: Optimize setting materials, only append those needed by the sector.
            if face.material_type in ('FLAT', 'GOURAUD'):
                # Get an already appended color material or create and append it to the mesh.
                mat_index = self.color_mat_indices.get(face.color)
                if mat_index:
                    bface.material_index = mat_index
                else:
                    r = face.color >> 16 & 0xFF
                    g = face.color >> 8 & 0xFF
                    b = face.color & 0xFF
                    bmat = btools.create_color_material(f"Color_{r}_{g}_{b}", (r / 0xFF, g / 0xFF, b / 0xFF, 1))
                    mat_index = len(self.mats)
                    bmesh.materials.append(bmat)
                    bface.material_index = mat_index
                    self.mats.append(bmat)
                    self.color_mat_indices[face.color] = mat_index
            else:
                bface.material_index = face.texture_index
            # Map UV from 0-255 range (Y is inverted).
            for i in range(face.vertex_count):
                bface.loops[i][b_uv].uv = (face.texture_uvs[i][0] / 0xFF, face.texture_uvs[i][1] / 0xFF)
        bm.to_mesh(bmesh)
        bm.free()
        return bmesh

    def create_texture(self, texture: Texture, size: int, format: TextureFormat) -> None:
        # Generate image name from region names.
        name = "_".join([region.name[:-4] for region in texture.regions]) + ".tga"
        # Create image.
        bimg = bpy.data.images.new(name, width=size, height=size)
        bimg.filepath = name
        # Convert pixel data.
        pixels = [0.0] * size * size * 4
        if format == TextureFormat.RGB565:
            data = memoryview(texture.data).cast("H")
            for i in range(size * size):
                idx = i * 4
                pixel = data[i]
                pixels[idx + 0] = (pixel >> 11) / 0b11111
                pixels[idx + 1] = (pixel >> 5 & 0b111111) / 0b111111
                pixels[idx + 2] = (pixel & 0b11111) / 0b11111
                pixels[idx + 3] = 1
        else:
            raise NotImplementedError("Unknown texture format.")
        bimg.pixels = pixels
        bimg.pack()
        # Create material list.
        bmat = btools.create_image_material(name, bimg)
        self.mats.append(bmat)
