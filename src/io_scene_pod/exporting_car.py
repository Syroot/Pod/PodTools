from __future__ import annotations

import itertools
from enum import Enum, auto
from pathlib import Path
from typing import List, Tuple

import bpy
import bpy.props
import bpy.types

from .editing_car import POD_CarPart
from .exporting import PodExportHelper
from .pod.cars import Car, Fx, Part, Spec
from .pod.common import TextureFormat
from .pod.pbdf import PbdfReader, PbdfWriter


def menu_func_export(self, context: bpy.types.Context) -> None:
    self.layout.operator(POD_OT_CarExport.bl_idname, text="POD Car (.bv4)")


def register() -> None:
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)


def unregister() -> None:
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)


class ChassisPart(Enum):
    FL = auto()
    FR = auto()
    RL = auto()
    RR = auto()
    SL = auto()
    SR = auto()


class WheelPart(Enum):
    FL = auto()
    FR = auto()
    RL = auto()
    RR = auto()


class ShadowPart(Enum):
    F = auto()
    R = auto()


class POD_OT_CarExport(bpy.types.Operator, PodExportHelper):
    """Save a POD BV4 car file"""
    bl_idname = "export_scene.bv4"
    bl_label = "Export POD Car"
    bl_options = {'UNDO'}
    filename_ext = ".bv4"
    filter_glob: bpy.props.StringProperty(
        default="*.bv4",
        options={'HIDDEN'})
    _chassis_names = [f"Chassis{p}{d}" for (d, p) in itertools.product(range(3), [x.name for x in ChassisPart])]

    def execute(self, context:  bpy.types.Context):
        # Validate before attempting to export.
        if not self.validate(context):
            return {'CANCELLED'}
        # Create car from base.
        file_path = self.properties.filepath
        scene = context.scene
        with PbdfReader(open(scene.pod_car.base_file, "rb")) as pbdf:
            car = pbdf.read_any(Car)
        car.name = (scene.pod_car.name or Path(file_path).stem)[:31]
        # Convert parts.
        def convert_part(part: List[float], props: POD_CarPart, location: Tuple[float, float, float]) -> None:
            part[Part.POSITION_X] = location[0]
            part[Part.POSITION_Y] = location[1]
            part[Part.POSITION_Z] = location[2]
            part[Part.SPRING_1] = props.spring_1
            part[Part.SPRING_2] = props.spring_2
            part[Part.TRAVEL_MIN] = props.travel_min
            part[Part.TRAVEL_MAX] = props.travel_max
        convert_part(car.physics.wheel_fr, scene.pod_car_wheel_fr, scene.objects["WheelFR"].location)
        convert_part(car.physics.wheel_rr, scene.pod_car_wheel_rr, scene.objects["WheelRR"].location)
        convert_part(car.physics.wheel_fl, scene.pod_car_wheel_fl, scene.objects["WheelFL"].location)
        convert_part(car.physics.wheel_rl, scene.pod_car_wheel_rl, scene.objects["WheelRL"].location)
        convert_part(car.physics.chassis, scene.pod_car_chassis, scene.objects["ChassisRR0"].location)
        # Convert physics.
        physics = scene.pod_car_physics
        car.physics.spec[Spec.FRONT_ACCEL] = physics.front_accel
        car.physics.spec[Spec.REAR_ACCEL] = physics.rear_accel
        car.physics.spec[Spec.SPRING_DAMPER_1] = physics.spring_damper_1
        car.physics.spec[Spec.SPRING_DAMPER_2] = physics.spring_damper_2
        car.physics.spec[Spec.WEIGHT] = physics.weight
        car.physics.spec[Spec.SPEED_FACTOR] = physics.speed_factor
        car.physics.spec[Spec.ACCEL_FACTOR] = physics.accel_factor
        # Convert settings.
        settings = scene.pod_car_settings
        car.settings.accelerate = settings.accelerate
        car.settings.brakes = settings.brakes
        car.settings.grip = settings.grip
        car.settings.handling = settings.handling
        car.settings.speed = settings.speed
        # Convert sound.
        sound = scene.pod_car_sound
        car.sound.fx[Fx.ENGINE_START] = sound.engine_start
        car.sound.fx[1] = sound.unknown_1
        car.sound.fx[2] = sound.unknown_2
        car.sound.fx[Fx.SKID] = sound.skid
        car.sound.fx[Fx.SKID_END] = sound.skid_end
        car.sound.fx[5] = sound.unknown_5
        car.sound.fx[6] = sound.unknown_6
        car.sound.fx[Fx.HIT_WALL] = sound.hit_wall
        car.sound.fx[Fx.ACCEL_START] = sound.accel_start
        car.sound.fx[Fx.ACCEL_END_1] = sound.accel_end_1
        car.sound.fx[Fx.ACCEL_END_2] = sound.accel_end_2
        car.sound.fx[Fx.ENGINE_STOP] = sound.engine_stop
        car.sound.fx[Fx.HIT_GROUND] = sound.hit_ground
        car.sound.fx[Fx.FLIPPED_START] = sound.flipped_start
        car.sound.fx[Fx.FLIPPED] = sound.flipped
        car.sound.fx[15] = sound.unknown_15
        # Convert chassis meshes, collecting textures on the way.
        # TODO: Use fallback meshes if they do not exist (s. validate() for the logic in determining them).
        self.create_mesh(context, scene.objects["ChassisRR0"], car.objects.chassisRR0.mesh)
        self.create_mesh(context, scene.objects["ChassisRL0"], car.objects.chassisRL0.mesh)
        self.create_mesh(context, scene.objects["ChassisSR0"], car.objects.chassisSR0.mesh)
        self.create_mesh(context, scene.objects["ChassisSL0"], car.objects.chassisSL0.mesh)
        self.create_mesh(context, scene.objects["ChassisFR0"], car.objects.chassisFR0.mesh)
        self.create_mesh(context, scene.objects["ChassisFL0"], car.objects.chassisFL0.mesh)
        self.create_mesh(context, scene.objects["ChassisRR1"], car.objects.chassisRR1.mesh)
        self.create_mesh(context, scene.objects["ChassisRL1"], car.objects.chassisRL1.mesh)
        self.create_mesh(context, scene.objects["ChassisSR1"], car.objects.chassisSR1.mesh)
        self.create_mesh(context, scene.objects["ChassisSL1"], car.objects.chassisSL1.mesh)
        self.create_mesh(context, scene.objects["ChassisFR1"], car.objects.chassisFR1.mesh)
        self.create_mesh(context, scene.objects["ChassisFL1"], car.objects.chassisFL1.mesh)
        self.create_mesh(context, scene.objects["ChassisRR2"], car.objects.chassisRR2.mesh)
        self.create_mesh(context, scene.objects["ChassisRL2"], car.objects.chassisRL2.mesh)
        self.create_mesh(context, scene.objects["ChassisSR2"], car.objects.chassisSR2.mesh)
        self.create_mesh(context, scene.objects["ChassisSL2"], car.objects.chassisSL2.mesh)
        self.create_mesh(context, scene.objects["ChassisFR2"], car.objects.chassisFR2.mesh)
        self.create_mesh(context, scene.objects["ChassisFL2"], car.objects.chassisFL2.mesh)
        # Convert material.
        car.material.name = car.name.upper()
        car.material.texture_list.flags = 0
        car.material.texture_list.textures.clear()
        for image in self.texture_mat_images:
            car.material.texture_list.textures.append(self.create_texture(image, TextureFormat.RGB565))
        # Write to file.
        with PbdfWriter(open(file_path, "w+b"), key=0xF2E, buf_size=0x4000, ofs_count=1, crypt_tail=False) as pbdf:
            pbdf.write_any(car)
        return {'FINISHED'}

    def validate(self, context: bpy.types.Context) -> bool:
        scene = context.scene

        valid = True
        def fail(msg: str) -> None:
            nonlocal valid
            self.report({'ERROR'}, "ERROR: " + msg)
            valid = False
        def warn(msg: str) -> None:
            self.report({'WARNING'}, msg)

        # Validate settings.
        if not Path(scene.pod_car.base_file).is_file():
            fail("Could not find base car file. Specify a valid file in Scene property panel.")

        # Validate existence of chassis (require undamaged and warn about missing damaged or ruined).
        for part in ChassisPart:
            if not scene.objects.get(f"Chassis{part.name}0"):
                fail(f"Missing 'Chassis{part.name}0' undamaged mesh.")
                continue
            if not scene.objects.get(f"Chassis{part.name}1"):
                if scene.objects.get(f"Chassis{part.name}2"):
                    warn(f"Missing 'Chassis{part.name}1' damaged mesh will be replaced with ruined variant.")
                else:
                    warn(f"Missing 'Chassis{part.name}1' damaged mesh will be replaced with undamaged variant.")
            if not scene.objects.get(f"Chassis{part.name}2"):
                if scene.objects.get(f"Chassis{part.name}1"):
                    warn(f"Missing 'Chassis{part.name}2' ruined mesh will be replaced with damaged variant.")
                else:
                    warn(f"Missing 'Chassis{part.name}2' ruined mesh will be replaced with undamaged variant.")
        # Validate chassis parts being at same location.
        chassis_loc = None
        for bobj in scene.objects:
            if not bobj.name in self._chassis_names:
                continue
            if chassis_loc and chassis_loc != bobj.location:
                fail("Chassis meshes must have the same origin.")
            else:
                chassis_loc = bobj.location
        # Validate existence of wheels.
        for part in WheelPart:
            if not scene.objects.get(f"Wheel{part.name}"):
                fail(f"Missing 'Wheel{part.name}' mesh.")
        # Validate existence of shadows (require undamaged and warn about missing ruined).
        for part in ShadowPart:
            if not scene.objects.get(f"Shadow{part.name}0"):
                fail(f"Missing 'Shadow{part.name}0' undamaged mesh.")
                continue
            if not scene.objects.get(f"Shadow{part.name}2"):
                warn(f"Missing 'Shadow{part.name}2' ruined mesh will be replaced with normal variant.")
        # Validate existence of LoD models.
        for i in range(1, 2):
            if not scene.objects.get(f"Lod{i}"):
                fail(f"Missing 'Lod{i}' mesh.")

        # Validate car settings.
        total = scene.pod_car_settings.get_total()
        if total > 300:
            fail("Car settings exceed 300 points, game will exit on load. Validate settings in Scene property panel.")
        elif total < 300:
            warn("Car settings are less than 300 points, this is untypical.")

        return valid


classes = (
    POD_OT_CarExport,
)
