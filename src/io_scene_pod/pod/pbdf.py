from __future__ import annotations

import io
import struct
from typing import Any, BinaryIO, List, Tuple, Type, TypeVar


class PbdfReader:
    key: int
    buf_size: int
    encoding: str
    ofs: List[int]
    _file: BinaryIO
    _buf: bytearray
    _buf_index: int
    _buf_pos: int
    T = TypeVar("T")

    def __init__(self, file: BinaryIO, key: int = 0, buf_size: int = 0, encoding: str = "cp1252") -> None:
        self._file = file
        self.key = key
        self.buf_size = buf_size
        self.encoding = encoding

    def __enter__(self) -> PbdfReader:
        # Determine file size.
        start = self._file.tell()
        self._file.seek(0, io.SEEK_END)
        file_size = self._file.tell() - start
        self._file.seek(start)

        # Determine key.
        if not self.key:
            self.key = struct.unpack("<I", self._file.read(4))[0] ^ file_size
            self._file.seek(start)

        # Determine buffer size.
        if not self.buf_size:
            checksum = 0
            pos = start
            while pos < file_size:
                dword = struct.unpack("<I", self._file.read(4))[0]
                pos = self._file.tell()
                if dword == checksum and file_size % pos == 0:
                    self.buf_size = pos
                    self._file.seek(start)
                    break
                checksum += dword ^ self.key
                checksum &= 0xFFFFFFFF
            if not self.buf_size:
                raise IOError("Could not determine PBDF buffer size.")
        self._buf = None
        self._buf_pos = self.buf_size

        # Read header.
        if self.read_u32() != file_size:
            raise IOError("Bad PBDF file size.")
        self.ofs = [self.read_u32() for _ in range(self.read_u32())]
        return self

    def __exit__(self, type, value, traceback) -> None:
        self._file.close()

    def offset(self, idx: int) -> None:
        # Seek to offset.
        offset = self.ofs[idx]
        # Update buffer status.
        buf_index = offset // self.buf_size
        if buf_index == self._buf_index:
            self._buf_pos = offset % self.buf_size
        else:
            self._buf_index = buf_index
            self._buf_pos = self.buf_size
            self._file.seek(self._buf_index * self.buf_size)

    def read(self, count: int) -> bytes:
        result = bytearray(count)
        pos = 0
        while pos < count:
            bytes_remain = self.buf_size - 4 - self._buf_pos
            if bytes_remain > 0:
                size = min(bytes_remain, count - pos)
                result[pos:pos + size] = self._buf[self._buf_pos:self._buf_pos + size]
                pos += size
                self._buf_pos += size
            else:
                self._read_buf()
        return result

    def read_any(self, t: Type[T], *args) -> T:
        value = t()
        value.read(self, *args)
        return value

    def read_f(self) -> float:
        return self.read_i32() / (1 << 16)  # fixed point 16x16

    def read_i8(self) -> int:
        return struct.unpack("<b", self.read(1))[0]

    def read_i16(self) -> int:
        return struct.unpack("<h", self.read(2))[0]

    def read_i32(self) -> int:
        return struct.unpack("<i", self.read(4))[0]

    def read_s(self) -> str:
        return bytes((c ^ ~i) & 0xFF for i, c in enumerate(self.read(self.read_u8()))).decode(self.encoding)

    def read_u8(self) -> int:
        return struct.unpack("<B", self.read(1))[0]

    def read_u16(self) -> int:
        return struct.unpack("<H", self.read(2))[0]

    def read_u32(self) -> int:
        return struct.unpack("<I", self.read(4))[0]

    def read_vec2u(self) -> Tuple[int, int]:
        return (self.read_u32(), self.read_u32())

    def read_vec3(self) -> Tuple[float, float, float]:
        return (self.read_f(), self.read_f(), self.read_f())

    def _read_buf(self) -> None:
        # Update buffer status.
        if self._buf:
            self._buf_index += 1
        else:
            self._buf = bytearray(self.buf_size)
            self._buf_index = 0
        self._buf_pos = 0

        # Read buffer.
        if self._file.readinto(self._buf) != self.buf_size:
            raise IOError("Could not read complete PBDF buffer.")

        # Decrypt buffer.
        checksum = 0
        if self._buf_index and self.key in [0x00005CA8, 0x0000D13F]:
            # Special encryption with specific keys in second and later buffers.
            enc_dword = 0
            for i in range(0, self.buf_size - 4, 4):
                key_dword = 0
                cmd = enc_dword >> 16 & 3
                if cmd == 0: key_dword = (enc_dword - 0x50A4A89D)
                elif cmd == 1: key_dword = (0x3AF70BC4 - enc_dword)
                elif cmd == 2: key_dword = (enc_dword + 0x07091971) << 1
                elif cmd == 3: key_dword = (0x11E67319 - enc_dword) << 1
                enc_dword = struct.unpack_from("<I", self._buf, i)[0]
                cmd = enc_dword & 3
                if cmd == 0: dec_dword = ~enc_dword ^ key_dword
                elif cmd == 1: dec_dword = ~enc_dword ^ ~key_dword
                elif cmd == 2: dec_dword = enc_dword ^ ~key_dword
                elif cmd == 3: dec_dword = enc_dword ^ key_dword ^ 0xFFFF
                dec_dword &= 0xFFFFFFFF
                struct.pack_into("<I", self._buf, i, dec_dword)
                checksum += dec_dword
                checksum &= 0xFFFFFFFF
        else:
            # Simple encryption for all buffers with most keys.
            for i in range(0, self.buf_size - 4, 4):
                enc_dword = struct.unpack_from("<I", self._buf, i)[0]
                dec_dword = enc_dword ^ self.key
                struct.pack_into("<I", self._buf, i, dec_dword)
                checksum += dec_dword
                checksum &= 0xFFFFFFFF
        # Validate the checksum.
        if checksum != struct.unpack_from("<I", self._buf, i + 4)[0]:
            raise IOError("Bad PBDF buffer checksum.")


class PbdfWriter:
    key: int
    buf_size: int
    encoding: str
    crypt_tail: bool
    _file: BinaryIO
    _buf: bytearray
    _buf_pos: int
    _ofs_count: int
    _ofs: List[int]

    def __init__(self, file: BinaryIO, key: int, buf_size: int, ofs_count: int,
                 encoding: str = "cp1252", crypt_tail: bool = True) -> None:
        self._file = file
        self.key = key
        self.buf_size = buf_size
        self._ofs_count = ofs_count
        self._ofs = []
        self.encoding = encoding
        self.crypt_tail = crypt_tail

    def __enter__(self) -> PbdfWriter:
        # Initialize buffering and reserve header.
        self._buf = bytearray(self.buf_size)
        self._buf_pos = 4 + 4 + 4 * self._ofs_count
        return self

    def __exit__(self, type, value, traceback) -> None:
        if len(self._ofs) != self._ofs_count:
            raise IOError(f"{self._ofs_count - len(self._ofs)} PBDF offsets have not been satisfied.")

        # Write a pending buffer.
        data_size = self._file.tell() + self._buf_pos
        if self._buf_pos > 0:
            self._write_buf()

        # Update header.
        file_size = self._file.tell()
        self._file.seek(0)
        self._file.write(struct.pack("<I", file_size))
        self._file.write(struct.pack("<I", self._ofs_count))
        self._file.write(struct.pack(f"<{self._ofs_count}I", *self._ofs))

        # Enrypt buffers.
        self._file.seek(0)
        for buf_index in range(0, file_size // self.buf_size):
            self._file.readinto(self._buf)
            checksum = 0
            if buf_index and self.key in [0x00005CA8, 0x0000D13F]:
                # Special encryption with specific keys in second and later buffers.
                for i in range(0, self.buf_size - 4, 4):
                    dec_dword = struct.unpack_from("<I", self._buf, i)[0]
                    if self.crypt_tail or buf_index * self.buf_size + i < data_size:
                        key_dword = 0
                        cmd = dec_dword >> 16 & 3
                        if cmd == 0: key_dword = (dec_dword - 0x50A4A89D)
                        elif cmd == 1: key_dword = (0x3AF70BC4 - dec_dword)
                        elif cmd == 2: key_dword = (dec_dword + 0x07091971) << 1
                        elif cmd == 3: key_dword = (0x11E67319 - dec_dword) << 1
                        cmd = dec_dword & 3
                        if cmd == 0: enc_dword = ~dec_dword ^ key_dword
                        elif cmd == 1: enc_dword = ~dec_dword ^ ~key_dword
                        elif cmd == 2: enc_dword = dec_dword ^ ~key_dword
                        elif cmd == 3: enc_dword = dec_dword ^ key_dword ^ 0xFFFF
                        enc_dword &= 0xFFFFFFFF
                        struct.pack_into("<I", self._buf, i, enc_dword)
                    else:
                        dec_dword ^= self.key
                    checksum += dec_dword
                    checksum &= 0xFFFFFFFF
            else:
                # Simple encryption for all buffers with most keys.
                for i in range(0, self.buf_size - 4, 4):
                    dec_dword = struct.unpack_from("<I", self._buf, i)[0]
                    if self.crypt_tail or buf_index * self.buf_size + i < data_size:
                        struct.pack_into("<I", self._buf, i, dec_dword ^ self.key)
                    else:
                        dec_dword ^= self.key
                    checksum += dec_dword
                    checksum &= 0xFFFFFFFF
            # Write the checksum and buffer.
            struct.pack_into("<I", self._buf, i + 4, checksum)
            self._file.seek(-self.buf_size, io.SEEK_CUR)
            self._file.write(self._buf)

        # Close file.
        self._file.close()

    def offset(self) -> None:
        if len(self._ofs) == self._ofs_count:
            raise IOError("No more PBDF offsets available.")
        self._ofs.append(self._file.tell() + self._buf_pos)

    def write(self, value: bytes) -> None:
        pos = 0
        while pos < len(value):
            bytes_remain = self.buf_size - 4 - self._buf_pos
            if bytes_remain > 0:
                size = min(bytes_remain, len(value) - pos)
                self._buf[self._buf_pos:self._buf_pos + size] = value[pos:pos + size]
                pos += size
                self._buf_pos += size
            else:
                self._write_buf()

    def write_any(self, value: Any, *args) -> None:
        value.write(self, *args)

    def write_f(self, value: float) -> None:
        self.write_i32(int(value * (1 << 16)))  # fixed point 16x16

    def write_i8(self, value: int) -> None:
        self.write(struct.pack("<b", value))

    def write_i16(self, value: int) -> None:
        self.write(struct.pack("<h", value))

    def write_i32(self, value: int) -> None:
        self.write(struct.pack("<i", value))

    def write_s(self, value: str) -> None:
        self.write_u8(len(value))
        self.write(bytes((c ^ ~i) & 0xFF for i, c in enumerate(value.encode(self.encoding))))

    def write_u8(self, value: int) -> None:
        self.write(struct.pack("<B", value))

    def write_u16(self, value: int) -> None:
        self.write(struct.pack("<H", value))

    def write_u32(self, value: int) -> None:
        self.write(struct.pack("<I", value))

    def write_vec2u(self, value: Tuple[int, int]) -> None:
        self.write_u32(value[0])
        self.write_u32(value[1])

    def write_vec3(self, value: Tuple[float, float, float]) -> None:
        self.write_f(value[0])
        self.write_f(value[1])
        self.write_f(value[2])

    def _write_buf(self) -> None:
        # Simply write raw data, checksum and encryption must be done on close.
        self._file.write(self._buf)
        self._buf_pos = 0
