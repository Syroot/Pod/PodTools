from __future__ import annotations

from dataclasses import dataclass, field
from enum import IntEnum
from typing import List, Tuple

from .common import Mesh, TextureFormat, TextureList
from .pbdf import PbdfReader, PbdfWriter


class Part(IntEnum):
    POSITION_X = 18
    POSITION_Y = 19
    POSITION_Z = 20
    SPRING_1 = 43
    SPRING_2 = 44
    GRIP = 45
    TRAVEL_MAX = 46
    TRAVEL_MIN = 47


class Spec(IntEnum):
    MASS = 4
    FRONT_ACCEL = 56
    REAR_ACCEL = 57
    SPRING_DAMPER_1 = 58
    SPRING_DAMPER_2 = 59
    WEIGHT = 60
    SPEED_FACTOR = 65
    ACCEL_FACTOR = 69


@dataclass
class Physics:
    wheel_fr: List[float] = field(default_factory=lambda: [0.0] * 52)  # s. Part
    wheel_rr: List[float] = field(default_factory=lambda: [0.0] * 52)  # s. Part
    wheel_fl: List[float] = field(default_factory=lambda: [0.0] * 52)  # s. Part
    wheel_rl: List[float] = field(default_factory=lambda: [0.0] * 52)  # s. Part
    chassis: List[float] = field(default_factory=lambda: [0.0] * 52)  # s. Part
    spec: List[float] = field(default_factory=lambda: [0.0] * 72)  # s. Spec
    extra: List[float] = field(default_factory=list)

    def read(self, pbdf: PbdfReader) -> None:
        self.wheel_fr = [pbdf.read_f() for _ in range(54)]
        self.wheel_rr = [pbdf.read_f() for _ in range(54)]
        self.wheel_fl = [pbdf.read_f() for _ in range(54)]
        self.wheel_rl = [pbdf.read_f() for _ in range(54)]
        self.chassis = [pbdf.read_f() for _ in range(54)]
        self.spec = [pbdf.read_f() for _ in range(72)]
        self.extra = [pbdf.read_f() for _ in range(pbdf.read_i32())]

    def write(self, pbdf: PbdfWriter) -> None:
        [pbdf.write_f(x) for x in self.wheel_fr]
        [pbdf.write_f(x) for x in self.wheel_rr]
        [pbdf.write_f(x) for x in self.wheel_fl]
        [pbdf.write_f(x) for x in self.wheel_rl]
        [pbdf.write_f(x) for x in self.chassis]
        [pbdf.write_f(x) for x in self.spec]
        pbdf.write_i32(len(self.extra))
        [pbdf.write_f(x) for x in self.extra]


@dataclass
class Material:
    name: str = ""
    texture_list: TextureList = field(default_factory=TextureList)

    def read(self, pbdf: PbdfReader) -> None:
        self.name = pbdf.read_s()
        self.texture_list = pbdf.read_any(TextureList, 128, TextureFormat.RGB565)

    def write(self, pbdf: PbdfWriter) -> None:
        pbdf.write_s(self.name)
        pbdf.write_any(self.texture_list)


@dataclass
class Object:
    mesh: Mesh = field(default_factory=Mesh)
    prism: List[float] = field(default_factory=lambda: [0.0] * 7)

    def read(self, pbdf: PbdfReader, named_faces: int) -> None:
        self.mesh = pbdf.read_any(Mesh, named_faces, False)
        self.prism = [pbdf.read_f() for _ in range(7)]

    def write(self, pbdf: PbdfWriter, named_faces: int) -> None:
        pbdf.write_any(self.mesh, named_faces, False)
        [pbdf.write_f(x) for x in self.prism]


@dataclass
class ObjectShadow:
    mesh: Mesh = field(default_factory=Mesh)

    def read(self, pbdf: PbdfReader, named_faces: int) -> None:
        self.mesh = pbdf.read_any(Mesh, named_faces, False)

    def write(self, pbdf: PbdfWriter, named_faces: int) -> None:
        pbdf.write_any(self.mesh, named_faces, False)


@dataclass
class ObjectLod:
    material_name: str = ""
    named_faces: int = 0
    mesh: Mesh = field(default_factory=Mesh)
    prism: List[float] = field(default_factory=lambda: [0.0] * 7)
    unknown: List[float] = field(default_factory=lambda: [0.0] * 7)

    def read(self, pbdf: PbdfReader, named_faces: int) -> None:
        self.material_name = pbdf.read_s()
        self.named_faces = pbdf.read_u32()
        self.mesh = pbdf.read_any(Mesh, named_faces, False)
        self.prism = [pbdf.read_f() for _ in range(7)]
        self.unknown = [pbdf.read_f() for _ in range(7)]

    def write(self, pbdf: PbdfWriter, named_faces: int) -> None:
        pbdf.write_s(self.material_name)
        pbdf.write_u32(self.named_faces)
        pbdf.write_any(self.mesh, named_faces, False)
        [pbdf.write_f(x) for x in self.prism]
        [pbdf.write_f(x) for x in self.unknown]


@dataclass
class Objects:
    named_faces: int = 0
    chassisRR0: Object = field(default_factory=Object)
    chassisRL0: Object = field(default_factory=Object)
    chassisSR0: Object = field(default_factory=Object)
    chassisSL0: Object = field(default_factory=Object)
    chassisFR0: Object = field(default_factory=Object)
    chassisFL0: Object = field(default_factory=Object)
    chassisRR1: Object = field(default_factory=Object)
    chassisRL1: Object = field(default_factory=Object)
    chassisSR1: Object = field(default_factory=Object)
    chassisSL1: Object = field(default_factory=Object)
    chassisFR1: Object = field(default_factory=Object)
    chassisFL1: Object = field(default_factory=Object)
    chassisRR2: Object = field(default_factory=Object)
    chassisRL2: Object = field(default_factory=Object)
    chassisSR2: Object = field(default_factory=Object)
    chassisSL2: Object = field(default_factory=Object)
    chassisFR2: Object = field(default_factory=Object)
    chassisFL2: Object = field(default_factory=Object)
    wheelFR: Object = field(default_factory=Object)
    wheelRR: Object = field(default_factory=Object)
    wheelFL: Object = field(default_factory=Object)
    wheelRL: Object = field(default_factory=Object)
    shadowR0: ObjectShadow = field(default_factory=ObjectShadow)
    shadowF0: ObjectShadow = field(default_factory=ObjectShadow)
    shadowR2: ObjectShadow = field(default_factory=ObjectShadow)
    shadowF2: ObjectShadow = field(default_factory=ObjectShadow)
    lod1: ObjectLod = field(default_factory=ObjectLod)
    lod2: ObjectLod = field(default_factory=ObjectLod)

    def read(self, pbdf: PbdfReader) -> None:
        self.named_faces = pbdf.read_u32()
        self.chassisRR0 = pbdf.read_any(Object, self.named_faces)
        self.chassisRL0 = pbdf.read_any(Object, self.named_faces)
        self.chassisSR0 = pbdf.read_any(Object, self.named_faces)
        self.chassisSL0 = pbdf.read_any(Object, self.named_faces)
        self.chassisFR0 = pbdf.read_any(Object, self.named_faces)
        self.chassisFL0 = pbdf.read_any(Object, self.named_faces)
        self.chassisRR1 = pbdf.read_any(Object, self.named_faces)
        self.chassisRL1 = pbdf.read_any(Object, self.named_faces)
        self.chassisSR1 = pbdf.read_any(Object, self.named_faces)
        self.chassisSL1 = pbdf.read_any(Object, self.named_faces)
        self.chassisFR1 = pbdf.read_any(Object, self.named_faces)
        self.chassisFL1 = pbdf.read_any(Object, self.named_faces)
        self.chassisRR2 = pbdf.read_any(Object, self.named_faces)
        self.chassisRL2 = pbdf.read_any(Object, self.named_faces)
        self.chassisSR2 = pbdf.read_any(Object, self.named_faces)
        self.chassisSL2 = pbdf.read_any(Object, self.named_faces)
        self.chassisFR2 = pbdf.read_any(Object, self.named_faces)
        self.chassisFL2 = pbdf.read_any(Object, self.named_faces)
        self.wheelFR = pbdf.read_any(Object, self.named_faces)
        self.wheelRR = pbdf.read_any(Object, self.named_faces)
        self.wheelFL = pbdf.read_any(Object, self.named_faces)
        self.wheelRL = pbdf.read_any(Object, self.named_faces)
        self.shadowR0 = pbdf.read_any(ObjectShadow, self.named_faces)
        self.shadowF0 = pbdf.read_any(ObjectShadow, self.named_faces)
        self.shadowR2 = pbdf.read_any(ObjectShadow, self.named_faces)
        self.shadowF2 = pbdf.read_any(ObjectShadow, self.named_faces)
        self.lod1 = pbdf.read_any(ObjectLod, self.named_faces)
        self.lod2 = pbdf.read_any(ObjectLod, self.named_faces)

    def write(self, pbdf: PbdfWriter) -> None:
        pbdf.write_u32(self.named_faces)
        pbdf.write_any(self.chassisRR0, self.named_faces)
        pbdf.write_any(self.chassisRL0, self.named_faces)
        pbdf.write_any(self.chassisSR0, self.named_faces)
        pbdf.write_any(self.chassisSL0, self.named_faces)
        pbdf.write_any(self.chassisFR0, self.named_faces)
        pbdf.write_any(self.chassisFL0, self.named_faces)
        pbdf.write_any(self.chassisRR1, self.named_faces)
        pbdf.write_any(self.chassisRL1, self.named_faces)
        pbdf.write_any(self.chassisSR1, self.named_faces)
        pbdf.write_any(self.chassisSL1, self.named_faces)
        pbdf.write_any(self.chassisFR1, self.named_faces)
        pbdf.write_any(self.chassisFL1, self.named_faces)
        pbdf.write_any(self.chassisRR2, self.named_faces)
        pbdf.write_any(self.chassisRL2, self.named_faces)
        pbdf.write_any(self.chassisSR2, self.named_faces)
        pbdf.write_any(self.chassisSL2, self.named_faces)
        pbdf.write_any(self.chassisFR2, self.named_faces)
        pbdf.write_any(self.chassisFL2, self.named_faces)
        pbdf.write_any(self.wheelFR, self.named_faces)
        pbdf.write_any(self.wheelRR, self.named_faces)
        pbdf.write_any(self.wheelFL, self.named_faces)
        pbdf.write_any(self.wheelRL, self.named_faces)
        pbdf.write_any(self.shadowR0, self.named_faces)
        pbdf.write_any(self.shadowF0, self.named_faces)
        pbdf.write_any(self.shadowR2, self.named_faces)
        pbdf.write_any(self.shadowF2, self.named_faces)
        pbdf.write_any(self.lod1, self.named_faces)
        pbdf.write_any(self.lod2, self.named_faces)


class Fx(IntEnum):
    ENGINE_START = 0
    SKID = 3
    SKID_END = 4
    HIT_WALL = 7
    ACCEL_START = 8
    ACCEL_END_1 = 9
    ACCEL_END_2 = 10
    ENGINE_STOP = 11
    HIT_GROUND = 12
    FLIPPED_START = 13
    FLIPPED = 14


@dataclass
class Sound:
    reserved: int = 0
    fx: List[int] = field(default_factory=lambda: [-1] * 16)  # s. Fx

    def read(self, pbdf: PbdfReader) -> None:
        self.reserved = pbdf.read_u32()
        self.fx = [pbdf.read_i16() for _ in range(16)]

    def write(self, pbdf: PbdfWriter) -> None:
        pbdf.write_u32(self.reserved)
        [pbdf.write_i16(x) for x in self.fx]


@dataclass
class Settings:
    accelerate: int = 0
    brakes: int = 0
    grip: int = 0
    handling: int = 0
    speed: int = 0

    def read(self, pbdf: PbdfReader) -> None:
        self.accelerate = pbdf.read_i32()
        self.brakes = pbdf.read_i32()
        self.grip = pbdf.read_i32()
        self.handling = pbdf.read_i32()
        self.speed = pbdf.read_i32()

    def write(self, pbdf: PbdfWriter) -> None:
        pbdf.write_i32(self.accelerate)
        pbdf.write_i32(self.brakes)
        pbdf.write_i32(self.grip)
        pbdf.write_i32(self.handling)
        pbdf.write_i32(self.speed)


@dataclass
class Unknown:
    unknown1: int = 0
    unknown2: List[Tuple[float, float, float]] = field(default_factory=lambda: [(0.0, 0.0, 0.0)] * 4)
    unknown3: float = 0
    unknown4: float = 0
    unknown5: float = 0
    unknown6: float = 0

    def read(self, pbdf: PbdfReader) -> None:
        self.unknown1 = pbdf.read_i32()
        self.unknown2 = [pbdf.read_vec3() for _ in range(4)]
        self.unknown3 = pbdf.read_f()
        self.unknown4 = pbdf.read_f()
        self.unknown5 = pbdf.read_f()
        self.unknown6 = pbdf.read_f()

    def write(self, pbdf: PbdfWriter) -> None:
        pbdf.write_i32(self.unknown1)
        [pbdf.write_vec3(x) for x in self.unknown2]
        pbdf.write_f(self.unknown3)
        pbdf.write_f(self.unknown4)
        pbdf.write_f(self.unknown5)
        pbdf.write_f(self.unknown6)


@dataclass
class Car:
    name: str = ""
    physics: Physics = field(default_factory=Physics)
    material: Material = field(default_factory=Material)
    objects: Objects = field(default_factory=Objects)
    sound: Sound = field(default_factory=Sound)
    settings: Settings = field(default_factory=Settings)
    unknown: Unknown = field(default_factory=Unknown)

    def read(self, pbdf: PbdfReader) -> None:
        pbdf.offset(0)
        self.name = pbdf.read_s()
        self.physics = pbdf.read_any(Physics)
        self.material = pbdf.read_any(Material)
        self.objects = pbdf.read_any(Objects)
        self.sound = pbdf.read_any(Sound)
        self.settings = pbdf.read_any(Settings)
        self.unknown = pbdf.read_any(Unknown)

    def write(self, pbdf: PbdfWriter) -> None:
        pbdf.offset()
        pbdf.write_s(self.name)
        pbdf.write_any(self.physics)
        pbdf.write_any(self.material)
        pbdf.write_any(self.objects)
        pbdf.write_any(self.sound)
        pbdf.write_any(self.settings)
        pbdf.write_any(self.unknown)
