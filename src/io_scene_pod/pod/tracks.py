from __future__ import annotations

from dataclasses import dataclass, field
from typing import List, Tuple

from .common import Mesh, TextureFormat, TextureList
from .pbdf import PbdfReader


@dataclass
class Event:
    name: str = ""
    param_size: int = 0
    param_count: int = 0
    param_data: bytes = None

    def read(self, pbdf: PbdfReader) -> None:
        self.name = pbdf.read_s()
        self.param_size = pbdf.read_i32()
        self.param_count = pbdf.read_i32()
        self.param_data = pbdf.read(self.param_size * self.param_count)


@dataclass
class Macro:
    values: List[int] = field(default_factory=list)

    def read(self, pbdf: PbdfReader, value_count: int) -> None:
        self.values = [pbdf.read_i32() for _ in range(value_count)]


@dataclass
class MacroList:
    base: List[Macro] = field(default_factory=list)
    main: List[Macro] = field(default_factory=list)
    init: List[Macro] = field(default_factory=list)
    active: List[Macro] = field(default_factory=list)
    desactive: List[Macro] = field(default_factory=list)
    remplace: List[Macro] = field(default_factory=list)
    echange: List[Macro] = field(default_factory=list)

    def read(self, pbdf: PbdfReader) -> None:
        self.base = [pbdf.read_any(Macro, 1) for _ in range(pbdf.read_i32())]
        self.main = [pbdf.read_any(Macro, 1) for _ in range(pbdf.read_i32())]
        self.init = [pbdf.read_any(Macro, 1) for _ in range(pbdf.read_i32())]
        self.active = [pbdf.read_any(Macro, 1) for _ in range(pbdf.read_i32())]
        self.desactive = [pbdf.read_any(Macro, 1) for _ in range(pbdf.read_i32())]
        self.remplace = [pbdf.read_any(Macro, 2) for _ in range(pbdf.read_i32())]
        self.echange = [pbdf.read_any(Macro, 2) for _ in range(pbdf.read_i32())]


@dataclass
class Sector:
    mesh: Mesh = field(default_factory=Mesh)
    vertex_lights: bytes = None
    bounding_box_min: Tuple[float, float, float] = (0.0, 0.0, 0.0)
    bounding_box_max: Tuple[float, float, float] = (0.0, 0.0, 0.0)

    def read(self, pbdf: PbdfReader, named_faces: int) -> None:
        self.mesh = pbdf.read_any(Mesh, named_faces, True)
        self.vertex_lights = pbdf.read(len(self.mesh.positions))
        self.bounding_box_min = pbdf.read_vec3()  # z -= 2
        self.bounding_box_max = pbdf.read_vec3()  # z += 10


@dataclass
class SectorList:
    named_faces: int = 0
    _sectors: List[Sector] = None

    def read(self, pbdf: PbdfReader) -> SectorList:
        named_faces = pbdf.read_u32()
        self._sectors = [pbdf.read_any(Sector, named_faces) for _ in range(pbdf.read_i32())]

    def __iter__(self):
        return self._sectors.__iter__()


@dataclass
class Track:
    events: List[Event] = field(default_factory=list)
    macros: MacroList = field(default_factory=MacroList)
    track_name: str = ""
    texture_lod: List[int] = field(default_factory=list)
    project_name: str = ""
    textures: TextureList = field(default_factory=TextureList)
    sectors: SectorList = field(default_factory=SectorList)

    def read(self, pbdf: PbdfReader) -> None:
        pbdf.offset(0)
        if pbdf.read_i32() != 3:
            raise IOError("Check value is not 3")
        unused = pbdf.read_i32()
        event_count = pbdf.read_i32()
        event_buffer_size = pbdf.read_i32()
        self.events = [pbdf.read_any(Event) for _ in range(event_count)]
        self.macros = pbdf.read_any(MacroList)
        self.track_name = pbdf.read_s()
        self.texture_lod = [pbdf.read_i32() for _ in range(16)]
        self.project_name = pbdf.read_s()
        self.textures = pbdf.read_any(TextureList, 256, TextureFormat.RGB565)
        self.sectors = pbdf.read_any(SectorList)
