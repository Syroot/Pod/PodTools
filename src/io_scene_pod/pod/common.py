from __future__ import annotations

from dataclasses import dataclass, field
from enum import Enum
from typing import List, Tuple

from .pbdf import PbdfReader, PbdfWriter


@dataclass
class MeshFace:
    name: str = ""
    indices: List[int] = field(default_factory=lambda: [0] * 4)
    vertex_count: int = 0
    normal: Tuple[float, float, float] = field(default_factory=lambda: (0.0, 0.0, 0.0))
    material_type: str = ""
    color: int = 0  # RGBX
    texture_index: int = 0
    texture_uvs: List[Tuple[float, float]] = field(default_factory=lambda: [(0, 0)] * 4)
    reserved: int = 0
    quad_reserved: Tuple[float, float, float] = field(default_factory=lambda: (0.0, 0.0, 0.0))
    sector_prop: int = 0  # byte
    properties: int = 0

    def read(self, pbdf: PbdfReader, flags: int, is_sector: bool) -> None:
        if flags:
            self.name = pbdf.read_s()
        if pbdf.key == 0x00005CA8:
            self.indices = [0] * 4
            self.indices[3] = pbdf.read_i32()
            self.indices[0] = pbdf.read_i32()
            self.vertex_count = pbdf.read_i32()
            self.indices[2] = pbdf.read_i32()
            self.indices[1] = pbdf.read_i32()
        else:
            self.vertex_count = pbdf.read_i32()
            self.indices = [pbdf.read_i32() for _ in range(4)]
        self.normal = pbdf.read_vec3()
        self.material_type = pbdf.read_s()
        if self.material_type in ('FLAT', 'GOURAUD'):
            self.color = pbdf.read_u32()
        else:
            self.texture_index = pbdf.read_i32()
        self.texture_uvs = [pbdf.read_vec2u() for _ in range(4)]
        self.reserved = pbdf.read_i32()
        if self.vertex_count == 4:
            self.quad_reserved = pbdf.read_vec3()
        if is_sector and any(self.normal):
            self.sector_prop = pbdf.read_i32()
        self.properties = pbdf.read_i32()

    def write(self, pbdf: PbdfWriter, has_name: int, is_sector: bool) -> None:
        if has_name:
            pbdf.write_s(self.name)
        if pbdf.key == 0x00005CA8:
            pbdf.write_i32(self.indices[3])
            self.indices[0] = pbdf.read_i32()
            pbdf.write_i32(self.vertex_count)
            pbdf.write_i32(self.indices[2])
            pbdf.write_i32(self.indices[1])
        else:
            pbdf.write_i32(self.vertex_count)
            [pbdf.write_i32(self.indices[i]) for i in range(4)]
        pbdf.write_vec3(self.normal)
        pbdf.write_s(self.material_type)
        if self.material_type in ('FLAT', 'GOURAUD'):
            pbdf.write_u32(self.color)
        else:
            pbdf.write_i32(self.texture_index)
        [pbdf.write_vec2u(self.texture_uvs[i]) for i in range(4)]
        pbdf.write_i32(self.reserved)
        if self.vertex_count == 4:
            pbdf.write_vec3(self.quad_reserved)
        if is_sector and any(self.normal):
            pbdf.write_i32(self.sector_prop)
        pbdf.write_i32(self.properties)


@dataclass
class Mesh:
    positions: List[Tuple[float, float, float]] = field(default_factory=list)
    faces: List[MeshFace] = field(default_factory=list)
    normals: List[Tuple[float, float, float]] = field(default_factory=list)
    unknown: int = 0  # Color?

    def read(self, pbdf: PbdfReader, face_flags: int, is_sector: bool) -> None:
        position_count = pbdf.read_i32()
        self.positions = [pbdf.read_vec3() for _ in range(position_count)]
        face_count = pbdf.read_i32()
        tri_count = pbdf.read_i32()
        quad_count = pbdf.read_i32()
        self.faces = [pbdf.read_any(MeshFace, face_flags, is_sector) for _ in range(face_count)]
        self.normals = [pbdf.read_vec3() for _ in range(position_count)]
        self.unknown = pbdf.read_i32()

    def write(self, pbdf: PbdfWriter, has_named_faces: int, is_sector: bool) -> None:
        pbdf.write_i32(len(self.positions))
        [pbdf.write_vec3(x) for x in self.positions]
        pbdf.write_i32(len(self.faces))
        pbdf.write_i32(sum(1 for f in self.faces if f.vertex_count == 3))
        pbdf.write_i32(sum(1 for f in self.faces if f.vertex_count == 4))
        [pbdf.write_any(x, has_named_faces, is_sector) for x in self.faces]
        [pbdf.write_vec3(x) for x in self.normals]
        pbdf.write_i32(self.unknown)


@dataclass
class TextureRegion:
    name: str = ""
    left: int = 0
    top: int = 0
    right: int = 0
    bottom: int = 0
    index: int = 0

    def read(self, pbdf: PbdfReader) -> None:
        self.name = pbdf.read(32).decode(pbdf.encoding).rstrip('\x00')
        self.left = pbdf.read_i32()
        self.top = pbdf.read_i32()
        self.right = pbdf.read_i32()
        self.bottom = pbdf.read_i32()
        self.index = pbdf.read_i32()

    def write(self, pbdf: PbdfWriter) -> None:
        name = bytearray(32)
        name[:len(self.name)] = self.name.encode(pbdf.encoding)
        pbdf.write(name)
        pbdf.write_i32(self.left)
        pbdf.write_i32(self.top)
        pbdf.write_i32(self.right)
        pbdf.write_i32(self.bottom)
        pbdf.write_i32(self.index)


@dataclass
class Texture:
    regions: List[TextureRegion] = field(default_factory=list)
    data: bytearray = None

    def read(self, pbdf: PbdfReader) -> None:
        self.regions = [pbdf.read_any(TextureRegion) for _ in range(pbdf.read_i32())]

    def write(self, pbdf: PbdfWriter) -> None:
        pbdf.write_i32(len(self.regions))
        [pbdf.write_any(x) for x in self.regions]


class TextureFormat(Enum):
    RGB565 = 2

    def get_pixel_size(self) -> int:
        if self == TextureFormat.RGB565: return 2
        return 0


@dataclass
class TextureList:
    textures: List[Texture] = field(default_factory=list)
    flags: int = 0

    def read(self, pbdf: PbdfReader, size: int, format: TextureFormat) -> None:
        count = pbdf.read_i32()
        self.flags = pbdf.read_i32()
        self.textures = [pbdf.read_any(Texture) for _ in range(count)]
        for texture in self.textures:
            texture.data = pbdf.read(size * size * format.get_pixel_size())

    def write(self, pbdf: PbdfWriter) -> None:
        pbdf.write_i32(len(self.textures))
        pbdf.write_i32(self.flags)
        [pbdf.write_any(x) for x in self.textures]
        for texture in self.textures:
            pbdf.write(texture.data)

    def __iter__(self):
        return self.textures.__iter__()
