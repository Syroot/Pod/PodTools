import bpy.props
import bpy.types


def register() -> None:
    bpy.types.Scene.pod_car = bpy.props.PointerProperty(type=POD_Car)
    bpy.types.Scene.pod_car_wheel_fr = bpy.props.PointerProperty(type=POD_CarPart)
    bpy.types.Scene.pod_car_wheel_rr = bpy.props.PointerProperty(type=POD_CarPart)
    bpy.types.Scene.pod_car_wheel_fl = bpy.props.PointerProperty(type=POD_CarPart)
    bpy.types.Scene.pod_car_wheel_rl = bpy.props.PointerProperty(type=POD_CarPart)
    bpy.types.Scene.pod_car_chassis = bpy.props.PointerProperty(type=POD_CarPart)
    bpy.types.Scene.pod_car_physics = bpy.props.PointerProperty(type=POD_CarPhysics)
    bpy.types.Scene.pod_car_settings = bpy.props.PointerProperty(type=POD_CarSettings)
    bpy.types.Scene.pod_car_sound = bpy.props.PointerProperty(type=POD_CarSound)


def unregister() -> None:
    del bpy.types.Scene.pod_car
    del bpy.types.Scene.pod_car_wheel_fr
    del bpy.types.Scene.pod_car_wheel_rr
    del bpy.types.Scene.pod_car_wheel_fl
    del bpy.types.Scene.pod_car_wheel_rl
    del bpy.types.Scene.pod_car_chassis
    del bpy.types.Scene.pod_car_physics
    del bpy.types.Scene.pod_car_settings
    del bpy.types.Scene.pod_car_sound


class POD_Car(bpy.types.PropertyGroup):
    name: bpy.props.StringProperty(name="Name",
        description="Name of the car as displayed in-game. Defaults to file name",
        default="")
    base_file: bpy.props.StringProperty(name="Base File",
        description="Car file providing currently unsupported values to be copied over to exported car",
        subtype='FILE_PATH')


class POD_CarPart(bpy.types.PropertyGroup):
    spring_1: bpy.props.FloatProperty(name="Spring 1",
        description="Bumpyness when touching ground",
        subtype='FACTOR',
        soft_min=0, soft_max=1)
    spring_2: bpy.props.FloatProperty(name="Spring 2",
        description="Bumpyness when touching ground",
        subtype='FACTOR',
        soft_min=0, soft_max=1)
    travel_max: bpy.props.FloatProperty(name="Travel Max",
        description="Positive distance a wheel can travel from its original position",
        subtype='FACTOR',
        soft_min=0, soft_max=1)
    travel_min: bpy.props.FloatProperty(name="Travel Min",
        description="Negative distance a wheel can travel from its original position",
        subtype='FACTOR',
        soft_min=-1, soft_max=0)


class POD_CarPhysics(bpy.types.PropertyGroup):
    front_accel: bpy.props.FloatProperty(name="Front Accel",
        description="Acceleration of front wheels",
        subtype='FACTOR',
        soft_min=0, soft_max=5000)
    rear_accel: bpy.props.FloatProperty(name="Rear Accel",
        description="Acceleration of rear wheels",
        subtype='FACTOR',
        soft_min=0, soft_max=5000)
    spring_damper_1: bpy.props.FloatProperty(name="Spring Damper 1",
        description="Stability against bumpyness",
        subtype='FACTOR',
        soft_min=0, soft_max=5000)
    spring_damper_2: bpy.props.FloatProperty(name="Spring Damper 2",
        description="Stability against bumpyness",
        subtype='FACTOR',
        soft_min=0, soft_max=5000)
    weight: bpy.props.FloatProperty(name="Weight",
        description="Heavyness of car body",
        subtype='FACTOR',
        soft_min=0, soft_max=10)
    speed_factor: bpy.props.FloatProperty(name="Speed Factor",
        description="Maximum speed achievable",
        subtype='FACTOR',
        soft_min=0, soft_max=5)
    accel_factor: bpy.props.FloatProperty(name="Accel Factor",
        description="Acceleration power",
        subtype='FACTOR',
        soft_min=0, soft_max=0.0001)


class POD_CarSettings(bpy.types.PropertyGroup):
    accelerate: bpy.props.IntProperty(name="Accelerate", subtype='FACTOR', soft_min=0, soft_max=100)
    brakes: bpy.props.IntProperty(name="Brakes", subtype='FACTOR', soft_min=0, soft_max=100)
    grip: bpy.props.IntProperty(name="Grip", subtype='FACTOR', soft_min=0, soft_max=100)
    handling: bpy.props.IntProperty(name="Handling", subtype='FACTOR', soft_min=0, soft_max=100)
    speed: bpy.props.IntProperty(name="Speed", subtype='FACTOR', soft_min=0, soft_max=100)

    def get_total(self) -> int:
        return self.accelerate + self.brakes + self.grip + self.handling + self.speed


class POD_CarSound(bpy.types.PropertyGroup):
    engine_start: bpy.props.IntProperty(name="Engine Start", min=-1, max=50)
    unknown_1: bpy.props.IntProperty(name="Unknown 1", min=-1, max=50)
    unknown_2: bpy.props.IntProperty(name="Unknown 2", min=-1, max=50)
    skid: bpy.props.IntProperty(name="Skid", min=-1, max=50)
    skid_end: bpy.props.IntProperty(name="Skid End", min=-1, max=50)
    unknown_5: bpy.props.IntProperty(name="Unknown 5", min=-1, max=50)
    unknown_6: bpy.props.IntProperty(name="Unknown 6", min=-1, max=50)
    hit_wall: bpy.props.IntProperty(name="Hit Wall", min=-1, max=50)
    accel_start: bpy.props.IntProperty(name="Accel Start", min=-1, max=50)
    accel_end_1: bpy.props.IntProperty(name="Accel End 1", min=-1, max=50)
    accel_end_2: bpy.props.IntProperty(name="Accel End 2", min=-1, max=50)
    engine_stop: bpy.props.IntProperty(name="Engine Stop", min=-1, max=50)
    hit_ground: bpy.props.IntProperty(name="Hit Ground", min=-1, max=50)
    flipped_start: bpy.props.IntProperty(name="Flipped Start", min=-1, max=50)
    flipped: bpy.props.IntProperty(name="Flipped", min=-1, max=50)
    unknown_15: bpy.props.IntProperty(name="Unknown 15", min=-1, max=50)


class POD_PT_Car(bpy.types.Panel):
    bl_label = "POD Car"
    bl_region_type = 'WINDOW'
    bl_space_type = "PROPERTIES"
    bl_context = "scene"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        scene = context.scene
        row = layout.row()
        row.prop(scene.pod_car, "name")
        row = layout.row()
        row.prop(scene.pod_car, "base_file")


class POD_PT_CarPart(bpy.types.Panel):
    bl_label = "POD Car Parts"
    bl_region_type = 'WINDOW'
    bl_space_type = "PROPERTIES"
    bl_context = "scene"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        scene = context.scene
        def draw_part(title: str, group: POD_CarPart, chassis: bool = False) -> None:
            row = layout.row()
            row.label(text=title)
            if not chassis:
                row = layout.row()
                row.prop(group, "spring_1")
                row.prop(group, "spring_2")
                row = layout.row()
                row.prop(group, "travel_min")
                row.prop(group, "travel_max")
        draw_part("Wheel Front Right", scene.pod_car_wheel_fr)
        draw_part("Wheel Rear Right", scene.pod_car_wheel_rr)
        draw_part("Wheel Front Left", scene.pod_car_wheel_fl)
        draw_part("Wheel Rear Left", scene.pod_car_wheel_rl)
        draw_part("Chassis", scene.pod_car_chassis, True)


class POD_PT_CarPhysics(bpy.types.Panel):
    bl_label = "POD Car Physics"
    bl_region_type = 'WINDOW'
    bl_space_type = "PROPERTIES"
    bl_context = "scene"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        scene = context.scene
        row = layout.row()
        row.prop(scene.pod_car_physics, "front_accel")
        row.prop(scene.pod_car_physics, "rear_accel")
        row = layout.row()
        row.prop(scene.pod_car_physics, "spring_damper_1")
        row.prop(scene.pod_car_physics, "spring_damper_2")
        row = layout.row()
        row.prop(scene.pod_car_physics, "weight")
        row.label()
        row = layout.row()
        row.prop(scene.pod_car_physics, "speed_factor")
        row.prop(scene.pod_car_physics, "accel_factor")


class POD_PT_CarSettings(bpy.types.Panel):
    bl_label = "POD Car Settings"
    bl_region_type = 'WINDOW'
    bl_space_type = "PROPERTIES"
    bl_context = "scene"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        settings = context.scene.pod_car_settings
        # Display each setting.
        row = layout.row()
        row.prop(settings, "accelerate")
        row = layout.row()
        row.prop(settings, "brakes")
        row = layout.row()
        row.prop(settings, "grip")
        row = layout.row()
        row.prop(settings, "handling")
        row = layout.row()
        row.prop(settings, "speed")
        # Display total.
        row = layout.row()
        total = settings.get_total()
        if total > 300:
            row.label(text=f"Total is {total}.", icon='CANCEL')
        elif total < 300:
            row.label(text=f"Total is {total}.", icon='ERROR')
        else:
            row.label(text=f"Total is {total}.", icon='CHECKMARK')


class POD_PT_CarSound(bpy.types.Panel):
    bl_label = "POD Car Sound"
    bl_region_type = 'WINDOW'
    bl_space_type = "PROPERTIES"
    bl_context = "scene"

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return True

    def draw(self, context: bpy.types.Context) -> None:
        layout = self.layout
        sound = context.scene.pod_car_sound
        # Display each setting.
        row = layout.row()
        row.prop(sound, "engine_start")
        row.prop(sound, "engine_stop")
        row = layout.row()
        row.prop(sound, "unknown_1")
        row.prop(sound, "unknown_2")
        row = layout.row()
        row.prop(sound, "skid")
        row.prop(sound, "skid_end")
        row = layout.row()
        row.prop(sound, "unknown_5")
        row.prop(sound, "unknown_6")
        row = layout.row()
        row.prop(sound, "hit_wall")
        row.prop(sound, "hit_ground")
        row = layout.row()
        row.prop(sound, "accel_start")
        row.label()
        row = layout.row()
        row.prop(sound, "accel_end_1")
        row.prop(sound, "accel_end_2")
        row = layout.row()
        row.prop(sound, "flipped_start")
        row.prop(sound, "flipped")
        row = layout.row()
        row.prop(sound, "unknown_15")
        row.label()


classes = (
    POD_Car,
    POD_CarPart,
    POD_CarPhysics,
    POD_CarSettings,
    POD_CarSound,
    POD_PT_Car,
    POD_PT_CarPart,
    POD_PT_CarPhysics,
    POD_PT_CarSettings,
    POD_PT_CarSound,
)
