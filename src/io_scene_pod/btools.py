from typing import Tuple

import bpy
import bpy.types


def colbox(self: bpy.types.UILayout, prop_data, prop_name: str) -> Tuple[bpy.types.UILayout, bpy.types.UILayout]:
    box = self.box()
    header = box.split(0.5)
    row = header.row(align=True)
    row.prop(prop_data, prop_name,
        icon="TRIA_DOWN" if getattr(prop_data, prop_name) else "TRIA_RIGHT",
        icon_only=True,
        emboss=False)
    row.label(getattr(prop_data.rna_type, prop_name)[1]["name"])
    return header, box


def create_empty_material(name: str, render_engine: str = None) -> bpy.types.Material:
    """ Creates an empty material for the given render engine.
    Args:
        name: The name of the resulting material.
        render_engine: The render engine to set the material up for, or None to use the currently active engine.
    Returns:
        The created material.
    """
    render_engine = render_engine or bpy.context.scene.render.engine

    material = bpy.data.materials.new(name)

    if render_engine in ('BLENDER_EEVEE', 'CYCLES'):
        material.use_nodes = True
        material.node_tree.nodes.clear()

    else:
        raise ValueError("Cannot create material: Unsupported render engine.")

    return material


def create_image_material(name: str, image: bpy.types.Image, render_engine: str = None) -> bpy.types.Material:
    """ Creates a textured material for the given render engine.
    Args:
        name: The name of the resulting material.
        image: The image to use as the texture.
        render_engine: The render engine to set the material up for, or None to use the currently active engine.
    Returns:
        The created material.
    """
    render_engine = render_engine or bpy.context.scene.render.engine

    material = create_empty_material(name, render_engine)

    if render_engine in ('BLENDER_EEVEE', 'CYCLES'):
        nodes = material.node_tree.nodes
        links = material.node_tree.links

        texture_node = nodes.new("ShaderNodeTexImage")
        texture_node.image = image
        diffuse_node = nodes.new("ShaderNodeBsdfPrincipled")
        diffuse_node.inputs['Specular'].default_value = 0
        output_node = nodes.new("ShaderNodeOutputMaterial")

        links.new(texture_node.outputs['Color'], diffuse_node.inputs['Base Color'])
        links.new(diffuse_node.outputs['BSDF'], output_node.inputs['Surface'])

    else:
        raise ValueError("Cannot create material: Unsupported render engine.")

    return material


def create_color_material(name: str, color: Tuple[float, float, float, float], render_engine: str = None) -> bpy.types.Material:
    """ Creates a color material for the given render engine.
    Args:
        name: The name of the resulting material.
        color: The color to use.
        render_engine: The render engine to set the material up for, or None to use the currently active engine.
    Returns:
        The created material.
    """
    render_engine = render_engine or bpy.context.scene.render.engine

    material = create_empty_material(name, render_engine)

    if render_engine in ('BLENDER_EEVEE', 'CYCLES'):
        nodes = material.node_tree.nodes
        links = material.node_tree.links

        diffuse_node = nodes.new("ShaderNodeBsdfPrincipled")
        diffuse_node.inputs['Base Color'].default_value = color
        diffuse_node.inputs['Specular'].default_value = 0
        output_node = nodes.new("ShaderNodeOutputMaterial")

        links.new(diffuse_node.outputs['BSDF'], output_node.inputs['Surface'])

    else:
        raise ValueError("Cannot create material: Unsupported render engine.")

    return material


def get_material_color(material: bpy.types.Material, render_engine: str = None) -> Tuple[float, float, float, float]:
    """ Returns the color of a color material it was created with.
    Args:
        material: The material to extract creation parameters from.
        render_engine: The render engine to parse the material for, or None to use the currently active engine.
    Returns:
        The parameters to recreate the material with or None.
    """
    render_engine = render_engine or bpy.context.scene.render.engine

    if render_engine in ('BLENDER_EEVEE', 'CYCLES'):
        nodes = material.node_tree.nodes

        for node in nodes:
            if isinstance(node, bpy.types.ShaderNodeBsdfPrincipled):
                return node.inputs['Base Color'].default_value
        return None

    else:
        raise ValueError("Cannot get material value: Unsupported render engine.")


def get_material_image(material: bpy.types.Material, render_engine: str = None) -> bpy.types.Image:
    """ Returns the image of an image material it was created with.
    Args:
        material: The material to extract creation parameters from.
        render_engine: The render engine to parse the material for, or None to use the currently active engine.
    Returns:
        The parameters to recreate the material with or None.
    """
    render_engine = render_engine or bpy.context.scene.render.engine

    if render_engine in ('BLENDER_EEVEE', 'CYCLES'):
        nodes = material.node_tree.nodes

        for node in nodes:
            if isinstance(node, bpy.types.ShaderNodeTexImage):
                return node.image
        return None

    else:
        raise ValueError("Cannot get material value: Unsupported render engine.")
