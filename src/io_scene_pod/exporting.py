from __future__ import annotations

from typing import Dict, List, Tuple

import bmesh as BM
import bpy.types
from bpy_extras.io_utils import ExportHelper

from . import btools
from .common import FACE_LAYER_NAME, FACE_LAYER_PROPS
from .pod.common import Mesh, MeshFace, Texture, TextureFormat, TextureList, TextureRegion


class PodExportHelper(ExportHelper):
    mats: Dict[bpy.types.Material, Tuple[str, int]]  # maps materials to their type and color / texture index
    texture_mat_images: List[bpy.types.Image]  # texture images to store in exported file

    def __init__(self) -> None:
        self.mats = {}
        self.texture_mat_images = []

    def create_mesh(self, context: bpy.types.Context, bobj: bpy.types.Object, mesh: Mesh) -> None:
        # Get bmesh from object.
        bmesh = bobj.data
        context.view_layer.objects.active = bobj
        bpy.ops.object.mode_set(mode='EDIT')
        bm = BM.from_edit_mesh(bmesh)
        # Create vertices.
        mesh.positions.clear()
        mesh.normals.clear()
        for bvert in bm.verts:
            mesh.positions.append(bvert.co.to_tuple())
            mesh.normals.append(bvert.normal.to_tuple())
        # Create faces.
        face_names = bm.faces.layers.string[FACE_LAYER_NAME]
        face_props = bm.faces.layers.int[FACE_LAYER_PROPS]
        loop_uvs = bm.loops.layers.uv[0]
        mesh.faces.clear()
        for bface in bm.faces:
            face = MeshFace()
            # Set name and properties.
            face.name = bface[face_names].decode()
            face.properties = bface[face_props]
            face.normal = bface.normal.to_tuple()
            # Set index and texture UV.
            #bface = bm.faces.new((bm.verts[face.indices[i]] for i in range(face.vertex_count)))
            #bface.loops[i][loop_uvs].uv = (face.texture_uvs[i][0] / 0xFF, face.texture_uvs[i][1] / 0xFF)
            for i, bloop in enumerate(bface.loops):
                face.indices[i] = bloop.vert.index
                uv = bloop[loop_uvs].uv.to_tuple()
                face.texture_uvs[i] = (int(round(uv[0] * 0xFF)), int(round(uv[1] * 0xFF)))
            face.vertex_count = i + 1
            # Set material value, collecting materials on the way.
            material = self._cache_material(bmesh.materials[bface.material_index], face)
            face.material_type = material[0]
            if face.material_type in ('FLAT', 'GOURAUD'):
                face.color = material[1]
            else:
                face.texture_index = material[1]
            mesh.faces.append(face)
        bpy.ops.object.mode_set(mode='OBJECT')

    def create_texture(self, image: bpy.types.Image, format: TextureFormat) -> Texture:
        texture = Texture()
        # Create dummy region.
        texture.regions.append(TextureRegion(image.name[:31], 0, 0, 0xFF, 0xFF))
        # Create texture data.
        if format == TextureFormat.RGB565:
            texture.data = bytearray(image.size[0] * image.size[1] * 2)
            data = memoryview(texture.data).cast("H")
            for i in range(image.size[0] * image.size[1]):
                idx = i * 4
                data[i] \
                    = int(image.pixels[idx + 0] * 0b11111) << 11 \
                    | int(image.pixels[idx + 1] * 0b111111) << 5 \
                    | int(image.pixels[idx + 2] * 0b11111)
        else:
            raise NotImplementedError("Unknown texture format.")
        return texture

    def _cache_material(self, bmat: bpy.types.Material, face: MeshFace) -> None:
        result = self.mats.get(bmat, None)
        if result:
            return result
        # Check for texture material.
        if not result:
            image = btools.get_material_image(bmat)
            if image:
                result = ('TEXGOU', len(self.texture_mat_images))
                self.texture_mat_images.append(image)
        # Check for color material.
        if not result:
            color = btools.get_material_color(bmat)
            if color:
                result = ('GOURAUD', int(color[0] * 0xFF) << 16 | int(color[1] * 0xFF) << 8 | int(color[2] * 0xFF))
        if not result:
            raise AssertionError(f"Could not determine type of material '{bmat.name}'.")
        self.mats[bmat] = result
        return result
