from __future__ import annotations

import bpy
import bpy.props
import bpy.types

from .importing import PodImportHelper
from .pod.common import TextureFormat
from .pod.pbdf import PbdfReader
from .pod.tracks import Track


def menu_func_track(self, context):
    self.layout.operator(POD_OT_TrackImport.bl_idname, text="POD Track (.bl4)")


def register():
    bpy.types.TOPBAR_MT_file_import.append(menu_func_track)


def unregister():
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_track)


class POD_OT_TrackImport(bpy.types.Operator, PodImportHelper):
    """Load a POD BL4 track file"""
    bl_idname = "import_scene.bl4"
    bl_label = "Import POD Track"
    bl_options = {'UNDO'}
    filename_ext = ".bl4"
    filter_glob: bpy.props.StringProperty(
        default="*.bl4",
        options={'HIDDEN'})
    encoding: bpy.props.StringProperty(
        name="Encoding",
        description="Code page to read strings in. cp437 may be required for fan made tracks",
        default="cp1252")

    def execute(self, context):
        file_path = self.properties.filepath
        with PbdfReader(open(file_path, "rb"), encoding=self.encoding) as pbdf:
            track = pbdf.read_any(Track)
        # Convert textures to materials.
        for texture in track.textures:
            self.create_texture(texture, 256, TextureFormat.RGB565)
        # Convert sectors to meshes.
        bcol_sectors = self.create_collection("POD Track Sectors")
        for i, sector in enumerate(track.sectors):
            bmesh = self.create_mesh_object(sector.mesh, f"Sector{i:03d}", bcol_sectors)
        return {'FINISHED'}


classes = (
    POD_OT_TrackImport,
)
