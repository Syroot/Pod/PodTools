from __future__ import annotations
from typing import List

import bpy
import bpy.props
import bpy.types

from .pod.cars import Car, Fx, Part, Spec
from .pod.common import TextureFormat
from .pod.pbdf import PbdfReader
from .importing import PodImportHelper
from .editing_car import POD_CarPart


def menu_func_import(self, context: bpy.types.Context) -> None:
    self.layout.operator(POD_OT_CarImport.bl_idname, text="POD Car (.bv4)")


def register() -> None:
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)


def unregister() -> None:
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)


class POD_OT_CarImport(bpy.types.Operator, PodImportHelper):
    """Load a POD BV4 car file"""
    bl_idname = "import_scene.bv4"
    bl_label = "Import POD Car"
    bl_options = {'UNDO'}
    filename_ext = ".bv4"
    filter_glob: bpy.props.StringProperty(
        default="*.bv4",
        options={'HIDDEN'})

    def execute(self, context: bpy.types.Context):
        file_path = self.properties.filepath
        with PbdfReader(open(file_path, "rb")) as pbdf:
            car = pbdf.read_any(Car)
        scene = bpy.context.scene
        scene.pod_car.name = car.name
        scene.pod_car.base_file = file_path
        # Convert parts.
        def convert_part(props: POD_CarPart, part: List[float]) -> None:
            props.spring_1 = part[Part.SPRING_1]
            props.spring_2 = part[Part.SPRING_2]
            props.travel_min = part[Part.TRAVEL_MIN]
            props.travel_max = part[Part.TRAVEL_MAX]
        convert_part(scene.pod_car_wheel_fr, car.physics.wheel_fr)
        convert_part(scene.pod_car_wheel_rr, car.physics.wheel_rr)
        convert_part(scene.pod_car_wheel_fl, car.physics.wheel_fl)
        convert_part(scene.pod_car_wheel_rl, car.physics.wheel_rl)
        convert_part(scene.pod_car_chassis, car.physics.chassis)
        # Convert physics.
        physics = scene.pod_car_physics
        physics.front_accel = car.physics.spec[Spec.FRONT_ACCEL]
        physics.rear_accel = car.physics.spec[Spec.REAR_ACCEL]
        physics.spring_damper_1 = car.physics.spec[Spec.SPRING_DAMPER_1]
        physics.spring_damper_2 = car.physics.spec[Spec.SPRING_DAMPER_2]
        physics.weight = car.physics.spec[Spec.WEIGHT]
        physics.speed_factor = car.physics.spec[Spec.SPEED_FACTOR]
        physics.accel_factor = car.physics.spec[Spec.ACCEL_FACTOR]
        # Convert settings.
        settings = scene.pod_car_settings
        settings.accelerate = car.settings.accelerate
        settings.brakes = car.settings.brakes
        settings.grip = car.settings.grip
        settings.handling = car.settings.handling
        settings.speed = car.settings.speed
        # Convert sound.
        sound = scene.pod_car_sound
        sound.engine_start = car.sound.fx[Fx.ENGINE_START]
        sound.unknown_1 = car.sound.fx[1]
        sound.unknown_2 = car.sound.fx[2]
        sound.skid = car.sound.fx[Fx.SKID]
        sound.skid_end = car.sound.fx[Fx.SKID_END]
        sound.unknown_5 = car.sound.fx[5]
        sound.unknown_6 = car.sound.fx[6]
        sound.hit_wall = car.sound.fx[Fx.HIT_WALL]
        sound.accel_start = car.sound.fx[Fx.ACCEL_START]
        sound.accel_end_1 = car.sound.fx[Fx.ACCEL_END_1]
        sound.accel_end_2 = car.sound.fx[Fx.ACCEL_END_2]
        sound.engine_stop = car.sound.fx[Fx.ENGINE_STOP]
        sound.hit_ground = car.sound.fx[Fx.HIT_GROUND]
        sound.flipped_start = car.sound.fx[Fx.FLIPPED_START]
        sound.flipped = car.sound.fx[Fx.FLIPPED]
        sound.unknown_15 = car.sound.fx[15]
        # Convert textures to be referenced by meshes.
        for texture in car.material.texture_list.textures:
            self.create_texture(texture, 128, TextureFormat.RGB565)
        # Convert chassis mesh.
        chassis_pos = (
            car.physics.chassis[Part.POSITION_X],
            car.physics.chassis[Part.POSITION_Y],
            car.physics.chassis[Part.POSITION_Z])
        bcol_chassis0 = self.create_collection("POD Car Chassis 0")
        self.create_mesh_object(car.objects.chassisRR0.mesh, "ChassisRR0", bcol_chassis0, chassis_pos)
        self.create_mesh_object(car.objects.chassisRL0.mesh, "ChassisRL0", bcol_chassis0, chassis_pos)
        self.create_mesh_object(car.objects.chassisSR0.mesh, "ChassisSR0", bcol_chassis0, chassis_pos)
        self.create_mesh_object(car.objects.chassisSL0.mesh, "ChassisSL0", bcol_chassis0, chassis_pos)
        self.create_mesh_object(car.objects.chassisFR0.mesh, "ChassisFR0", bcol_chassis0, chassis_pos)
        self.create_mesh_object(car.objects.chassisFL0.mesh, "ChassisFL0", bcol_chassis0, chassis_pos)
        bcol_chassis1 = self.create_collection("POD Car Chassis 1")
        self.create_mesh_object(car.objects.chassisRR1.mesh, "ChassisRR1", bcol_chassis1, chassis_pos)
        self.create_mesh_object(car.objects.chassisRL1.mesh, "ChassisRL1", bcol_chassis1, chassis_pos)
        self.create_mesh_object(car.objects.chassisSR1.mesh, "ChassisSR1", bcol_chassis1, chassis_pos)
        self.create_mesh_object(car.objects.chassisSL1.mesh, "ChassisSL1", bcol_chassis1, chassis_pos)
        self.create_mesh_object(car.objects.chassisFR1.mesh, "ChassisFR1", bcol_chassis1, chassis_pos)
        self.create_mesh_object(car.objects.chassisFL1.mesh, "ChassisFL1", bcol_chassis1, chassis_pos)
        bcol_chassis2 = self.create_collection("POD Car Chassis 2")
        self.create_mesh_object(car.objects.chassisRR2.mesh, "ChassisRR2", bcol_chassis2, chassis_pos)
        self.create_mesh_object(car.objects.chassisRL2.mesh, "ChassisRL2", bcol_chassis2, chassis_pos)
        self.create_mesh_object(car.objects.chassisSR2.mesh, "ChassisSR2", bcol_chassis2, chassis_pos)
        self.create_mesh_object(car.objects.chassisSL2.mesh, "ChassisSL2", bcol_chassis2, chassis_pos)
        self.create_mesh_object(car.objects.chassisFR2.mesh, "ChassisFR2", bcol_chassis2, chassis_pos)
        self.create_mesh_object(car.objects.chassisFL2.mesh, "ChassisFL2", bcol_chassis2, chassis_pos)
        # Convert wheels to meshes.
        bcol_wheels = self.create_collection("POD Car Wheels")
        self.create_mesh_object(car.objects.wheelFR.mesh, "WheelFR", bcol_wheels, (
            car.physics.wheel_fr[Part.POSITION_X],
            car.physics.wheel_fr[Part.POSITION_Y],
            car.physics.wheel_fr[Part.POSITION_Z]))
        self.create_mesh_object(car.objects.wheelRR.mesh, "WheelRR", bcol_wheels, (
            car.physics.wheel_rr[Part.POSITION_X],
            car.physics.wheel_rr[Part.POSITION_Y],
            car.physics.wheel_rr[Part.POSITION_Z]))
        self.create_mesh_object(car.objects.wheelFL.mesh, "WheelFL", bcol_wheels, (
            car.physics.wheel_fl[Part.POSITION_X],
            car.physics.wheel_fl[Part.POSITION_Y],
            car.physics.wheel_fl[Part.POSITION_Z]))
        self.create_mesh_object(car.objects.wheelRL.mesh, "WheelRL", bcol_wheels, (
            car.physics.wheel_rl[Part.POSITION_X],
            car.physics.wheel_rl[Part.POSITION_Y],
            car.physics.wheel_rl[Part.POSITION_Z]))
        # Convert shadows to meshes.
        bcol_shadow0 = self.create_collection("POD Car Shadow 0")
        self.create_mesh_object(car.objects.shadowR0.mesh, "ShadowR0", bcol_shadow0)
        self.create_mesh_object(car.objects.shadowF0.mesh, "ShadowF0", bcol_shadow0)
        bcol_shadow2 = self.create_collection("POD Car Shadow 2")
        self.create_mesh_object(car.objects.shadowR0.mesh, "ShadowR2", bcol_shadow2)
        self.create_mesh_object(car.objects.shadowF0.mesh, "ShadowF2", bcol_shadow2)
        # Convert LoD models to meshes.
        bcol_lod = self.create_collection("POD Car LoD")
        self.create_mesh_object(car.objects.lod1.mesh, "Lod1", bcol_lod, chassis_pos)
        self.create_mesh_object(car.objects.lod2.mesh, "Lod2", bcol_lod, chassis_pos)
        return {'FINISHED'}


classes = (
    POD_OT_CarImport,
)
