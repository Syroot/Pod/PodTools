﻿using System;

namespace Syroot.Pod.CarScripts
{
    /// <summary>
    /// Represents a car information block stored in a <see cref="CarScript"/>.
    /// </summary>
    public record CarInfo
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the case-insensitive name how the car appears in the menu. Must not be longer than 63
        /// characters.
        /// </summary>
        public string DisplayName { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the base name of the file associated with the car. Must not be longer than 19 characters.
        /// </summary>
        public string Name { get; set; } = String.Empty;

        public string ImageFile1 { get; set; } = String.Empty;

        public string ImageFile2 { get; set; } = String.Empty;

        public string ImageFile3 { get; set; } = String.Empty;
    }
}
