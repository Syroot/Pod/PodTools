﻿using System;
using System.Collections.Generic;
using System.IO;
using Syroot.Pod.Core;

namespace Syroot.Pod.CarScripts
{
    /// <summary>
    /// Represents VOITUTRES.BIN, VOITURE2.BIN and VOIRESET.BIN files storing information on (installed) cars.
    /// </summary>
    public class CarScript
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _infoSize = 128;
        private const int _infoKey = 0x68;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the vehicles available in the game. Should not exceed 8 for VOITURES.BIN and VOIRESET.BIN, and 32 for
        /// VOIRTURE2.BIN. Should have at least 2 or the game crashes with AI opponents.
        /// </summary>
        public IList<CarInfo> Cars { get; } = new List<CarInfo>();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the instance data from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the instance data from.</param>
        public void Load(Stream stream)
        {
            // Reset instance.
            Cars.Clear();

            // Read raw number of cars.
            int count = stream.ReadInt32LittleEndian();

            // Read each enciphered car info.
            Span<byte> buffer = stackalloc byte[_infoSize];
            while (count-- > 0)
            {
                stream.Read(buffer);
                for (int i = 0; i < _infoSize; i++)
                    buffer[i] ^= _infoKey;

                Cars.Add(new()
                {
                    DisplayName = Encodings.Codepage1252.GetString0(buffer[0..64]),
                    Name = Encodings.Codepage1252.GetString0(buffer[64..84]),
                    ImageFile1 = Encodings.Codepage1252.GetString0(buffer[84..97]),
                    ImageFile2 = Encodings.Codepage1252.GetString0(buffer[97..110]),
                    ImageFile3 = Encodings.Codepage1252.GetString0(buffer[110..124])
                });
            }
        }

        /// <summary>
        /// Saves the instance data in the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to save the instance data in.</param>
        public void Save(Stream stream)
        {
            // Write raw number of tracks.
            stream.WriteInt32LittleEndian(Cars.Count);

            // Write each enciphered track info.
            Span<byte> buffer = stackalloc byte[_infoSize];
            foreach (CarInfo car in Cars)
            {
                Encodings.Codepage1252.GetBytes0(car.DisplayName, buffer[0..64]);
                Encodings.Codepage1252.GetBytes0(car.Name, buffer[64..84]);
                Encodings.Codepage1252.GetBytes0(car.ImageFile1, buffer[84..97]);
                Encodings.Codepage1252.GetBytes0(car.ImageFile2, buffer[97..110]);
                Encodings.Codepage1252.GetBytes0(car.ImageFile3, buffer[110..124]);

                for (int i = 0; i < _infoSize; i++)
                    buffer[i] ^= _infoKey;
                stream.Write(buffer);
            }
        }
    }
}
