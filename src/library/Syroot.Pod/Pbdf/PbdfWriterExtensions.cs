﻿using System;
using System.Drawing;
using System.Numerics;

namespace Syroot.Pod.Pbdf
{
    /// <summary>
    /// Represents extension methods for <see cref="PbdfWriter"/> instances.
    /// </summary>
    internal static class PbdfWriterExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Writes a <see cref="Boolean"/> instance as a 4-byte value to the PBDF.
        /// </summary>
        /// <param name="pbdf">A <see cref="PbdfWriter"/> to write with.</param>
        /// <param name="value">A <see cref="Boolean"/> instance to write.</param>
        internal static void WriteBoolean4(this PbdfWriter pbdf, bool value)
        {
            pbdf.WriteInt32(value ? 1 : 0);
        }

        /// <summary>
        /// Writes a <see cref="Matrix4x4"/> instance as Matrix3x3 to the PBDF.
        /// </summary>
        /// <param name="pbdf">A <see cref="PbdfWriter"/> to write with.</param>
        /// <param name="value">A <see cref="Matrix3x3"/> instance to write.</param>
        internal static void WriteMatrix3x3(this PbdfWriter pbdf, Matrix4x4 value)
        {
            pbdf.WriteSingle(value.M11); pbdf.WriteSingle(value.M12); pbdf.WriteSingle(value.M13);
            pbdf.WriteSingle(value.M21); pbdf.WriteSingle(value.M22); pbdf.WriteSingle(value.M23);
            pbdf.WriteSingle(value.M31); pbdf.WriteSingle(value.M32); pbdf.WriteSingle(value.M33);
        }

        /// <summary>
        /// Writes a <see cref="Point"/> instance to the PBDF.
        /// </summary>
        /// <param name="pbdf">A <see cref="PbdfWriter"/> to write with.</param>
        /// <param name="value">A <see cref="Point"/> instance to write.</param>
        internal static void WritePoint(this PbdfWriter pbdf, Point value)
        {
            pbdf.WriteInt32(value.X);
            pbdf.WriteInt32(value.Y);
        }

        /// <summary>
        /// Writes a <see cref="String"/> instance as a buffer of the given <paramref name="length"/>.
        /// </summary>
        /// <param name="pbdf">A <see cref="PbdfWriter"/> to write with.</param>
        /// <param name="length">The size of the buffer the string will be stored in.</param>
        internal static void WriteStringBuffer(this PbdfWriter pbdf, string value, int length)
        {
            Span<byte> buffer = stackalloc byte[length];
            pbdf.Encoding.GetBytes(value, buffer);
            pbdf.Write(buffer);
        }

        /// <summary>
        /// Writes a <see cref="Vector3"/> instance to the PBDF.
        /// </summary>
        /// <param name="pbdf">A <see cref="PbdfWriter"/> to write with.</param>
        /// <param name="value">A <see cref="Vector3"/> instance to write.</param>
        internal static void WriteVector3(this PbdfWriter pbdf, Vector3 value)
        {
            pbdf.WriteSingle(value.X);
            pbdf.WriteSingle(value.Y);
            pbdf.WriteSingle(value.Z);
        }
    }
}
