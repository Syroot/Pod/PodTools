﻿using System;
using System.Drawing;
using System.Numerics;

namespace Syroot.Pod.Pbdf
{
    /// <summary>
    /// Represents extension methods for <see cref="PbdfReader"/> instances.
    /// </summary>
    internal static class PbdfReaderExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Reads a <see cref="Boolean"/> instance as a 4-byte value from the PBDF.
        /// </summary>
        /// <param name="pbdf">A <see cref="PbdfReader"/> to read with.</param>
        /// <returns>The read <see cref="Boolean"/> instance.</returns>
        internal static bool ReadBoolean4(this PbdfReader pbdf)
        {
            return pbdf.ReadInt32() != 0;
        }

        /// <summary>
        /// Reads a <see cref="Matrix4x4"/> instance as Matrix3x3 from the PBDF.
        /// </summary>
        /// <param name="pbdf">A <see cref="PbdfReader"/> to read with.</param>
        /// <returns>The read <see cref="Matrix4x4"/> instance.</returns>
        internal static Matrix4x4 ReadMatrix3x3(this PbdfReader pbdf)
        {
            return new(
                pbdf.ReadSingle(), pbdf.ReadSingle(), pbdf.ReadSingle(), 0,
                pbdf.ReadSingle(), pbdf.ReadSingle(), pbdf.ReadSingle(), 0,
                pbdf.ReadSingle(), pbdf.ReadSingle(), pbdf.ReadSingle(), 0,
                0, 0, 0, 1);
        }

        /// <summary>
        /// Reads a <see cref="Point"/> instance from the PBDF.
        /// </summary>
        /// <param name="pbdf">A <see cref="PbdfReader"/> to read with.</param>
        /// <returns>The read <see cref="Point"/> instance.</returns>
        internal static Point ReadPoint(this PbdfReader pbdf)
        {
            Point value = new Point();
            value.X = pbdf.ReadInt32();
            value.Y = pbdf.ReadInt32();
            return value;
        }

        /// <summary>
        /// Reads a <see cref="String"/> instance which is stored in a buffer of the given <paramref name="length"/>.
        /// </summary>
        /// <param name="pbdf">A <see cref="PbdfReader"/> to read with.</param>
        /// <param name="length">The size of the buffer the string is stored in.</param>
        /// <returns>The read <see cref="String"/> instance.</returns>
        internal static string ReadStringBuffer(this PbdfReader pbdf, int length)
        {
            Span<byte> buffer = stackalloc byte[length];
            pbdf.Read(buffer);
            return pbdf.Encoding.GetString(buffer).TrimEnd('\0');
        }

        /// <summary>
        /// Reads a <see cref="Vector3"/> instance from the PBDF.
        /// </summary>
        /// <param name="pbdf">A <see cref="PbdfReader"/> to read with.</param>
        /// <returns>The read <see cref="Vector3"/> instance.</returns>
        internal static Vector3 ReadVector3(this PbdfReader pbdf)
        {
            return new(pbdf.ReadSingle(), pbdf.ReadSingle(), pbdf.ReadSingle());
        }
    }
}
