﻿using System;
using System.Buffers.Binary;
using System.IO;
using System.Text;

namespace Syroot.Pod.Pbdf
{
    /// <summary>
    /// Represents a read-only POD binary data file (PBDF) stream.
    /// </summary>
    public sealed class PbdfReader : Stream, IDisposable
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly Stream _baseStream;
        private readonly bool _leaveOpen;
        private readonly byte[] _buffer;
        private int _bufferIndex;
        private int _bufferPos;
        private readonly int[] _offsets;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="PbdfReader"/> class with the given settings.
        /// </summary>
        /// <param name="baseStream">An underlying <see cref="Stream"/> to read input data from. This stream requires to
        /// be readable and seekable.</param>
        /// <param name="key">An encryption key with which the contents of this file are encrypted, or 0 to
        /// automatically detect it.</param>
        /// <param name="bufferSize">A size of a block in the file at which a checksum follows, or 0 to automatically
        /// detect it.</param>
        /// <param name="encoding">An <see cref="System.Text.Encoding"/> to use for reading strings or
        /// <see langword="null"/> for using the default 1252 codepage.</param>
        /// <param name="leaveOpen">Whether to leave the <see cref="Stream"/> open after disposing this instance.</param>
        public PbdfReader(Stream baseStream, uint key = 0, int bufferSize = 0, Encoding? encoding = null,
            bool leaveOpen = false)
        {
            // Validate base stream.
            if (!baseStream.CanRead)
                throw new ArgumentException($"Base stream requires to be readable.", nameof(baseStream));
            if (!baseStream.CanSeek)
                throw new ArgumentException($"Base stream requires to be seekable.", nameof(baseStream));
            _baseStream = baseStream;
            _leaveOpen = leaveOpen;

            // Validate key.
            if (key == 0)
            {
                _baseStream.Seek(0, SeekOrigin.Begin);
                key = LoadDword() ^ (uint)_baseStream.Length;
            }
            Key = key;

            // Validate buffer size.
            if (bufferSize == 0)
            {
                _baseStream.Seek(0, SeekOrigin.Begin);
                uint checksum = 0;
                while (_baseStream.Position < _baseStream.Length)
                {
                    uint dword = LoadDword();
                    if (dword == checksum && _baseStream.Length % _baseStream.Position == 0)
                    {
                        bufferSize = (int)_baseStream.Position;
                        break;
                    }
                    checksum += dword ^ Key;
                }
                if (bufferSize == 0)
                    throw new ArgumentException("Could not automatically determine buffer size.", nameof(bufferSize));
            }
            BufferSize = bufferSize;
            _buffer = new byte[BufferSize];
            _bufferIndex = -1;
            _bufferPos = BufferSize;

            // Validate encoding.
            Encoding = encoding ?? Encoding.GetEncoding(1252);

            // Read header.
            _baseStream.Seek(0, SeekOrigin.Begin);

            if (ReadInt32() != _baseStream.Length)
                throw new IOException("Invalid PBDF size.");

            _offsets = new int[ReadInt32()];
            for (int i = 0; i < _offsets.Length; i++)
                _offsets[i] = ReadInt32();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override bool CanRead => true;

        /// <inheritdoc/>
        public override bool CanSeek => false;

        /// <inheritdoc/>
        public override bool CanWrite => false;

        /// <inheritdoc/>
        public override long Length => throw new NotSupportedException("PBDF length cannot be retrieved.");

        /// <inheritdoc/>
        public override long Position
        {
            get => throw new NotSupportedException("PBDF position cannot be retrieved.");
            set => throw new NotSupportedException("PBDF data can only be seeked to offsets.");
        }

        /// <summary>
        /// Gets the size of a block at which a checksum follows.
        /// </summary>
        public int BufferSize { get; }

        /// <summary>
        /// Gets or sets the <see cref="System.Text.Encoding"/> to use for reading strings.
        /// </summary>
        public Encoding Encoding { get; set; }

        /// <summary>
        /// Gets the encryption key with which contents of the file are encrypted.
        /// </summary>
        public uint Key { get; }

        /// <summary>
        /// Gets the number offsets the PBDF can be seeked to through the <see cref="Offset"/> method.
        /// </summary>
        public int OffsetCount => _offsets.Length;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <inheritdoc/>
        public override void Flush()
        {
            // nop
        }

        /// <inheritdoc/>
        public override int Read(byte[] buffer, int offset, int count) => Read(buffer.AsSpan(offset, count));

        /// <inheritdoc/>
        public override int Read(Span<byte> buffer)
        {
            int pos = 0;
            while (pos < buffer.Length)
            {
                int bytesRemain = BufferSize - sizeof(uint) - _bufferPos;
                if (bytesRemain > 0)
                {
                    int size = Math.Min(bytesRemain, buffer.Length - pos);
                    _buffer.AsSpan(_bufferPos, size).CopyTo(buffer[pos..]);
                    pos += size;
                    _bufferPos += size;
                }
                else
                {
                    LoadBuffer();
                }
            }
            return buffer.Length;
        }

        /// <inheritdoc/>
        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotSupportedException($"PBDF can only be seeked with the {nameof(Offset)}() method.");
        }

        /// <inheritdoc/>
        public override void SetLength(long value) => throw new NotSupportedException($"PBDF length cannot be set.");

        /// <inheritdoc/>
        public override void Write(byte[] buffer, int offset, int count)
        {
            throw new NotSupportedException($"{nameof(PbdfReader)} is read-only, use a {nameof(PbdfWriter)} instead.");
        }

        /// <summary>
        /// Seeks to the PBDF section offset with the given <paramref name="index"/>.
        /// </summary>
        /// <param name="index">An index of a PBDF offset to seek to.</param>
        public void Offset(int index)
        {
            // Validate offset.
            if (index < 0 || index > OffsetCount)
                throw new ArgumentOutOfRangeException($"Index {index} is invalid in PBDF with {OffsetCount} offsets.");
            int offset = _offsets[index];

            // Update buffer status.
            int bufferIndex = offset / BufferSize;
            if (bufferIndex == _bufferIndex)
            {
                _bufferPos = offset % BufferSize;
            }
            else
            {
                _bufferIndex = bufferIndex;
                _bufferPos = BufferSize;
                _baseStream.Seek(_bufferIndex * BufferSize, SeekOrigin.Begin);
            }
        }

        /// <summary>
        /// Reads a <see cref="Byte"/> value from the PBDF.
        /// </summary>
        /// <returns>The read <see cref="Byte"/> instance.</returns>
        public new byte ReadByte()
        {
            Span<byte> buffer = stackalloc byte[sizeof(byte)];
            Read(buffer);
            return buffer[0];
        }

        /// <summary>
        /// Reads an <see cref="Int16"/> value from the PBDF.
        /// </summary>
        /// <returns>The read <see cref="Int16"/> instance.</returns>
        public short ReadInt16()
        {
            Span<byte> buffer = stackalloc byte[sizeof(short)];
            Read(buffer);
            return BinaryPrimitives.ReadInt16LittleEndian(buffer);
        }

        /// <summary>
        /// Reads an <see cref="Int32"/> value from the PBDF.
        /// </summary>
        /// <returns>The read <see cref="Int32"/> instance.</returns>
        public int ReadInt32()
        {
            Span<byte> buffer = stackalloc byte[sizeof(int)];
            Read(buffer);
            return BinaryPrimitives.ReadInt32LittleEndian(buffer);
        }

        /// <summary>
        /// Reads a 16x16 fixed point decimal as a <see cref="Single"/> value from the PBDF.
        /// </summary>
        /// <returns>The read <see cref="Single"/> instance.</returns>
        public float ReadSingle()
        {
            return ReadInt32() / (float)(1 << 16);
        }

        /// <summary>
        /// Reads an encrypted string as a <see cref="String"/> value from the PBDF.
        /// </summary>
        /// <returns>The read <see cref="String"/> instance.</returns>
        public string ReadString()
        {
            Span<byte> buffer = stackalloc byte[ReadByte()];
            Read(buffer);
            for (int i = 0; i < buffer.Length; i++)
                buffer[i] = (byte)(buffer[i] ^ ~i);
            return Encoding.GetString(buffer);
        }

        /// <summary>
        /// Reads an <see cref="Int32"/> value from the PBDF.
        /// </summary>
        /// <returns>The read <see cref="Int32"/> instance.</returns>
        public ushort ReadUInt16()
        {
            Span<byte> buffer = stackalloc byte[sizeof(ushort)];
            Read(buffer);
            return BinaryPrimitives.ReadUInt16LittleEndian(buffer);
        }

        /// <summary>
        /// Reads an <see cref="Int32"/> value from the PBDF.
        /// </summary>
        /// <returns>The read <see cref="Int32"/> instance.</returns>
        public uint ReadUInt32()
        {
            Span<byte> buffer = stackalloc byte[sizeof(uint)];
            Read(buffer);
            return BinaryPrimitives.ReadUInt32LittleEndian(buffer);
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <inheritdoc/>
        protected override void Dispose(bool disposing)
        {
            if (!_leaveOpen)
                _baseStream.Dispose();
            base.Dispose(disposing);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void LoadBuffer()
        {
            // Update buffer status.
            _bufferIndex++;
            _bufferPos = 0;

            // Read buffer.
            if (_baseStream.Read(_buffer) != _buffer.Length)
                throw new IOException($"Could not read complete PBDF buffer.");

            // Decrypt buffer.
            uint checksum = 0;
            if (_bufferIndex != 0 && (Key == 0x00005CA8 || Key == 0x0000D13F))
            {
                // Special encryption with specific keys in second and later buffers.
                uint encDword = 0;
                for (int i = 0; i < BufferSize - sizeof(uint); i += sizeof(uint))
                {
#pragma warning disable CS8509 // Warning for non-exhaustive cases is incorrect as it does not handle arithmetics
                    Span<byte> bufferDword = _buffer.AsSpan(i);
                    uint keyDword = encDword >> 16 & 3 switch
                    {
                        0 => encDword - 0x50A4A89D,
                        1 => 0x3AF70BC4 - encDword,
                        2 => (encDword + 0x07091971) << 1,
                        3 => (0x11E67319 - encDword) << 1
                    };
                    encDword = BinaryPrimitives.ReadUInt32LittleEndian(bufferDword);
                    uint decDword = encDword & 3 switch
                    {
                        0 => ~encDword ^ keyDword,
                        1 => ~encDword ^ ~keyDword,
                        2 => encDword ^ ~keyDword,
                        3 => encDword ^ keyDword ^ 0xFFFF
                    };
                    BinaryPrimitives.WriteUInt32LittleEndian(bufferDword, decDword);
                    checksum += decDword;
#pragma warning restore CS8509
                }
            }
            else
            {
                // Simple encryption for all buffers with most keys.
                for (int i = 0; i < BufferSize - sizeof(uint); i += sizeof(uint))
                {
                    Span<byte> bufferDword = _buffer.AsSpan(i);
                    uint encDword = BinaryPrimitives.ReadUInt32LittleEndian(bufferDword);
                    uint decDword = encDword ^ encDword;
                    BinaryPrimitives.WriteUInt32LittleEndian(bufferDword, decDword);
                    checksum += decDword;
                }
            }
            // Validate the checksum.
            if (checksum != BinaryPrimitives.ReadUInt32BigEndian(_buffer.AsSpan(BufferSize - sizeof(uint))))
                throw new IOException("Invalid PBDF buffer checksum.");
        }

        private uint LoadDword()
        {
            Span<byte> buffer = new byte[sizeof(uint)];
            if (_baseStream.Read(buffer) != buffer.Length)
                throw new IOException($"Could not read complete PBDF Dword.");
            return BinaryPrimitives.ReadUInt32LittleEndian(buffer);
        }
    }
}
