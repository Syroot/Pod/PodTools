﻿using Syroot.BinaryData;
using Syroot.Maths;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class RepairZone : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Vector3F[] Positions { get; set; }

        public Vector3F CenterPosition { get; set; }

        public float Height { get; set; }

        public float Delay { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            Positions = loader.ReadMany(4, () => loader.ReadVector3F16x16());
            CenterPosition = loader.ReadVector3F16x16();
            Height = loader.ReadSingle16x16();
            Delay = loader.ReadSingle16x16();
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WriteMany(Positions, x => saver.WriteVector3F16x16(x));
            saver.WriteVector3F16x16(CenterPosition);
            saver.WriteSingle16x16(Height);
            saver.WriteSingle16x16(Delay);
        }
    }
}