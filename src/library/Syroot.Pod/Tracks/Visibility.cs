﻿using System.Collections.Generic;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    /// <summary>
    /// Represents a list of indices referencing <see cref="Sector"/> instances of a <see cref="SectorList"/> which are
    /// shown if this visibility group is active.
    /// </summary>
    public class Visibility
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the indices of other sectors visible when this one is active, e.g. the player car is colliding with it.
        /// If <see langword="null"/>, collisions with this sector and resulting other sectors are completely ignored.
        /// If 0, only this sector is visible when active.
        /// </summary>
        public IList<int>? SectorIndices { get; set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf)
        {
            int count = pbdf.ReadInt32();
            if (count > 0)
            {
                SectorIndices = new List<int>(count);
                for (int i = 0; i < count; i++)
                    SectorIndices.Add(pbdf.ReadInt32());
            }
            else
            {
                SectorIndices = null;
            }
        }

        internal void Write(PbdfWriter pbdf)
        {
            if (SectorIndices == null)
            {
                pbdf.WriteInt32(-1);
            }
            else
            {
                pbdf.WriteInt32(SectorIndices.Count);
                for (int i = 0; i < SectorIndices.Count; i++)
                    pbdf.WriteInt32(SectorIndices[i]);
            }
        }
    }
}
