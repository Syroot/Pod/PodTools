﻿using Syroot.BinaryData;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class DesignationValue : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public uint[] Data { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            Data = loader.ReadUInt32s(8);
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WriteUInt32s(Data);
        }
    }
}