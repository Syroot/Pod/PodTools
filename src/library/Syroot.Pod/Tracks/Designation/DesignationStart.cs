﻿using Syroot.BinaryData;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class DesignationStart : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public byte[] Data { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            Data = loader.ReadBytes(36);
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WriteBytes(Data);
        }
    }
}