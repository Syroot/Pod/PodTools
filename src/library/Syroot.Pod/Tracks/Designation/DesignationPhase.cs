﻿using Syroot.BinaryData;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class DesignationPhase : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name { get; set; }

        public int MacroIndex { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            Name = loader.ReadPodString();
            MacroIndex = loader.ReadInt32();
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WritePodString(Name);
            saver.WriteInt32(MacroIndex);
        }
    }
}