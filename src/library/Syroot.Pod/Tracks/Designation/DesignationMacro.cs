﻿using Syroot.BinaryData;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class DesignationMacro : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public byte[] Data { get; set; }

        public int MacroIndex1 { get; set; }

        public int MacroIndex2 { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            Data = loader.ReadBytes(60);
            MacroIndex1 = loader.ReadInt32();
            MacroIndex2 = loader.ReadInt32();
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WriteBytes(Data);
            saver.WriteInt32(MacroIndex1);
            saver.WriteInt32(MacroIndex2);
        }
    }
}