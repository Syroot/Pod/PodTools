﻿using System;
using System.Collections.Generic;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    /// <summary>
    /// Represents extension methods for <see cref="PbdfWriter"/> instances.
    /// </summary>
    public static class PbdfWriterExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static void WriteList<T>(this PbdfWriter pbdf, IList<T> value, Action<T> writeCallback)
        {
            pbdf.WriteInt32(value.Count);
            for (int i = 0; i < value.Count; i++)
                writeCallback(value[i]);
        }

        internal static void WriteMacros(this PbdfWriter pbdf, IList<uint[]> value, int parameterCount)
        {
            pbdf.WriteInt32(value.Count);
            for (int i = 0; i < value.Count; i++)
            {
                uint[] macro = value[i];
                if (macro.Length != parameterCount)
                    throw new ArgumentException("Macro parameter count mismatch.", nameof(parameterCount));
                for (int j = 0; j < macro.Length; j++)
                    pbdf.WriteUInt32(macro[j]);
            }
        }




        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private const string _noneName = "NEANT";


        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Saves an <see cref="ISectionData"/> instance of type <typeparamref name="T"/>, writing "NEANT" if the
        /// instance is <see langword="null"/>.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="ISectionData"/> to save.</typeparam>
        /// <param name="self">The extended <see cref="DataSaver{T}"/> instance.</param>
        /// <param name="value">The instance to save.</param>
        /// <param name="parameter">The optional parameter to pass in to the saving instance.</param>
        public static void SaveSection<T>(this DataSaver<Track> self, T value, object parameter = null)
            where T : class, ISectionData, new()
        {
            if (value == null)
            {
                self.WritePodString(_noneName);
            }
            else
            {
                self.WritePodString(value.Name);
                value.Save(self, parameter);
            }
        }

        /// <summary>
        /// Saves an <see cref="IDifficultySectionData"/> instance of type <typeparamref name="T"/>, writing "NEANT" if
        /// the instance is <see langword="null"/>.
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IDifficultySectionData"/> to save.</typeparam>
        /// <param name="self">The extended <see cref="DataSaver{T}"/> instance.</param>
        /// <param name="value">The instance to save.</param>
        /// <param name="difficultyName">The name of the difficulty.</param>
        public static void SaveDifficultySection<T>(this DataSaver<Track> self, T value, string difficultyName)
            where T : IDifficultySectionData, new()
        {
            if (value == null)
            {
                self.WritePodString(difficultyName);
                self.WritePodString(_noneName);
            }
            else
            {
                self.WritePodString(value.DifficultyName);
                self.WritePodString(value.Name);
                value.Save(self);
            }
        }
    }
}
