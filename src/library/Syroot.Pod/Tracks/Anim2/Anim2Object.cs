﻿using System.Collections.Generic;
using System.Linq;
using Syroot.BinaryData;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class Anim2Object : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int AnimIndex { get; set; }

        public IList<Anim2ObjectFrame> Frames { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            AnimIndex = loader.ReadInt32();
            Frames = loader.LoadMany<Anim2ObjectFrame>(loader.ReadInt32()).ToList();
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WriteInt32(AnimIndex);
            saver.WriteInt32(Frames.Count);
            saver.SaveMany(Frames);
        }
    }
}