﻿using Syroot.BinaryData;
using Syroot.Maths;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class Anim2TextureKey : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int TextureIndex { get; set; }

        public Vector2U[] TexCoords { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            TextureIndex = loader.ReadInt32();
            TexCoords = loader.ReadMany(4, () => loader.ReadVector2U());
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WriteInt32(TextureIndex);
            saver.WriteMany(TexCoords, x => saver.WriteVector2U(x));
        }
    }
}