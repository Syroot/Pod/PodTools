﻿using System.Collections.Generic;
using System.Linq;
using Syroot.BinaryData;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class Anim2Texture : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name { get; set; }

        public IList<Anim2TextureKey> Keys { get; set; }

        public float TotalTime { get; set; }

        public IList<Anim2TextureFrame> Frames { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            Name = loader.ReadPodString();
            Keys = loader.LoadMany<Anim2TextureKey>(loader.ReadInt32()).ToList();
            TotalTime = loader.ReadSingle16x16();
            Frames = loader.LoadMany<Anim2TextureFrame>(loader.ReadInt32()).ToList();
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WritePodString(Name);
            saver.WriteInt32(Keys.Count);
            saver.SaveMany(Keys);
            saver.WriteSingle16x16(TotalTime);
            saver.WriteInt32(Frames.Count);
            saver.SaveMany(Frames);
        }
    }
}