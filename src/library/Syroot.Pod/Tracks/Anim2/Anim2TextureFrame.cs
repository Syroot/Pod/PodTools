﻿using Syroot.BinaryData;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class Anim2TextureFrame : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int Time { get; set; }

        public int KeyIndex { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            Time = loader.ReadInt32();
            KeyIndex = loader.ReadInt32();
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WriteInt32(Time);
            saver.WriteInt32(KeyIndex);
        }
    }
}