﻿using System.Collections.Generic;
using System.Linq;
using Syroot.BinaryData;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class Anim1SectorList : List<Anim1Sector>, IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public uint Unknown1 { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            int count = loader.ReadInt32();
            Unknown1 = loader.ReadUInt32();
            AddRange(loader.LoadMany<Anim1Sector>(count).ToList());
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WriteInt32(Count);
            saver.Write(Unknown1);
            saver.SaveMany(this);
        }
    }
}
