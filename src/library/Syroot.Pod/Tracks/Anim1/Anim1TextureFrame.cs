﻿using System.Collections.Generic;
using System.Linq;
using Syroot.BinaryData;
using Syroot.Maths;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    /// <summary>
    /// Represents the <see cref="Anim1TextureKey"/> instances available at a specific time frame.
    /// </summary>
    public class Anim1TextureFrame : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Vector3U Unknown { get; set; }

        /// <summary>
        /// Gets or sets the list of <see cref="Anim1TextureKey"/> instances which set the transformation of each
        /// <see cref="Texture"/>.
        /// </summary>
        public IList<Anim1TextureKey> Keys { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            Unknown = loader.ReadVector3U();
            Keys = loader.LoadMany<Anim1TextureKey>(loader.ReadInt32()).ToList();
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WriteVector3U(Unknown);
            saver.WriteInt32(Keys.Count);
            saver.SaveMany(Keys);
        }
    }
}