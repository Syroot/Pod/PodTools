﻿using System.Collections.Generic;
using System.Numerics;
using Syroot.Pod.Meshes;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    /// <summary>
    /// Represents a mesh which visibility is toggled to increase performance.
    /// </summary>
    public class Sector
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public Mesh Mesh { get; set; } = new Mesh();

        public IList<byte> VertexGamma { get; set; } = new List<byte>();

        public Vector3 BoundingBoxMin { get; set; } // Z -= 2

        public Vector3 BoundingBoxMax { get; set; } // Z += 10

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf, bool namedFaces)
        {
            Mesh = new();
            Mesh.Read(pbdf, namedFaces, true);

            VertexGamma = new List<byte>(Mesh.Positions.Count);
            for (int i = 0; i < VertexGamma.Count; i++)
                VertexGamma.Add(pbdf.ReadByte());

            BoundingBoxMin = pbdf.ReadVector3();
            BoundingBoxMax = pbdf.ReadVector3();
        }

        internal void Write(PbdfWriter pbdf, bool namedFaces)
        {
            Mesh.Write(pbdf, namedFaces, true);

            for (int i = 0; i < VertexGamma.Count; i++)
                pbdf.WriteByte(VertexGamma[i]);

            pbdf.WriteVector3(BoundingBoxMin);
            pbdf.WriteVector3(BoundingBoxMax);
        }
    }
}
