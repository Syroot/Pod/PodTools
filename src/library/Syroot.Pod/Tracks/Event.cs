﻿using System;
using System.Collections.Generic;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    public class Event
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name { get; set; } = String.Empty;

        public IList<byte[]> Params { get; set; } = new List<byte[]>();

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf)
        {
            Name = pbdf.ReadString();

            int paramCount = pbdf.ReadInt32();
            int paramSize = pbdf.ReadInt32();

            Params = new List<byte[]>(paramCount);
            for (int i = 0; i < paramCount; i++)
            {
                byte[] paramData = new byte[paramSize];
                pbdf.Read(paramData);
                Params.Add(paramData);
            }
        }

        internal void Write(PbdfWriter pbdf)
        {
            pbdf.WriteString(Name);

            pbdf.WriteInt32(Params.Count);
            pbdf.WriteInt32(Params.Count == 0 ? 0 : Params[0].Length);
            for (int i = 0; i < Params.Count; i++)
            {
                pbdf.Write(Params[i]);
            }
        }
    }
}
