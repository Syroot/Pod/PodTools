﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Syroot.Pod.Pbdf;
using Syroot.Pod.Textures;

namespace Syroot.Pod.Tracks
{
    /// <summary>
    /// Represents BL4 circuit files.
    /// </summary>
    public class Track
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const uint _key = 0xF7E;
        private const int _bufferSize = 0x4000;
        private const int _offsetCount = 9;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IList<Event> Events { get; set; } = new List<Event>();
        public IList<uint[]> MacrosBase { get; set; } = new List<uint[]>();
        public IList<uint[]> Macros { get; set; } = new List<uint[]>();
        public IList<uint[]> MacrosInit { get; set; } = new List<uint[]>();
        public IList<uint[]> MacrosActive { get; set; } = new List<uint[]>();
        public IList<uint[]> MacrosInactive { get; set; } = new List<uint[]>();
        public IList<uint[]> MacrosReplace { get; set; } = new List<uint[]>();
        public IList<uint[]> MacrosExchange { get; set; } = new List<uint[]>();

        public string TrackName { get; set; } = String.Empty;

        public uint[] LevelOfDetail { get; } = new uint[16];

        public string ProjectName { get; set; } = String.Empty;

        public TextureList Textures { get; } = new TextureList(256, TextureFormat.RGB565);

        public bool HasNamedSectorFaces { get; set; }
        public IList<Sector> Sectors { get; set; } = new List<Sector>();
        public IList<Visibility> Visibilities { get; set; } = new List<Visibility>();

        public EnvironmentSection? EnvironmentSection { get; set; }

        public LightSection LightSection { get; set; }

        public Anim1Section Anim1Section { get; set; }

        public SoundSection SoundSection { get; set; }

        public Background Background { get; set; }

        public Sky Sky { get; set; }

        public Anim2SectionList Anim2Sections { get; set; }

        public RepairZoneSection RepairZoneSection { get; set; }

        public Designation DesignationForward { get; set; }

        public DifficultySection DifficultyForwardEasy { get; set; }
        public LevelSection LevelForwardEasy { get; set; }

        public DifficultySection DifficultyForwardNormal { get; set; }
        public LevelSection LevelForwardNormal { get; set; }

        public DifficultySection DifficultyForwardHard { get; set; }
        public LevelSection LevelForwardHard { get; set; }

        public Designation DesignationReverse { get; set; }
        public DifficultySection DifficultyReverseEasy { get; set; }
        public LevelSection LevelReverseEasy { get; set; }

        public DifficultySection DifficultyReverseNormal { get; set; }
        public LevelSection LevelReverseNormal { get; set; }

        public DifficultySection DifficultyReverseHard { get; set; }
        public LevelSection LevelReverseHard { get; set; }

        public CompetitorSection CompetitorsEasy { get; set; }
        public CompetitorSection CompetitorsNormal { get; set; }
        public CompetitorSection CompetitorsHard { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Reads the track from the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to read the track from.</param>
        /// <param name="encoding">The <see cref="Encoding"/> to use. Defaults to cp1252. Some fan-made tracks require
        /// a special code page to be read properly.</param>
        public void Load(string fileName, Encoding? encoding = null)
        {
            using PbdfReader pbdf = new(new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read),
                _key, _bufferSize, encoding);

            // Load header.
            pbdf.Offset((int)Offset.Default);
            if (pbdf.ReadInt32() != 3)
                throw new InvalidDataException("Check byte is not 3.");
            pbdf.ReadInt32(); // reserved, not used

            LoadEvents(pbdf);

            // Load general data.
            TrackName = pbdf.ReadString();
            for (int i = 0; i < LevelOfDetail.Length; i++)
                LevelOfDetail[i] = pbdf.ReadUInt32();
            ProjectName = pbdf.ReadString();
            Textures.Read(pbdf);

            LoadSectors(pbdf);

            // Load optional sections.
            if (pbdf.ReadSectionName(out string environmentName))
            {
                EnvironmentSection = new();
                EnvironmentSection.Name = environmentName;
                EnvironmentSection.Read(pbdf, Sectors.Count);
            }
            if (pbdf.ReadSectionName(out string lightName))
            {
                LightSection = new();
                LightSection.Name = lightName;
                LightSection.Read(pbdf);
            }
            Anim1Section = pbdf.LoadSection<Anim1Section>();
            SoundSection = pbdf.LoadSection<SoundSection>();
            Background = pbdf.Load<Background>();
            Sky = pbdf.Load<Sky>();
            Anim2Sections = pbdf.Load<Anim2SectionList>();
            RepairZoneSection = pbdf.LoadSection<RepairZoneSection>();

            // Load forward specifications.
            DesignationForward = pbdf.Load<Designation>();

            DifficultyForwardEasy = pbdf.LoadDifficultySection<DifficultySection>();
            LevelForwardEasy = pbdf.LoadSection<LevelSection>();

            pbdf.Position = Offsets[(int)Offset.DifficultyForwardNormal];
            DifficultyForwardNormal = pbdf.LoadDifficultySection<DifficultySection>();
            LevelForwardNormal = pbdf.LoadSection<LevelSection>();

            pbdf.Position = Offsets[(int)Offset.DifficultyForwardHard];
            DifficultyForwardHard = pbdf.LoadDifficultySection<DifficultySection>();
            LevelForwardHard = pbdf.LoadSection<LevelSection>();

            // Load reverse specifications.
            pbdf.Position = Offsets[(int)Offset.DesignationReverse];
            DesignationReverse = pbdf.Load<Designation>();

            DifficultyReverseEasy = pbdf.LoadDifficultySection<DifficultySection>();
            LevelReverseEasy = pbdf.LoadSection<LevelSection>();

            pbdf.Position = Offsets[(int)Offset.DifficultyReverseNormal];
            DifficultyReverseNormal = pbdf.LoadDifficultySection<DifficultySection>();
            LevelReverseNormal = pbdf.LoadSection<LevelSection>();

            pbdf.Position = Offsets[(int)Offset.DifficultyReverseHard];
            DifficultyReverseHard = pbdf.LoadDifficultySection<DifficultySection>();
            LevelReverseHard = pbdf.LoadSection<LevelSection>();

            // Load competitors.
            pbdf.Position = Offsets[(int)Offset.CompetitorsEasy];
            CompetitorsEasy = pbdf.LoadDifficultySection<CompetitorSection>();
            pbdf.Position = Offsets[(int)Offset.CompetitorsNormal];
            CompetitorsNormal = pbdf.LoadDifficultySection<CompetitorSection>();
            pbdf.Position = Offsets[(int)Offset.CompetitorsHard];
            CompetitorsHard = pbdf.LoadDifficultySection<CompetitorSection>();
        }

        /// <summary>
        /// Writes the track to the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to write the track to.</param>
        /// <param name="encoding">The <see cref="Encoding"/> to use. Defaults to cp1252. Some fan-made tracks require
        /// a special code page to be written properly.</param>
        public void Save(string fileName, Encoding? encoding = null)
        {
            using PbdfWriter pbdf = new(new FileStream(fileName, FileMode.Open, FileAccess.Write, FileShare.None),
                _key, _bufferSize, _offsetCount, encoding);

            Offsets.Add((int)pbdf.Position);
            pbdf.WriteUInt32(3);
            pbdf.WriteUInt32(0);

            // Save events and macros.
            pbdf.WriteInt32(Events.Count);
            int bufferSize = 0;
            foreach (Event evnt in Events)
            {
                if (evnt.Params.Length > 0)
                    bufferSize += evnt.Params.Length * evnt.Params[0].Length;
            }
            pbdf.WriteInt32(bufferSize);
            pbdf.SaveMany(Events);
            pbdf.WriteInt32(MacrosBase.Count);
            pbdf.SaveMany(MacrosBase);
            pbdf.WriteInt32(Macros.Count);
            pbdf.SaveMany(Macros);
            pbdf.WriteInt32(MacrosInit.Count);
            pbdf.SaveMany(MacrosInit);
            pbdf.WriteInt32(MacrosActive.Count);
            pbdf.SaveMany(MacrosActive);
            pbdf.WriteInt32(MacrosInactive.Count);
            pbdf.SaveMany(MacrosInactive);
            pbdf.WriteInt32(MacrosReplace.Count);
            pbdf.SaveMany(MacrosReplace);
            pbdf.WriteInt32(MacrosExchange.Count);
            pbdf.SaveMany(MacrosExchange);

            // Save general data.
            pbdf.WritePodString(TrackName);
            pbdf.WriteUInt32s(LevelOfDetail);
            pbdf.WritePodString(ProjectName);
            pbdf.Save(Textures);
            pbdf.WriteBoolean(HasNamedSectorFaces, BooleanCoding.Dword);
            pbdf.WriteInt32(Sectors.Count);
            pbdf.SaveMany(Sectors);
            pbdf.WriteInt32(Visibilities.Count);
            pbdf.SaveMany(Visibilities);
            pbdf.SaveSection(EnvironmentSection);
            pbdf.SaveSection(LightSection);
            pbdf.SaveSection(Anim1Section);
            pbdf.SaveSection(SoundSection);
            pbdf.Save(Background);
            pbdf.Save(Sky);
            pbdf.Save(Anim2Sections);
            pbdf.SaveSection(RepairZoneSection);

            // Save forward specifications.
            pbdf.Save(DesignationForward);

            pbdf.SaveDifficultySection(DifficultyForwardEasy, "EASY");
            pbdf.SaveSection(LevelForwardEasy);

            Offsets.Add((int)pbdf.Position);
            pbdf.SaveDifficultySection(DifficultyForwardNormal, "NORMAL");
            pbdf.SaveSection(LevelForwardNormal);

            Offsets.Add((int)pbdf.Position);
            pbdf.SaveDifficultySection(DifficultyForwardHard, "HARD");
            pbdf.SaveSection(LevelForwardHard);

            // Save reverse specifications.
            Offsets.Add((int)pbdf.Position);
            pbdf.Save(DesignationReverse);
            pbdf.SaveDifficultySection(DifficultyReverseEasy, "EASY");
            pbdf.SaveSection(LevelReverseEasy);

            Offsets.Add((int)pbdf.Position);
            pbdf.SaveDifficultySection(DifficultyReverseNormal, "NORMAL");
            pbdf.SaveSection(LevelReverseNormal);

            Offsets.Add((int)pbdf.Position);
            pbdf.SaveDifficultySection(DifficultyReverseHard, "HARD");
            pbdf.SaveSection(LevelReverseHard);

            // Save competitors.
            Offsets.Add((int)pbdf.Position);
            pbdf.SaveDifficultySection(CompetitorsEasy, "EASY");
            Offsets.Add((int)pbdf.Position);
            pbdf.SaveDifficultySection(CompetitorsNormal, "NORMAL");
            Offsets.Add((int)pbdf.Position);
            pbdf.SaveDifficultySection(CompetitorsHard, "HARD");

            Offsets.Add(0); // Seems to be a random offset.
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void LoadEvents(PbdfReader pbdf)
        {
            // Load events.
            int eventCount = pbdf.ReadInt32();
            pbdf.ReadInt32(); // bufferSize
            Events = new List<Event>(eventCount);
            for (int i = 0; i < eventCount; i++)
            {
                Event evnt = new();
                evnt.Read(pbdf);
                Events.Add(evnt);
            }

            // Load macros.
            MacrosBase = pbdf.ReadMacros(3);
            Macros = pbdf.ReadMacros(1);
            MacrosInit = pbdf.ReadMacros(1);
            MacrosActive = pbdf.ReadMacros(1);
            MacrosInactive = pbdf.ReadMacros(1);
            MacrosReplace = pbdf.ReadMacros(2);
            MacrosExchange = pbdf.ReadMacros(2);
        }

        private void LoadSectors(PbdfReader pbdf)
        {
            HasNamedSectorFaces = pbdf.ReadBoolean4();

            // Load sectors.
            int sectorCount = pbdf.ReadInt32();
            Sectors = new List<Sector>(sectorCount);
            for (int i = 0; i < sectorCount; i++)
            {
                Sector sector = new();
                sector.Read(pbdf, HasNamedSectorFaces);
                Sectors.Add(sector);
            }

            // Load sector visibilities.
            int visibilityCount = pbdf.ReadInt32();
            Visibilities = new List<Visibility>(visibilityCount);
            for (int i = 0; i < visibilityCount; i++)
            {
                Visibility visibility = new();
                visibility.Read(pbdf);
                Visibilities.Add(visibility);
            }
        }

        // ---- CLASSES, STRUCTS & ENUMS -------------------------------------------------------------------------------

        private enum Offset
        {
            Default,
            DifficultyForwardNormal,
            DifficultyForwardHard,
            DesignationReverse,
            DifficultyReverseNormal,
            DifficultyReverseHard,
            CompetitorsEasy,
            CompetitorsNormal,
            CompetitorsHard
        }
    }
}
