﻿using System.Collections.Generic;
using System.Drawing;
using System.Numerics;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    public class DecorationInstance
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int Index { get; set; }

        public IList<Point> Vectors { get; set; } = new List<Point>();

        public Vector3 Position { get; set; }

        public Matrix4x4 Rotation { get; set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf)
        {
            Index = pbdf.ReadInt32();

            int vectorCount = pbdf.ReadInt32();
            Vectors = new List<Point>(vectorCount);
            for (int i = 0; i < vectorCount; i++)
                Vectors.Add(pbdf.ReadPoint());

            Position = pbdf.ReadVector3();
            Rotation = pbdf.ReadMatrix3x3();
        }

        internal void Write(PbdfWriter pbdf)
        {
            pbdf.WriteInt32(Index);

            pbdf.WriteInt32(Vectors.Count);
            for (int i = 0; i < Vectors.Count; i++)
                pbdf.WritePoint(Vectors[i]);

            pbdf.WriteVector3(Position);
            pbdf.WriteMatrix3x3(Rotation);
        }
    }
}
