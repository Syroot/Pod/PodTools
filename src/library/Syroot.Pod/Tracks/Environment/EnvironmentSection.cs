﻿using System;
using System.Collections.Generic;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    public class EnvironmentSection
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name { get; set; } = String.Empty;

        public IList<uint[]> Macros { get; set; } = new List<uint[]>();

        public IList<Decoration> Decorations { get; set; } = new List<Decoration>();

        public IList<DecorationInstance> DecorationInstances { get; set; } = new List<DecorationInstance>();

        public IList<IList<DecorationInstance>> SectorInstances { get; set; } = new List<IList<DecorationInstance>>();

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf, int sectorCount)
        {
            Macros = pbdf.ReadMacros(3);

            Decorations = pbdf.ReadList<Decoration>(x => x.Read(pbdf));
            DecorationInstances = pbdf.ReadList<DecorationInstance>(x => x.Read(pbdf));

            SectorInstances = new List<IList<DecorationInstance>>(sectorCount);
            for (int i = 0; i < sectorCount; i++)
                SectorInstances.Add(pbdf.ReadList<DecorationInstance>(x => x.Read(pbdf)));
        }

        internal void Write(PbdfWriter pbdf, int sectorCount)
        {
            pbdf.WriteMacros(Macros, 3);

            pbdf.WriteList(Decorations, x => x.Write(pbdf));
            pbdf.WriteList(DecorationInstances, x => x.Write(pbdf));

            if (sectorCount != SectorInstances.Count)
                throw new ArgumentException("Sector decoration instance count mismatch.");
            for (int i = 0; i < sectorCount; i++)
                pbdf.WriteList(SectorInstances[i], x => x.Write(pbdf));
        }
    }
}
