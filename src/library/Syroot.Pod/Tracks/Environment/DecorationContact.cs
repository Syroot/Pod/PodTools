﻿using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    public class DecorationContact
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public byte[] Data { get; } = new byte[64];

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf)
        {
            pbdf.Read(Data);
        }

        internal void Write(PbdfWriter pbdf)
        {
            pbdf.Write(Data);
        }
    }
}