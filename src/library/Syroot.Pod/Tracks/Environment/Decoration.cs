﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Syroot.Pod.Meshes;
using Syroot.Pod.Pbdf;
using Syroot.Pod.Textures;

namespace Syroot.Pod.Tracks
{
    public class Decoration
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name { get; set; } = String.Empty;

        public string ObjectName { get; set; } = String.Empty;

        public TextureList Textures { get; set; } = new TextureList(128, TextureFormat.RGB565);

        public bool HasNamedFaces { get; set; }

        public Mesh Mesh { get; set; } = new();

        public Vector3 CollisionPrism1 { get; set; }

        public uint CollisionPrism2 { get; set; }

        public Vector3 CollisionPrism3 { get; set; }

        public uint Unknown1 { get; set; }

        public uint[] Unknown2 { get; } = new uint[3];

        public uint Unknown3 { get; set; }

        public uint Unknown4 { get; set; }

        public IList<DecorationContact> Contacts { get; set; } = new List<DecorationContact>();

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf)
        {
            Name = pbdf.ReadString();
            ObjectName = pbdf.ReadString();

            Textures = new(128, TextureFormat.RGB565);
            Textures.Read(pbdf);

            HasNamedFaces = pbdf.ReadBoolean4();
            Mesh = new();
            Mesh.Read(pbdf, HasNamedFaces, false);

            CollisionPrism1 = pbdf.ReadVector3();
            CollisionPrism2 = pbdf.ReadUInt32();
            CollisionPrism3 = pbdf.ReadVector3();
            Unknown1 = pbdf.ReadUInt32();
            for (int i = 0; i < Unknown2.Length; i++)
                Unknown2[i] = pbdf.ReadUInt32();
            Unknown3 = pbdf.ReadUInt32();
            Unknown4 = pbdf.ReadUInt32();

            Contacts = pbdf.ReadList<DecorationContact>(x => x.Read(pbdf));
        }

        internal void Write(PbdfWriter pbdf)
        {
            pbdf.WriteString(Name);
            pbdf.WriteString(ObjectName);

            Textures.Write(pbdf);

            pbdf.WriteBoolean4(HasNamedFaces);
            Mesh.Write(pbdf, HasNamedFaces, false);

            pbdf.WriteVector3(CollisionPrism1);
            pbdf.WriteUInt32(CollisionPrism2);
            pbdf.WriteVector3(CollisionPrism3);
            pbdf.WriteUInt32(Unknown1);
            for (int i = 0; i < 3; i++)
                pbdf.WriteUInt32(Unknown2[i]);
            pbdf.WriteUInt32(Unknown3);
            pbdf.WriteUInt32(Unknown4);

            pbdf.WriteList(Contacts, x => x.Write(pbdf));
        }
    }
}