﻿using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    public class Sound
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public uint[] Data { get; private set; } = new uint[14];

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf)
        {
            for (int i = 0; i < Data.Length; i++)
                Data[i] = pbdf.ReadUInt32();
        }

        internal void Write(PbdfWriter pbdf)
        {
            for (int i = 0; i < Data.Length; i++)
                pbdf.WriteUInt32(Data[i]);
        }
    }
}
