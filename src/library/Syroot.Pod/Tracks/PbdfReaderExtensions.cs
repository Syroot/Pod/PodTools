﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    /// <summary>
    /// Represents extension methods for <see cref="PbdfReader"/> instances.
    /// </summary>
    internal static class PbdfReaderExtensions
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static IList<T> ReadList<T>(this PbdfReader pbdf, Action<T> readCallback)
            where T : new()
        {
            int count = pbdf.ReadInt32();
            IList<T> list = new List<T>(count);
            while (count-- > 0)
            {
                T element = new();
                readCallback(element);
                list.Add(element);
            }
            return list;
        }

        internal static IList<uint[]> ReadMacros(this PbdfReader pbdf, int parameterCount)
        {
            int count = pbdf.ReadInt32();
            List<uint[]> macros = new(count);
            for (int i = 0; i < count; i++)
            {
                uint[] macro = new uint[parameterCount];
                for (int j = 0; j < macro.Length; j++)
                    macro[j] = pbdf.ReadUInt32();
                macros.Add(macro);
            }
            return macros;
        }

        internal static bool ReadSectionName(this PbdfReader pbdf, out string name)
        {
            name = pbdf.ReadString();
            return !name.Equals("NEANT", StringComparison.OrdinalIgnoreCase);
        }

        /// <summary>
        /// Loads an <see cref="IDifficultySectionData"/> instance of type <typeparamref name="T"/>, returning
        /// <c>null</c> if the name is "NEANT".
        /// </summary>
        /// <typeparam name="T">The type of the <see cref="IDifficultySectionData"/> to load.</typeparam>
        /// <param name="self">The extended <see cref="DataLoader{T}"/> instance.</param>
        /// <param name="parameter">The optional parameter to pass in to the loading instance.</param>
        /// <returns>The loaded difficulty section instance or <see langword="null"/> if the name was "NEANT".</returns>
        public static T LoadDifficultySection<T>(this DataLoader<Track> self, object parameter = null)
            where T : IDifficultySectionData, new()
        {
            string difficultyName = self.ReadPodString();
            string name = self.ReadPodString();
            if (name == String.Empty || String.Compare(name, _noneName, true, CultureInfo.InvariantCulture) == 0)
            {
                return default;
            }
            else
            {
                T data = new();
                data.DifficultyName = difficultyName;
                data.Name = name;
                data.Load(self, parameter);
                return data;
            }
        }
    }
}
