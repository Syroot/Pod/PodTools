﻿using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    public class Light
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public uint Type { get; set; }

        public byte[] Data { get; } = new byte[48];

        public uint Value2 { get; set; }

        public uint Value3 { get; set; }

        public uint Diffusion { get; set; }

        public uint Values { get; set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf)
        {
            Type = pbdf.ReadUInt32();
            pbdf.Read(Data);
            Value2 = pbdf.ReadUInt32();
            Value3 = pbdf.ReadUInt32();
            Diffusion = pbdf.ReadUInt32();
            Values = pbdf.ReadUInt32();
        }

        internal void Write(PbdfWriter pbdf)
        {
            pbdf.WriteUInt32(Type);
            pbdf.Write(Data);
            pbdf.WriteUInt32(Value2);
            pbdf.WriteUInt32(Value3);
            pbdf.WriteUInt32(Diffusion);
            pbdf.WriteUInt32(Values);
        }
    }
}
