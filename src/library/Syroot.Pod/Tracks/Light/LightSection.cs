﻿using System;
using System.Collections.Generic;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Tracks
{
    public class LightSection
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name { get; set; } = String.Empty;

        public uint Value1 { get; set; }

        public uint[] Value2 { get; } = new uint[3];

        public uint Value3 { get; set; }

        public uint Value4 { get; set; }

        public uint Value5 { get; set; }

        public uint Value6 { get; set; }

        public uint Value7 { get; set; }

        public uint Value8 { get; set; }

        public uint Value9 { get; set; }

        public IList<Light>? GlobalLights { get; set; }

        public IList<IList<Light>> SectorLights { get; set; } = new List<IList<Light>>();

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf)
        {
            int lights = pbdf.ReadInt32();

            Value1 = pbdf.ReadUInt32();
            for (int i = 0; i < Value2.Length; i++)
                Value2[i] = pbdf.ReadUInt32();
            Value3 = pbdf.ReadUInt32();
            Value4 = pbdf.ReadUInt32();
            Value5 = pbdf.ReadUInt32();
            Value6 = pbdf.ReadUInt32();
            Value7 = pbdf.ReadUInt32();
            Value8 = pbdf.ReadUInt32();
            Value9 = pbdf.ReadUInt32();

            if (lights > 0)
                GlobalLights = pbdf.ReadList<Light>(x => x.Read(pbdf));

            SectorLights = new List<IList<Light>>();
            for (int i = 0; i < lights; i++)
                SectorLights.Add(pbdf.ReadList<Light>(x => x.Read(pbdf)));
        }

        internal void Write(PbdfWriter pbdf)
        {
            pbdf.WriteInt32(SectorLights.Count);

            pbdf.WriteUInt32(Value1);
            for (int i = 0; i < Value2.Length; i++)
                pbdf.WriteUInt32(Value2[i]);
            pbdf.WriteUInt32(Value3);
            pbdf.WriteUInt32(Value4);
            pbdf.WriteUInt32(Value5);
            pbdf.WriteUInt32(Value6);
            pbdf.WriteUInt32(Value7);
            pbdf.WriteUInt32(Value8);
            pbdf.WriteUInt32(Value9);

            if (GlobalLights != null)
                pbdf.WriteList(GlobalLights, x => x.Write(pbdf));

            for (int i = 0; i < SectorLights.Count; i++)
                pbdf.WriteList(SectorLights[i], x => x.Write(pbdf));
        }
    }
}
