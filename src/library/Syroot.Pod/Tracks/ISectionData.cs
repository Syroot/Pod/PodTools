﻿using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public interface ISectionData : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        string Name { get; set; }
    }
}