﻿using System.Collections.Generic;
using Syroot.BinaryData;
using Syroot.Maths;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class Competitor : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name { get; set; }

        public int Unknown1 { get; set; }

        public int Unknown2 { get; set; }

        public string Number { get; set; }

        public IList<Vector3U> Unknown3 { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            Name = loader.ReadPodString();
            Unknown1 = loader.ReadInt32();
            Unknown2 = loader.ReadInt32();
            Number = loader.ReadPodString();
            Unknown3 = loader.ReadMany(loader.ReadInt32(), () => loader.ReadVector3U());
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WritePodString(Name);
            saver.WriteInt32(Unknown1);
            saver.WriteInt32(Unknown2);
            saver.WritePodString(Number);
            saver.WriteInt32(Unknown3.Count);
            saver.WriteMany(Unknown3, x => saver.WriteVector3U(x));
        }
    }
}