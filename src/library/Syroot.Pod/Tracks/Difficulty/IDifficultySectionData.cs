﻿namespace Syroot.Pod.Tracks
{
    public interface IDifficultySectionData : ISectionData
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        string DifficultyName { get; set; }
    }
}