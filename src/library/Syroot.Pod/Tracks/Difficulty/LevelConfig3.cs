﻿using Syroot.BinaryData;
using Syroot.Pod.IO;

namespace Syroot.Pod.Tracks
{
    public class LevelConfig3 : IData<Track>
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public int Unknown1 { get; set; }

        public int Unknown2 { get; set; }

        public int Unknown3 { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        void IData<Track>.Load(DataLoader<Track> loader, object parameter)
        {
            Unknown1 = loader.ReadInt32();
            Unknown2 = loader.ReadInt32();
            Unknown3 = loader.ReadInt32();
        }

        void IData<Track>.Save(DataSaver<Track> saver, object parameter)
        {
            saver.WriteInt32(Unknown1);
            saver.WriteInt32(Unknown2);
            saver.WriteInt32(Unknown3);
        }
    }
}