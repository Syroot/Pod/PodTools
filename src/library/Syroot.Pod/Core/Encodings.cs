﻿using System;
using System.Text;

namespace Syroot.Pod.Core
{
    internal static class Encodings
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        internal static readonly Encoding Codepage1252;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        static Encodings()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
            Codepage1252 = Encoding.GetEncoding(1252);
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static void GetBytes0(this Encoding encoding, ReadOnlySpan<char> chars, Span<byte> bytes)
        {
            int length = encoding.GetBytes(chars, bytes);
            if (length >= bytes.Length)
                throw new ArgumentException("String exceeds buffer size.", nameof(bytes));
            bytes[length..].Fill(0);
        }

        internal static string GetString0(this Encoding encoding, ReadOnlySpan<byte> bytes)
        {
            string value = encoding.GetString(bytes).TrimEnd('\0');
            if (value.Length >= bytes.Length)
                throw new ArgumentException("String exceeds buffer size.", nameof(bytes));
            return value;
        }
    }
}
