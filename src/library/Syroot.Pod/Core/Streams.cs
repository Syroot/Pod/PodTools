﻿using System;
using System.Buffers.Binary;
using System.IO;

namespace Syroot.Pod.Core
{
    internal static class Streams
    {
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal static int ReadInt32LittleEndian(this Stream stream)
        {
            Span<byte> buffer = stackalloc byte[sizeof(int)];
            stream.Read(buffer);
            return BinaryPrimitives.ReadInt32LittleEndian(buffer);
        }

        internal static void WriteInt32LittleEndian(this Stream stream, int value)
        {
            Span<byte> buffer = stackalloc byte[sizeof(int)];
            BinaryPrimitives.WriteInt32LittleEndian(buffer, value);
            stream.Write(buffer);
        }
    }
}
