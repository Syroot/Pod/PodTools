﻿using System.Collections.Generic;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Textures
{
    /// <summary>
    /// Represents a list of <see cref="Texture"/> instances which all have the same dimensions.
    /// </summary>
    public class TextureList
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private List<Texture> _textures = new();

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public TextureList(int dimension, TextureFormat format)
        {
            Dimension = dimension;
            Format = format;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the width and height of the <see cref="Textures"/> stored in this instance.
        /// </summary>
        public int Dimension { get; }

        /// <summary>
        /// Gets the <see cref="TextureFormat"/> of the <see cref="Textures"/> stored in this instance.
        /// </summary>
        public TextureFormat Format { get; }

        /// <summary>
        /// Gets the list of <see cref="Texture"/> instances managed by this instance.
        /// </summary>
        public IReadOnlyList<Texture> Textures => _textures.AsReadOnly();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Adds a <see cref="Texture"/> with the appropriate size.
        /// </summary>
        /// <returns>The added <see cref="Texture"/> instance.</returns>
        public Texture AddTexture()
        {
            Texture texture = new(Dimension, Format);
            _textures.Add(texture);
            return texture;
        }

        /// <summary>
        /// Removes a <see cref="Texture"/> at the given <paramref name="index"/>.
        /// </summary>
        /// <param name="index">The index of the texture to remove.</param>
        public void RemoveTexture(int index) => _textures.RemoveAt(index);

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf)
        {
            int count = pbdf.ReadInt32();
            pbdf.ReadUInt32(); // reserved

            _textures = new List<Texture>(count);

            for (int i = 0; i < count; i++)
            {
                Texture texture = AddTexture();
                int areaCount = pbdf.ReadInt32();
                for (int j = 0; j < areaCount; i++)
                {
                    TextureArea area = new();
                    area.Read(pbdf);
                    texture.Areas.Add(area);
                }
            }

            for (int i = 0; i < count; i++)
                pbdf.Read(Textures[i].Data);
        }

        internal void Write(PbdfWriter pbdf)
        {
            pbdf.WriteInt32(Textures.Count);
            pbdf.WriteInt32(0); // reserved

            for (int i = 0; i < Textures.Count; i++)
            {
                Texture texture = Textures[i];
                pbdf.WriteInt32(texture.Areas.Count);
                for (int j = 0; j < texture.Areas.Count; j++)
                {
                    texture.Areas[i].Write(pbdf);
                }
            }

            for (int i = 0; i < Textures.Count; i++)
            {
                pbdf.Write(Textures[i].Data);
            }
        }
    }
}
