﻿using System;

namespace Syroot.Pod.Textures
{
    public enum TextureFormat
    {
        RGB565
    }

    internal static class TextureFormatExtensions
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        internal static int GetBytesPerPixel(this TextureFormat format)
        {
            return format switch
            {
                TextureFormat.RGB565 => 2,
                _ => throw new ArgumentException("Unknown texture format.", nameof(format))
            };
        }
    }
}
