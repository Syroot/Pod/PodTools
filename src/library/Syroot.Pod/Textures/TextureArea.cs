﻿using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Textures
{
    /// <summary>
    /// Represents a frame on a texture which forms its own visual image.
    /// </summary>
    public struct TextureArea
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name;
        public int Left;
        public int Top;
        public int Right;
        public int Bottom;
        public int Index;

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf)
        {
            Name = pbdf.ReadStringBuffer(32);
            Left = pbdf.ReadInt32();
            Top = pbdf.ReadInt32();
            Right = pbdf.ReadInt32();
            Bottom = pbdf.ReadInt32();
            Index = pbdf.ReadInt32();
        }

        internal void Write(PbdfWriter pbdf)
        {
            pbdf.WriteStringBuffer(Name, 32);
            pbdf.WriteInt32(Left);
            pbdf.WriteInt32(Top);
            pbdf.WriteInt32(Right);
            pbdf.WriteInt32(Bottom);
            pbdf.WriteInt32(Index);
        }
    }
}
