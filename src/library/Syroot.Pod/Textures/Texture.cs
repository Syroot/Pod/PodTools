﻿using System.Collections.Generic;

namespace Syroot.Pod.Textures
{
    /// <summary>
    /// Represents a square texture which stores several sub images and is managed by a <see cref="TextureList"/>.
    /// </summary>
    public class Texture
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal Texture(int dimension, TextureFormat format)
        {
            Data = new byte[dimension * dimension * format.GetBytesPerPixel()];
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the list of sub images in this texture.
        /// </summary>
        public IList<TextureArea> Areas { get; set; } = new List<TextureArea>();

        /// <summary>
        /// Gets the raw pixel data.
        /// </summary>
        public byte[] Data { get; }
    }
}
