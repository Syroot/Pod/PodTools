﻿using System;
using System.Buffers.Binary;
using System.Collections.Generic;
using System.IO;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.TrackScripts
{
    /// <summary>
    /// Represents CIRCUIT.BIN files storing information on (installed) tracks.
    /// </summary>
    public class TrackScript
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _infoSize = 140;
        private const int _infoKey = 0x50;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the circuits available in the game. Should not exceed 96 to prevent runtime errors.
        /// </summary>
        public IList<TrackInfo> Tracks { get; } = new List<TrackInfo>();

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the instance data from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the instance data from.</param>
        public void Load(Stream stream)
        {
            // Reset instance.
            Tracks.Clear();

            // Read raw number of tracks.
            int count = stream.ReadInt32LittleEndian();

            // Read each enciphered track info.
            Span<byte> buffer = stackalloc byte[_infoSize];
            while (count-- > 0)
            {
                stream.Read(buffer);
                for (int i = 0; i < _infoSize; i++)
                    buffer[i] ^= _infoKey;

                Tracks.Add(new()
                {
                    Name = Encodings.Codepage1252.GetString0(buffer[0..20]),
                    DisplayName = Encodings.Codepage1252.GetString0(buffer[20..84]),
                    ImageFile1 = Encodings.Codepage1252.GetString0(buffer[84..97]),
                    ImageFile2 = Encodings.Codepage1252.GetString0(buffer[97..110]),
                    ImageFile3 = Encodings.Codepage1252.GetString0(buffer[110..124]),
                    Flags = (TrackFlags)buffer[124],
                    Version = buffer[125],
                    ID = buffer[126],
                    Length = BinaryPrimitives.ReadInt32LittleEndian(buffer[128..132]),
                    Laps = buffer[132],
                    Level = buffer[133]
                });
            }
        }

        /// <summary>
        /// Saves the instance data in the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to save the instance data in.</param>
        public void Save(Stream stream)
        {
            // Write raw number of tracks.
            stream.WriteInt32LittleEndian(Tracks.Count);

            // Write each enciphered track info.
            Span<byte> buffer = stackalloc byte[_infoSize];
            foreach (TrackInfo track in Tracks)
            {
                Encodings.Codepage1252.GetBytes0(track.Name, buffer[0..20]);
                Encodings.Codepage1252.GetBytes0(track.DisplayName, buffer[20..84]);
                Encodings.Codepage1252.GetBytes0(track.ImageFile1, buffer[84..97]);
                Encodings.Codepage1252.GetBytes0(track.ImageFile2, buffer[97..110]);
                Encodings.Codepage1252.GetBytes0(track.ImageFile3, buffer[110..124]);
                buffer[124] = (byte)track.Flags;
                buffer[125] = track.Version;
                buffer[126] = track.ID;
                BinaryPrimitives.WriteInt32LittleEndian(buffer[128..132], track.Length);
                buffer[132] = track.Laps;
                buffer[133] = track.Level;

                for (int i = 0; i < _infoSize; i++)
                    buffer[i] ^= _infoKey;
                stream.Write(buffer);
            }
        }
    }
}
