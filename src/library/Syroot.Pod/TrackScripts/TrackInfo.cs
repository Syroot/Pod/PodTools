﻿using System;

namespace Syroot.Pod.TrackScripts
{
    /// <summary>
    /// Represents a circuit information block stored in a <see cref="TrackScript"/> for each installed track.
    /// </summary>
    public record TrackInfo
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the base name of the file associated with the circuit. Must not be longer than 19 characters.
        /// </summary>
        public string Name { get; set; } = String.Empty;

        /// <summary>
        /// Gets or sets the case-insensitive name how the track appears in the menu. Must not be longer than 63
        /// characters.
        /// </summary>
        public string DisplayName { get; set; } = String.Empty;

        public string ImageFile1 { get; set; } = String.Empty;

        public string ImageFile2 { get; set; } = String.Empty;

        public string ImageFile3 { get; set; } = String.Empty;

        public TrackFlags Flags { get; set; }

        public byte Version { get; set; }

        public byte ID { get; set; }

        /// <summary>
        /// Gets or sets the track length in meters.
        /// </summary>
        public int Length { get; set; }

        /// <summary>
        /// Gets or sets the number of laps to drive. Only up to 8 laps correctly work.
        /// </summary>
        public byte Laps { get; set; }

        public byte Level { get; set; }
    }

    [Flags]
    public enum TrackFlags : byte
    {
        Active = 1 << 0,
        Mirror = 1 << 1
    }
}
