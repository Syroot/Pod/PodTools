﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Meshes
{
    /// <summary>
    /// Represents the surface of a polygonal model.
    /// </summary>
    public class Mesh
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public IList<Vector3> Positions { get; set; } = new List<Vector3>();

        public IList<MeshFace> Faces { get; set; } = new List<MeshFace>();

        public IList<Vector3> Normals { get; set; } = new List<Vector3>();

        /// <summary>
        /// Gets or sets the virtual size of the mesh. If too low, the mesh tends to become invisible too early.
        /// </summary>
        public float Volume { get; set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf, bool namedFaces, bool isSector)
        {
            int vertexCount = pbdf.ReadInt32();

            Positions = new List<Vector3>(vertexCount);
            for (int i = 0; i < vertexCount; i++)
                Positions.Add(pbdf.ReadVector3());

            int faceCount = pbdf.ReadInt32();
            int _ = pbdf.ReadInt32(); // triCount
            int _ = pbdf.ReadInt32(); // quadCount

            Faces = new List<MeshFace>(faceCount);
            for (int i = 0; i < faceCount; i++)
            {
                MeshFace face = new();
                face.Read(pbdf, namedFaces, isSector);
                Faces.Add(face);
            }

            Normals = new List<Vector3>(vertexCount);
            for (int i = 0; i < vertexCount; i++)
                Normals.Add(pbdf.ReadVector3());

            Volume = pbdf.ReadSingle();
        }

        internal void Write(PbdfWriter pbdf, bool namedFaces, bool isSector)
        {
            if (Positions.Count != Normals.Count)
                throw new InvalidOperationException("Count of positions and normals does not match.");

            int vertexCount = Positions.Count;
            pbdf.WriteInt32(vertexCount);

            for (int i = 0; i < Positions.Count; i++)
                pbdf.WriteVector3(Positions[i]);

            pbdf.WriteInt32(Faces.Count); // faceCount
            pbdf.WriteInt32(Faces.Where(x => x.FaceVertexCount == 3).Count()); // triCount
            pbdf.WriteInt32(Faces.Where(x => x.FaceVertexCount == 4).Count()); // quadCount

            for (int i = 0; i < Faces.Count; i++)
                Faces[i].Write(pbdf, namedFaces, isSector);

            for (int i = 0; i < Normals.Count; i++)
                pbdf.WriteVector3(Normals[i]);

            pbdf.WriteSingle(Volume);
        }
    }
}
