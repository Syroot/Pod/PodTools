﻿using System;
using System.Drawing;
using System.Numerics;
using Syroot.Pod.Pbdf;

namespace Syroot.Pod.Meshes
{
    /// <summary>
    /// Represents a single polygon of a <see cref="Mesh"/>.
    /// </summary>
    public class MeshFace
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public string Name { get; set; } = String.Empty;

        public int FaceVertexCount { get; set; }

        public int[] Indices { get; private set; } = new int[4];

        public Vector3 Normal { get; set; }

        public string MaterialType { get; set; } = String.Empty;

        public uint ColorOrTexIndex { get; set; }

        public Point[] TexCoords { get; private set; } = new Point[4];

        public uint Reserved { get; set; }

        public Vector3 QuadReserved { get; set; }

        public uint SectorProp { get; set; }

        public uint Properties { get; set; }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        internal void Read(PbdfReader pbdf, bool namedFaces, bool isSector)
        {
            if (namedFaces)
                Name = pbdf.ReadString();

            if (pbdf.Key == 0x00005CA8)
            {
                Indices[3] = pbdf.ReadInt32();
                Indices[0] = pbdf.ReadInt32();
                FaceVertexCount = pbdf.ReadInt32();
                Indices[2] = pbdf.ReadInt32();
                Indices[1] = pbdf.ReadInt32();
            }
            else
            {
                FaceVertexCount = pbdf.ReadInt32();
                for (int i = 0; i < Indices.Length; i++)
                    Indices[i] = pbdf.ReadInt32();
            }

            Normal = pbdf.ReadVector3();
            MaterialType = pbdf.ReadString();
            ColorOrTexIndex = pbdf.ReadUInt32();

            for (int i = 0; i < TexCoords.Length; i++)
                TexCoords[i] = new Point(pbdf.ReadInt32(), pbdf.ReadInt32());

            Reserved = pbdf.ReadUInt32();
            if (FaceVertexCount == 4)
                QuadReserved = pbdf.ReadVector3();
            if (isSector && Normal != Vector3.Zero)
                SectorProp = pbdf.ReadUInt32();
            Properties = pbdf.ReadUInt32();
        }

        internal void Write(PbdfWriter pbdf, bool namedFaces, bool isSector)
        {
            if (namedFaces)
                pbdf.WriteString(Name);

            if (pbdf.Key == 0x00005CA8)
            {
                pbdf.WriteInt32(Indices[3]);
                pbdf.WriteInt32(Indices[0]);
                pbdf.WriteInt32(FaceVertexCount);
                pbdf.WriteInt32(Indices[2]);
                pbdf.WriteInt32(Indices[1]);
            }
            else
            {
                pbdf.WriteInt32(FaceVertexCount);
                for (int i = 0; i < Indices.Length; i++)
                    pbdf.WriteInt32(Indices[i]);
            }

            pbdf.WriteVector3(Normal);
            pbdf.WriteString(MaterialType);
            pbdf.WriteUInt32(ColorOrTexIndex);

            for (int i = 0; i < TexCoords.Length; i++)
                pbdf.WritePoint(TexCoords[i]);

            pbdf.WriteUInt32(Reserved);
            if (FaceVertexCount == 4)
                pbdf.WriteVector3(QuadReserved);
            if (isSector && Normal != Vector3.Zero)
                pbdf.WriteUInt32(SectorProp);
            pbdf.WriteUInt32(Properties);
        }
    }
}
