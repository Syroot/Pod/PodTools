#define WIN32_LEAN_AND_MEAN
#include "pkExe.h"
#include "pkIni.h"
#include "pkUtils.h"

// ---- Initialization ----

// The default buffer for installed cars only fits 32 cars, but 50 cars are known to exist, and having all of them
// installed crashed POD or caused weird behavior when entering the "Load Car" screen. This setting relocates the
// memory at which the car data is stored to the given address.
UINT iniAllow64InstalledCars;

// This changes a jump that would skip the 800x600 resolution on 3dfx when toggling through it.The only use-case for
// this seems to be the increased draw distance after toggling to it in each race.
BOOL iniAltF7Include800x600;

// A sound related mishap in POD accesses an array out of bounds, sprinkling null pointers into the sound buffer
// array, to which the game still tries to write after ending a race, causing POD to crash.The array accesses were
// completely nopped out as tests did not show any problems caused by this.
BOOL iniFixRaceEndCrash;

// On some systems, MCI_OPEN fails with MCIERR_CANNOT_LOAD_DRIVER. This error can be ignored since CD audio is
// wrapped by PodHacks. The patch simply returns "Ignore" instead of showing the error dialog.
BOOL iniIgnoreCDAudioFail;

void init()
{
	pk::Ini ini("pkPlatinum.ini");

	// Load INI settings.
	ini.get("Fixes", "Allow64InstalledCars", iniAllow64InstalledCars, 0x6CFA00);
	ini.get("Fixes", "AltF7Include800x600", iniAltF7Include800x600, TRUE);
	ini.get("Fixes", "FixRaceEndCrash", iniFixRaceEndCrash, TRUE);
	ini.get("Fixes", "IgnoreCDAudioFail", iniIgnoreCDAudioFail, TRUE);

	// Ensure INI file has been created with default settings.
	ini.set("Fixes", "Allow64InstalledCars", iniAllow64InstalledCars);
	ini.set("Fixes", "AltF7Include800x600", iniAltF7Include800x600);
	ini.set("Fixes", "FixRaceEndCrash", iniFixRaceEndCrash);
	ini.set("Fixes", "IgnoreCDAudioFail", iniIgnoreCDAudioFail);
}

// ---- Patch ----

void patch(pk::Exe& exe, int gameVersion)
{
	switch (gameVersion)
	{
		case pk::GAMEID_2281_3DFX_MMX:
		{
			/*if (iniAllow64InstalledCars)
			{
				exe.set<UINT>(0x2FDCE, 0x6CFA00);
				exe.set<UINT>(0x30358, 0x6CFA00);
				exe.set<UINT>(0x30382, 0x6CFA00);
				exe.set<UINT>(0x30418, 0x6CFA00);
				exe.set<UINT>(0x305BD, 0x6CFA00);
				exe.set<UINT>(0x305F2, 0x6CFA00);
				exe.set<UINT>(0x306A5, 0x6CFA00);
				exe.set<UINT>(0x6F0DA, 0x6CFA00);
			}
			if (iniAltF7Include800x600)
			{
				exe.set<BYTE>(0xA4994, 0x75);
			}*/
			if (iniFixRaceEndCrash)
			{
				exe.setNop(0xC166A, 6);
				exe.setNop(0xC1687, 6);
			}
			/*if (iniIgnoreCDAudioFail)
			{
				exe.set<BYTE>(0xC4A0F, 0xB8);
				exe.set<UINT>(0xC4A10, 0x02);
			}*/
			break;
		}
	}
}

// ---- Main ----

int getSupportedVersion(DWORD timeDateStamp)
{
	int id = pk::getGameID(timeDateStamp);
	switch (id)
	{
		case pk::GAMEID_2281_3DFX_MMX:
			return id;
		default:
			return 0;
	}
}

BOOL WINAPI DllMain(HMODULE, DWORD fdwReason, LPVOID)
{
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
		{
			pk::Exe exe;
			int version = getSupportedVersion(exe.FileHeader->TimeDateStamp);
			if (version == 0)
			{
				MessageBox(NULL, "pkPlatinum is only compatible with POD 3DFX MMX 2.2.8.1. "
					"You can delete the module to remove this warning.", "pkPlatinum", MB_ICONWARNING);
			}
			else
			{
				init();
				patch(exe, version);
			}
		}
		break;

		case DLL_PROCESS_DETACH:
			break;
	}
	return TRUE;
}
