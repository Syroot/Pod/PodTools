#include "pkUtils.h"
#include <string>
#include "pkPatch.h"

namespace pk
{
	int getGameID(DWORD timeDateStamp)
	{
		switch (timeDateStamp)
		{
			case 0x34538F97: return GAMEID_2281;          // 1997-10-26 18:44:39
			case 0x34538B18: return GAMEID_2281_MMX;      // 1997-10-26 18:25:28
			case 0x3454B183: return GAMEID_2281_MS;       // 1997-10-27 15:21:39

			case 0x345270AB: return GAMEID_2281_3DFX;     // 1997-10-25 22:20:27
			case 0x345274BF: return GAMEID_2281_3DFX_MMX; // 1997-10-25 22:37:51
			case 0x3454B92E: return GAMEID_2281_3DFX_MS;  // 1997-10-27 15:54:22

			case 0x3453B114: return GAMEID_2281_ATI;      // 1997-10-26 21:07:32
			case 0x3457C02A: return GAMEID_2281_ATI_MMX;  // 1997-10-29 23:00:58

			case 0x34527B93: return GAMEID_2281_D3D;      // 1997-10-25 23:06:59
			case 0x34528156: return GAMEID_2281_D3D_MMX;  // 1997-10-25 23:31:34

			case 0x346C81C8: return GAMEID_2281_D3D5;     // 1997-11-14 16:52:24
			case 0x346CA39A: return GAMEID_2281_D3D5_MMX; // 1997-11-14 19:16:42

			case 0x3457B104: return GAMEID_2281_PVR;      // 1997-10-29 21:56:20
			case 0x3457B57C: return GAMEID_2281_PVR_MMX;  // 1997-10-29 22:15:24

			case 0x345399FC: return GAMEID_2281_S3;       // 1997-10-26 19:29:00
			case 0x3457B871: return GAMEID_2281_S3_MMX;   // 1997-10-29 22:28:01

			default: return GAMEID_NONE;
		}
	}

	std::string getErrorMessage(int error)
	{
		if (error == ERROR_SUCCESS)
			return std::string();

		LPTSTR buffer = NULL;
		const DWORD cchMsg = FormatMessageA(
			FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS | FORMAT_MESSAGE_ALLOCATE_BUFFER, NULL,
			error, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), reinterpret_cast<LPTSTR>(&buffer), 0, NULL);
		if (cchMsg > 0)
		{
			std::string message(buffer);
			LocalFree(buffer);
			return message;
		}
		else
		{
			CHAR msg[32];
			sprintf_s(msg, "Error code 0x%08X.", error);
			return msg;
		}
	}
}
