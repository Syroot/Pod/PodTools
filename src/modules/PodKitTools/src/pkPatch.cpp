#include "pkPatch.h"
#include <stdexcept>

namespace pk
{
	// ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

	Patch::Patch(LPVOID address, SIZE_T size)
		: _address((LPBYTE)address)
		, _size(size)
		, position(0)
	{
		if (!_address || !_size)
			throw std::invalid_argument("Address and size must not be 0.");
		if (!VirtualProtect(_address, _size, PAGE_EXECUTE_READWRITE, &_oldProtect))
			throw std::exception("VirtualProtect failed, call GetLastError for more info.");
	}

	Patch::~Patch()
	{
		close();
	}

	// ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------

	void Patch::close() const
	{
		DWORD oldProtect;
		if (!VirtualProtect(_address, _size, _oldProtect, &oldProtect))
			throw std::exception("VirtualProtect failed, call GetLastError for more info.");
	}
};
