#include "pkExe.h"
#include <iostream>
#include <sstream>
#include <stdexcept>
#include <vector>

namespace pk
{
	// ---- CONSTRUCTORS & DESTRUCTOR ----------------------------------------------------------------------------------

	Exe::Exe(HMODULE hModule)
	{
		Handle = hModule == NULL ? GetModuleHandleA(NULL) : hModule;
		DosHeader = (PIMAGE_DOS_HEADER)Handle;
		NtHeader = (PIMAGE_NT_HEADERS)((DWORD)DosHeader + DosHeader->e_lfanew);
		FileHeader = (PIMAGE_FILE_HEADER)&NtHeader->FileHeader;
		OptHeader = (PIMAGE_OPTIONAL_HEADER)&NtHeader->OptionalHeader;
	}

	// ---- METHODS (PUBLIC) -------------------------------------------------------------------------------------------
	
	ULONG_PTR Exe::find(std::string signature)
	{
		std::istringstream iss(signature);
		std::istream_iterator<std::string> begin(iss), end;
		std::vector<std::string> tokens(begin, end);

		std::vector<int16_t> sig;
		for (auto& token : tokens)
			sig.push_back(token == "??" ? -1 : static_cast<int16_t>(std::stoi(token, 0, 16)));

		return find(&sig[0], sig.size());
	}

	ULONG_PTR Exe::find(int16_t signature[], size_t signatureSize)
	{
		PIMAGE_SECTION_HEADER sections = IMAGE_FIRST_SECTION(NtHeader);
		for (int index = 0; index < FileHeader->NumberOfSections; index++)
		{
			if (sections[index].Characteristics != (IMAGE_SCN_CNT_CODE | IMAGE_SCN_MEM_EXECUTE | IMAGE_SCN_MEM_READ))
				continue;
			LPBYTE code = (LPBYTE)(ULONG_PTR)Handle + sections[index].VirtualAddress;
			LPBYTE last = &code[sections[index].SizeOfRawData - signatureSize];
			for (; code < last; code++)
			{
				bool found = true;
				for (size_t i = 0; i < signatureSize; i++)
				{
					if (signature[i] < 0x00 || signature[i] > 0xFF || code[i] == (BYTE)signature[i])
					{
						continue;
					}
					else
					{
						found = false;
						break;
					}
				}
				if (found)
					return (ULONG_PTR)code - (ULONG_PTR)Handle;
			}
		}
		throw std::exception("Could not find signature.");
	}

	void Exe::setImp(LPVOID import, LPVOID hook)
	{
		bool success = false;
		PIMAGE_IMPORT_DESCRIPTOR imports = getImports();
		if (!imports)
			throw std::exception("Import patching failed, import table not found.");

		while (imports->Characteristics)
		{
			PIMAGE_THUNK_DATA thunk = (PIMAGE_THUNK_DATA)((ULONG_PTR)Handle + imports->OriginalFirstThunk);
			PIMAGE_THUNK_DATA table = (PIMAGE_THUNK_DATA)((ULONG_PTR)Handle + imports->FirstThunk);
			while (thunk->u1.Ordinal)
			{
				if (table->u1.Function == (ULONG_PTR)import)
				{
					Patch patch(&table->u1.Function, sizeof(ULONG_PTR));
					patch.write(hook);
					success = true;
				}
				thunk++;
				table++;
			}
			imports++;
		}

		if (!success)
			throw std::exception("Import patching failed, import not found.");
	}

	void Exe::setJmp(ULONG_PTR offset, SIZE_T size, LPVOID callee, DWORD jumpType)
	{
		Patch patch(getAddress(offset), size);

		if (size >= 5 && offset)
		{
			BYTE opSize, opCode;
			switch (jumpType)
			{
				case JT_PUSHRET: opSize = 6; opCode = 0x68; break;
				case JT_FARJMP: opSize = 7; opCode = 0xEA; break;
				case JT_FARCALL: opSize = 7; opCode = 0x9A; break;
				case JT_CALL: opSize = 5; opCode = 0xE8; break;
				default: opSize = 5; opCode = 0xE9; break;
			}

			if (size < opSize)
				throw std::exception("Not enough space to patch opcode.");

			patch.write(opCode);
			switch (opSize)
			{
				case 7:
					patch.write((ULONG)callee);
					patch.write<WORD>(0x23);
					break;
				case 6:
					patch.write((ULONG)callee);
					patch.write<BYTE>(0xC3);
					break;
				default:
					patch.write((ULONG)callee - (ULONG_PTR)offset - 5);
					break;
			}
			for (DWORD i = opSize; i < size; i++)
				patch.write((uint8_t)0x90);
		}
	}

	void Exe::setNop(ULONG_PTR offset, SIZE_T size)
	{
		Patch patch(getAddress(offset), size);
		while (size--)
			patch.write((uint8_t)0x90);
	}

	// ---- METHODS (PRIVATE) ------------------------------------------------------------------------------------------

	LPVOID Exe::getAddress(ULONG_PTR offset)
	{
		return (LPVOID)((ULONG_PTR)Handle + offset);
	}

	PIMAGE_IMPORT_DESCRIPTOR Exe::getImports()
	{
		if ((FileHeader->SizeOfOptionalHeader
			>= FIELD_OFFSET(IMAGE_OPTIONAL_HEADER, DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT + 1]))
			&& OptHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress
			&& OptHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size)
		{
			return (PIMAGE_IMPORT_DESCRIPTOR)((ULONG_PTR)Handle
				+ OptHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
		}
		return NULL;
	}
}
