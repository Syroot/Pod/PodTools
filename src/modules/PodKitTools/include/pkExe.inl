namespace pk
{
	template <class T>
	void Exe::set(ULONG_PTR offset, T newValue)
	{
		Patch patch(getAddress(offset), sizeof(T));
		patch.write(newValue);
	}
}