#pragma once
#include <stdint.h>
#include <string>
#include <Windows.h>
#include "pkPatch.h"

namespace pk
{
	enum JumpType
	{
		JT_JMP, // jmp (0xE9)
		JT_CALL, // call (0xE8)
		JT_FARJMP, // farjump (0xEA)
		JT_FARCALL, // farcall (0x9A)
		JT_PUSHRET, // pushret
	};

	struct Exe
	{
	public:
		// ---- FIELDS -------------------------------------------------------------------------------------------------

		HANDLE Handle;
		PIMAGE_DOS_HEADER DosHeader;
		PIMAGE_NT_HEADERS NtHeader;
		PIMAGE_FILE_HEADER FileHeader;
		PIMAGE_OPTIONAL_HEADER OptHeader;

		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		Exe(HMODULE hModule = NULL);

		// ---- METHODS ------------------------------------------------------------------------------------------------

		/// <summary>
		/// Returns the offset of the first match of the given signature. It must take the form "12 FF ?? EB 34", where
		/// two question marks represent a wildcard.
		/// </summary>
		/// <param name="signature">The signature to find.</param>
		/// <returns>The offset of the first match of the signature.</returns>
		ULONG_PTR find(std::string signature);

		/// <summary>
		/// Returns the offset of the first match of the given signature. It is an array of bytes, where all values
		/// being negative or bigger than 0xFF represent a wildcard.
		/// </summary>
		/// <param name="signature">The signature to find.</param>
		/// <returns>The offset of the first match of the signature.</returns>
		ULONG_PTR find(int16_t signature[], size_t signatureSize); // any non-uint8_t value is a wildcard

		/// <summary>
		/// Writes the given value to the specified offset.
		/// </summary>
		/// <typeparam name="T">The type of the value to write.</typeparam>
		/// <param name="offset">The relative address at which to write the value.</param>
		/// <param name="value">The value to write.</param>
		template <class T> void set(ULONG_PTR offset, T value);

		/// <summary>
		/// Replaces the given import with the specified hook, which must have the same signature.
		/// </summary>
		/// <param name="import">The imported method to replace.</param>
		/// <param name="hook">The method to replace the import with.</param>
		void setImp(LPVOID import, LPVOID hook);

		/// <summary>
		/// Writes a jump to the given callee at the specified offset.
		/// </summary>
		/// <param name="offset">The relative address at which to write the value.</param>
		/// <param name="size">The number of bytes overwritten for the jump opcode.</param>
		/// <param name="callee">The called method.</param>
		/// <param name="jumpType">The type of the jump to insert.</param>
		void setJmp(ULONG_PTR offset, SIZE_T size, LPVOID callee, DWORD jumpType);

		/// <summary>
		/// Nops out the given number of bytes at the specified offset.
		/// </summary>
		/// <param name="offset">The relative address at which to nop out code.</param>
		/// <param name="size">The number of bytes to nop out.</param>
		void setNop(ULONG_PTR offset, SIZE_T size);

	private:
		LPVOID getAddress(ULONG_PTR offset);
		PIMAGE_IMPORT_DESCRIPTOR getImports();
	};
}

#include "pkExe.inl"