#pragma once
#include <Windows.h>

namespace pk
{
	struct Patch
	{
	public:
		// ---- FIELDS -------------------------------------------------------------------------------------------------	

		ULONG_PTR position;

		// ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

		Patch(LPVOID address, SIZE_T size);
		~Patch();

		// ---- METHODS ------------------------------------------------------------------------------------------------

		void close() const;
		template <class T> void write(const T& value);

	private:
		// ---- FIELDS -------------------------------------------------------------------------------------------------
		LPBYTE _address;
		SIZE_T _size;
		DWORD _oldProtect;
	};
}

#include "pkPatch.inl"
