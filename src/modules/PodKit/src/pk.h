#define WIN32_LEAN_AND_MEAN
#include <vector>
#include <Windows.h>

using namespace std;

namespace pk
{
	std::vector<HMODULE> modules;

	void attach()
	{
		// Get executable directory.
		CHAR buffer[MAX_PATH];
		GetModuleFileName(NULL, buffer, MAX_PATH);
		*(strrchr(buffer, '\\') + 1) = '\0';

		// Attempt to load all library files matching the pk*.dll search pattern.
		lstrcat(buffer, "pk*.dll");
		WIN32_FIND_DATA findFileData;
		HANDLE hFindFile = FindFirstFile(buffer, &findFileData);
		if (hFindFile == INVALID_HANDLE_VALUE)
			return;
		do
		{
			if (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
				continue;
			HMODULE hLibrary = LoadLibrary(findFileData.cFileName);
			if (hLibrary)
			{
				modules.push_back(hLibrary);
			}
			else
			{
				sprintf_s(buffer, "Could not load module %s.", findFileData.cFileName);
				MessageBox(NULL, buffer, "PodKit", MB_ICONWARNING);
			}
		} while (FindNextFile(hFindFile, &findFileData));
		FindClose(hFindFile);
	}

	void detach()
	{
		// Release all loaded modules.
		for (HMODULE hModule : modules)
			FreeLibrary(hModule);
		modules.clear();
	}
}
