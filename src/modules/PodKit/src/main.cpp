#include <Windows.h>
#include "pk.h"

struct DInputDll
{
	HMODULE dll;
	FARPROC DirectInputCreateA;
} dinput;

__declspec(naked) void DirectInputCreateA() { _asm { jmp[dinput.DirectInputCreateA] } }

BOOL APIENTRY DllMain(HMODULE, DWORD fdwReason, LPVOID)
{
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH:
			char path[MAX_PATH];
			GetSystemDirectory(path, MAX_PATH);
			lstrcat(path, "\\dinput.dll");

			dinput.dll = LoadLibrary(path);
			if (dinput.dll == NULL)
			{
				MessageBox(NULL, "Failed to load original dinput.dll.", "PodKit", MB_ICONERROR);
				return FALSE;
			}
			dinput.DirectInputCreateA = GetProcAddress(dinput.dll, "DirectInputCreateA");

			pk::attach();
			break;

		case DLL_PROCESS_DETACH:
			FreeLibrary(dinput.dll);

			pk::detach();
			break;
	}

	return TRUE;
}
