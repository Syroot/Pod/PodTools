# Pod

Tools and libraries for working with game data of the 1997 racing game "POD - Planet of Death", created by UbiSoft.

The following projects are available in the `src` folder:
- `010_editor`: Scripts and templates for de-/encrypting and parsing game files.
- `io_scene_pod`: Blender add-on for loading POD cars and tracks.
- `sgl_stub`: Stub PowerVR SGL.dll wrapper for running MSVC compiled versions of POD minus the 3D rendering.
- `Syroot.Pod`: .NET 5 library for loading, manipulating, and saving game files.
- `Syroot.Pod.GameService`: A rewrite of the GameService router and server for online matchmaking.

![Scorp car loaded with io_scene_pod](res/readme/io_scene_pod_car.png)
![AlderOEM track loaded with io_scene_pod](res/readme/io_scene_pod_track.png)
